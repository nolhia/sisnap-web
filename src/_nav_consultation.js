// Menu visible pour les profils de consultation

export default {
  items: [
    {
      name: 'Tableau de bord',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      divider: true
    },
    {
      title: true,
      name: 'États de sortie',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Requêtes',
      url: '/requetes',
      icon: 'icon-book-open',
      children: [
        {
          name: 'Mesures',
          url: '/requetes/mesures',
          icon: 'icon-equalizer'
        },
        {
          name: 'Analyses',
          url: '/requetes/analyses',
          icon: 'icon-grid'
        }
      ]
    },
    {
      name: 'Graphiques',
      url: '/graphiques',
      icon: 'icon-graph',
      children: [
        {
          name: "Piézométrie",
          url: "/graphiques/piezometries",
          icon: "icon-equalizer"
        },
        {
          name: 'Hydrométrie',
          url: '/graphiques/hydrometries',
          icon: 'icon-equalizer'
        },
        {
          name: 'Pluviométrie',
          url: '/graphiques/pluviometries',
          icon: 'icon-equalizer'
        }
      ]
    },
    {
      name: 'Cartes',
      url: '/cartes',
      icon: 'icon-map',
      children: [
        {
          name: 'Statiques',
          url: '/cartes/statiques',
          icon: 'icon-picture'
        },
        {
          name: 'Dynamiques',
          url: '/cartes/dynamiques',
          icon: 'icon-location-pin'
        }
      ]
    },
    {
      name: 'Rapports',
      url: '/rapports',
      icon: 'icon-notebook',
      children: [
        {
          name: 'Physique&chimique',
          url: '/rapports/analyseschimiques',
          icon: 'icon-chemistry'
        },
        {
          name: 'Bactériologie',
          url: '/rapports/bacteriologiques',
          icon: 'icon-social-reddit'
        }
      ]
    }
  ]
};
