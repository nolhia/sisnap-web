/*
    Composant permettant la gestion des regions et des departements
*/
import React, { Component } from 'react'; 
import { Col,  FormGroup, Input, Label } from 'reactstrap'; 
import _ from 'lodash';
var zones = '';

class RegionDepartement extends Component {
    constructor(props) {
        super(props);    
        this.state = { 
            niveau: 1,
            region_id: '',
            departement_id: '', 
            regions: [],
            departements: []
        }
        this.handleChange = this.handleChange.bind(this); 
        this.handleRegion = this.handleRegion.bind(this); 
        this.handleDepartement = this.handleDepartement.bind(this); 
    } 

    componentDidMount() { 
        zones = localStorage.getItem('zonegeographique') || '';
        if(zones !== ''){
            zones = JSON.parse(zones);
            this.setState({ regions: zones.regions, departements: zones.departements });
        }
    } 
  
    handleRegion(e) {
        const { name, value } = e.target;  
        const departements = _.filter(zones.departements, function(o) {  
            return o.region_id === value; 
        }); 
        this.setState({ [name]: value, departements: departements });  
    } 

    handleDepartement(e) {
        const { name, value } = e.target;  
        this.setState({ [name]: value }); 
    } 
    
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    } 
  
    render(){  
        const { region_id, departement_id, regions, departements} = this.state;
        return (
            <> 
                <Col xs="4">
                  <FormGroup>
                    <Label htmlFor="region_id">Région</Label>
                    <Input type="select" id="region_id" name="region_id" value={region_id} onChange={this.handleRegion} >
                      <option value='' key=''></option>
                      {regions && regions.length > 0 && regions.map(region => <option key={region.id} value={region.id}>{region.titre}</option>)}
                    </Input> 
                  </FormGroup>
                </Col>
                <Col xs="4">
                  <FormGroup>
                    <Label htmlFor="departement_id">Département</Label>
                    <Input type="select" id="departement_id" name="departement_id" value={departement_id} onChange={this.handleDepartement} >
                      <option value='' key=''></option>
                      {departements && departements.length > 0 && departements.map(departement => <option key={departement.id} value={departement.id}>{departement.titre}</option>)}
                    </Input> 
                  </FormGroup>
                </Col>
                    
            </> 
        )
    
      }
}

export default RegionDepartement ;