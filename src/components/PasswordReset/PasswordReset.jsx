/*
    Composant permettant le changement de mot de passe
*/
import React, { Component } from 'react'; 
import { Card, CardBody, CardFooter, CardHeader, Col, Form, FormGroup, Input, Label, Row } from 'reactstrap';
import { userService } from '../../services';

class PasswordReset extends Component {
    constructor(props) {
        super(props);  
        userService.logout();

        this.state = {
            login: '', 
            submitted: false,
            loading: false,
            error: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this); 
    }
  // Soumission du formulaire
    handleSubmit(e) {
        e.preventDefault(); 
        this.setState({ submitted: true });
        const { login, password } = this.state;

        // stop here if form is invalid
        if (!login ) {
            return;
        }

        this.setState({ loading: true });
        userService.login(login, password)
            .then(
                user => {
                    const { from } = this.props.location.state || { from: { pathname: "/dashboard" } };
                    this.props.history.push(from);
                },
                error => { 
                    if(typeof error === 'string'){
                        this.setState({ error , loading: false })
                    }
                    else{
                        this.setState({ error: 'Connexion API refusée.' , loading: false })
                    } 
            }
        );
    }
  
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }
    //validation 
    validateForm() {
        return this.state.password.length > 8;
    } 
  
    render(){  
        const { loading, error } = this.state;
        return (
            <div>
                <Form onSubmit={this.handleSubmit} method="POST"> 
                    <Card>
                        <CardHeader>
                            <h3 >Réinitialiser le mot de passe?</h3> 
                            {error &&
                                <div className={'alert alert-danger'}>{error}</div>
                            }
                        </CardHeader>
                            <CardBody>  
                                <Row>
                                    <Col xs="12">
                                        <FormGroup>
                                            <Label htmlFor="mot_de_passe">Nouveau mot de passe</Label>
                                            <Input type="password" name="mot_de_passe" id="mot_de_passe" onChange={this.handleChange} placeholder="Mot de passe" required />  
                                        </FormGroup>
                                    </Col>
                                </Row> 
                                <Row>
                                    <Col xs="12">
                                        <FormGroup>
                                            <Label htmlFor="mot_de_passe_confirm">Confirmer mot de passe</Label>
                                            <Input type="password" name="mot_de_passe_confirm" id="mot_de_passe_confirm" onChange={this.handleChange} placeholder="Confirmer" required />  
                                        </FormGroup> 
                                    </Col>
                                </Row> 
                            </CardBody> 
                        <CardFooter>
                            <Row>
                                <Col xs="6">
                                    <button type="submit" className="btn btn-outline-dark" disabled={loading}>Valider le mot de passe</button> 
                                    {loading &&
                                        <img alt='Loading...' src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                    }
                                </Col> 
                            </Row>
                        </CardFooter>
                    </Card>
                </Form> 
            </div> 
        )
    
      }
}

export default PasswordReset ;