/*
    Composant permettant la gestion des regions, departements et communes
*/
import React, { Component } from 'react';
import { Col, FormGroup, Input, Label, Row } from 'reactstrap';
import _ from 'lodash';
var zones = '';

class ZoneGeographique extends Component {
  constructor(props) {
    super(props);
    this.state = {
      region_id: '',
      departement_id: '',
      commune_id: '',
      regions: [],
      departements: [],
      communes: []
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleRegion = this.handleRegion.bind(this);
    this.handleDepartement = this.handleDepartement.bind(this); 
  }
  // initialisation
   componentDidMount(){ 
    const {region_id, departement_id, commune_id} = this.props
    zones = localStorage.getItem('zonegeographique') || '';
   
    if (zones !== '') {
      zones = JSON.parse(zones);
      this.setState({ regions: zones.regions, departements: zones.departements, communes: zones.communes, region_id: region_id, departement_id: departement_id, commune_id: commune_id });
    }
  } 
  // suivi du changement de region
  handleRegion(e) {
    const { name, value } = e.target;
    const departements = _.filter(zones.departements, function (o) {
      return o.region_id === value;
    });
    this.setState({ [name]: value, departements: departements }); 
  }
 // suivi du changement de departement
  handleDepartement(e) {
    const { name, value } = e.target;
    const communes = _.filter(zones.communes, function (o) {
      return o.departement_id === value;
    });
    this.setState({ [name]: value, communes: communes });
  }
  // suivi de tout autre changement
  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  render() {
    const { region_id, departement_id, commune_id, regions, departements, communes } = this.state;
    return (
      <>
        <Row>
          <Col xs="4">
            <FormGroup>
              <Label htmlFor="region_id">Région</Label>
              <Input type="select" id="region_id" name="region_id" value={region_id} onChange={this.handleRegion} >
                <option value='' key=''></option>
                {regions && regions.length > 0 && regions.map(region => <option key={region.id} value={region.id}>{region.titre}</option>)}
              </Input>
            </FormGroup>
          </Col>
          <Col xs="4">
            <FormGroup>
              <Label htmlFor="departement_id">Département</Label>
              <Input type="select" id="departement_id" name="departement_id" value={departement_id} onChange={this.handleDepartement} >
                <option value='' key=''></option>
                {departements && departements.length > 0 && departements.map(departement => <option key={departement.id} value={departement.id}>{departement.titre}</option>)}
              </Input>
            </FormGroup>
          </Col>
          <Col xs="4">
            <FormGroup>
              <Label htmlFor="commune_id">Commune</Label>
              <Input type="select" id="commune_id" name="commune_id" value={commune_id} onChange={this.handleChange} >
                <option value='' key=''></option>
                {communes && communes.length > 0 && communes.map(commune => <option key={commune.id} value={commune.id}>{commune.titre}</option>)}
              </Input>
            </FormGroup>
          </Col>
        </Row>
      </>
    )

  }
}

export default ZoneGeographique;