import React from "react";
// tableau de board
const Dashboard = React.lazy(() => import("./views/Dashboard"));
// Coupegeologiques components
const Coupegeologiques = React.lazy(() => import("./views/pages/coupegeologique/Coupegeologiques"));
const CoupegeologiqueCreate = React.lazy(() => import("./views/pages/coupegeologique/CoupegeologiqueCreate"));
// Nappes
const Nappes = React.lazy(() => import("./views/pages/nappe/Nappes"));
const NappeCreate = React.lazy(() => import("./views/pages/nappe/NappeCreate"));
// Laboratoire
const Laboratoires = React.lazy(() => import("./views/pages/laboratoire/Laboratoires"));
const LaboratoireCreate = React.lazy(() => import("./views/pages/laboratoire/LaboratoireCreate"));
// Type ouvrage
const Typeouvrages = React.lazy(() => import("./views/pages/typeouvrage/Typeouvrages"));
const TypeouvrageCreate = React.lazy(() => import("./views/pages/typeouvrage/TypeouvrageCreate"));
// Type de station de suivi
const Codelithologique = React.lazy(() => import("./views/pages/codelithologique/Codelithologique"));
const CodelithologiqueCreate = React.lazy(() => import("./views/pages/codelithologique/CodelithologiqueCreate"));
const Elementchimiques = React.lazy(() => import("./views/pages/elementchimique/Elementchimiques"));
const ElementchimiqueCreate = React.lazy(() => import("./views/pages/elementchimique/ElementchimiqueCreate"));
// Zones geographiques
const Regions = React.lazy(() => import("./views/pages/region/Regions"));
const Departements = React.lazy(() => import("./views/pages/departement/Departements"));
const Communes = React.lazy(() => import("./views/pages/commune/Communes"));
const Enquetes = React.lazy(() => import("./views/pages/enquete/Enquetes"));
const EnqueteForm = React.lazy(() => import("./views/pages/enquete/EnqueteForm"));
const EnqueteValidation = React.lazy(() => import("./views/pages/enquete/EnqueteValidation"));
// Stations de suivi
const Stations = React.lazy(() => import("./views/pages/station/Stations"));
const StationCreate = React.lazy(() => import("./views/pages/station/StationCreate"));
// Collecte des analyses
const AnalysesChimiques = React.lazy(() => import("./views/pages/analyses/chimique/AnalysesChimiques"));
const AnalyseChimiquesCreate = React.lazy(() => import("./views/pages/analyses/chimique/AnalyseChimiquesCreate"));
const AnalyseBacteriologies = React.lazy(() => import("./views/pages/analyses/bacteriologie/bacteriologies"));
const AnalyseBacteriologiesCreate = React.lazy(() => import("./views/pages/analyses/bacteriologie/bacteriologiesCreate"));
const AnalysesIsotopiques = React.lazy(() => import("./views/pages/analyses/isotope/Isotopes"));
const AnalyseIsotopiqueCreate = React.lazy(() => import("./views/pages/analyses/isotope/IsotopeCreate"));
const AnalysesPesticides = React.lazy(() => import("./views/pages/analyses/pesticide/Pesticides"));
const AnalysePesticideCreate = React.lazy(() => import("./views/pages/analyses/pesticide/PesticideCreate"));
// Collecte des mesures
const Piezometrie = React.lazy(() => import("./views/pages/analyses/piezometrie/Piezometrie"));
const PiezometrieCreate = React.lazy(() => import("./views/pages/analyses/piezometrie/PiezometrieCreate"));
const Hydrometrie = React.lazy(() => import("./views/pages/analyses/hydrometrie/Hydrometrie"));
const HydrometrieCreate = React.lazy(() => import("./views/pages/analyses/hydrometrie/HydrometrieCreate"));
const Pluviometrie = React.lazy(() => import("./views/pages/analyses/pluviometrie/Pluviometrie"));
const PluviometrieCreate = React.lazy(() => import("./views/pages/analyses/pluviometrie/PluviometrieCreate"));
// Requetes
const RequeteMesures = React.lazy(() => import("./views/pages/requetes/mesures/Mesures"));
const RequeteAnalyses = React.lazy(() => import("./views/pages/requetes/analyses/Analyses"));
// Graphiques
const GraphiquePiezometrie = React.lazy(() => import("./views/pages/graphiques/Piezometrie"));
const GraphiqueHydrometrie = React.lazy(() => import("./views/pages/graphiques/Hydrometrie"));
const GraphiquePluviometrie = React.lazy(() => import("./views/pages/graphiques/Pluviometrie"));
// Cartes
const CarteStatiques = React.lazy(() => import("./views/pages/cartes/Statiques/Statiques"));
const CarteStatiquesCreate = React.lazy(() => import("./views/pages/cartes/Statiques/StatiquesCreate"));
const CarteStatiquesEdit = React.lazy(() => import("./views/pages/cartes/Statiques/StatiquesEdit"));
const CarteStatiquesShow = React.lazy(() => import("./views/pages/cartes/Statiques/StatiquesShow"));
const CarteMenu = React.lazy(() => import("./views/pages/cartes/Dynamiques/Menu"));
const CarteStationSuivi = React.lazy(() => import("./views/pages/cartes/Dynamiques/StationSuivi"));
// Rapports
const RapportAnalysesChimiques = React.lazy(() => import("./views/pages/rapports/analyseschimiques"));
const RapportAnalysesChimiquesPrint = React.lazy(() => import("./views/pages/rapports/analyseschimiques-print"));
const RapportAnalysesBacteriologique = React.lazy(() => import("./views/pages/rapports/analysesbacteriologiques"));
const RapportAnalysesBacteriologiquePrint = React.lazy(() => import("./views/pages/rapports/bacteriologie-print"));

// Users components
const Users = React.lazy(() => import("./views/pages/users/Users"));
const UserCreate = React.lazy(() => import("./views/pages/users/UserCreate"));
const UserPasswordReset = React.lazy(() => import("./views/pages/users/PasswordReset"));
const SelectSite = React.lazy(() => import("./views/pages/users/SelectSite"));

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  // Accueil, Tableau de board
  { path: "/", exact: true, name: "Accueil" },
  { path: "/dashboard", name: "Tableau de bord", component: Dashboard },
  //Analyses routes 
  { path: "/collecte/analyseschimiques", exact: true, name: "Analyses physico-chimiques", component: AnalysesChimiques },
  { path: "/collecte/analyseschimiques/add", exact: true, name: "Nouvelle analyse physico-chimique", component: AnalyseChimiquesCreate },
  { path: "/collecte/analyseschimiques/edit/:id", exact: true, name: "Édition", component: AnalyseChimiquesCreate },

  { path: "/collecte/analysesbacteriologiques", exact: true, name: "Analyses bactériologiques", component: AnalyseBacteriologies },
  { path: "/collecte/analysesbacteriologiques/add", exact: true, name: "Nouvelle analyse bactériologique", component: AnalyseBacteriologiesCreate },
  { path: "/collecte/analysesbacteriologiques/edit/:id", exact: true, name: "Édition", component: AnalyseBacteriologiesCreate },

  { path: "/collecte/analysesisotopiques", exact: true, name: "Analyses Isotopiques", component: AnalysesIsotopiques },
  { path: "/collecte/analysesisotopiques/add", exact: true, name: "Nouvelle analyse isotopique", component: AnalyseIsotopiqueCreate },
  { path: "/collecte/analysesisotopiques/edit/:id", exact: true, name: "Edition", component: AnalyseIsotopiqueCreate },

  { path: "/collecte/analysespesticides", exact: true, name: "Analyses Résidus des pesticides", component: AnalysesPesticides },
  { path: "/collecte/analysespesticides/add", exact: true, name: "Nouvelle analyse résidus des pesticides", component: AnalysePesticideCreate },
  { path: "/collecte/analysespesticides/edit/:id", exact: true, name: "Édition", component: AnalysePesticideCreate },
  // Rapports
  { path: "/rapports/analyseschimiques", exact: true, name: "Rapport analyses physico-chimiques", component: RapportAnalysesChimiques },
  { path: "/rapports/analyseschimiquesprint/:id", exact: true, name: "Rapport analyses physico-chimiques", component: RapportAnalysesChimiquesPrint },
  { path: "/rapports/bacteriologiques", exact: true, name: "Rapport analyses bacteriologiques", component: RapportAnalysesBacteriologique },
  { path: "/rapports/bacteriologiquesprint/:id", exact: true, name: "Rapport analyses bacteriologiques", component: RapportAnalysesBacteriologiquePrint },

  // Piezometrie 
  { path: "/collecte/piezometries", exact: true, name: "Piézométrie", component: Piezometrie },
  { path: "/collecte/piezometries/add", exact: true, name: "Nouveau", component: PiezometrieCreate },
  { path: "/collecte/piezometries/edit/:id", exact: true, name: "Édition", component: PiezometrieCreate },
  // Pluviometrie  
  { path: "/collecte/pluviometries", exact: true, name: "Pluviométrie", component: Pluviometrie },
  { path: "/collecte/pluviometries/add", exact: true, name: "Nouveau", component: PluviometrieCreate },
  { path: "/collecte/pluviometries/edit/:id", exact: true, name: "Édition", component: PluviometrieCreate },
  // Hydrometrie 
  { path: "/collecte/hydrometries", exact: true, name: "Hydrométrie", component: Hydrometrie },
  { path: "/collecte/hydrometries/add", exact: true, name: "Nouveau", component: HydrometrieCreate },
  { path: "/collecte/hydrometries/edit/:id", exact: true, name: "Édition", component: HydrometrieCreate },
  // parameters routes
  { path: "/parametre/nappes", exact: true, name: "Nappes", component: Nappes },
  { path: "/parametre/nappes/add", exact: true, name: "Nouveau", component: NappeCreate },
  { path: "/parametre/nappes/edit/:id", exact: true, name: "Édition", component: NappeCreate },
  // Laboratoires
  { path: "/parametre/laboratoires", exact: true, name: "Laboratoires", component: Laboratoires },
  { path: "/parametre/laboratoires/add", exact: true, name: "Nouveau", component: LaboratoireCreate },
  { path: "/parametre/laboratoires/edit/:id", exact: true, name: "Édition Laboratoire", component: LaboratoireCreate },
  // coupes geologiques
  { path: "/parametre/coupegeologiques", exact: true, name: "Coupes géologiques", component: Coupegeologiques },
  { path: "/parametre/coupegeologiques/add", exact: true, name: "Nouveau", component: CoupegeologiqueCreate },
  { path: "/parametre/coupegeologiques/edit/:id", exact: true, name: "Édition", component: CoupegeologiqueCreate },
  // type ouvrages
  { path: "/parametre/typeouvrages", exact: true, name: "Type ouvrages", component: Typeouvrages },
  { path: "/parametre/typeouvrages/add", exact: true, name: "Nouveau", component: TypeouvrageCreate },
  { path: "/parametre/typeouvrages/edit/:id", exact: true, name: "Édition Type Ouvrage", component: TypeouvrageCreate },
  // code lithologique
  { path: "/parametre/codelithologique", exact: true, name: "Code lithologique", component: Codelithologique },
  { path: "/parametre/codelithologique/add", exact: true, name: "Nouveau", component: CodelithologiqueCreate },
  { path: "/parametre/codelithologique/edit/:id", exact: true, name: "Édition", component: CodelithologiqueCreate },
  // elements chimiques
  { path: "/parametre/elementchimiques", exact: true, name: "Éléments chimiques", component: Elementchimiques },
  { path: "/parametre/elementchimiques/add", exact: true, name: "Nouveau", component: ElementchimiqueCreate },
  { path: "/parametre/elementchimiques/edit/:id", exact: true, name: "Édition", component: ElementchimiqueCreate },
  // Peiode de collecte
  { path: "/parametre/enquetes", exact: true, name: "Enquêtes", component: Enquetes },
  { path: "/parametre/enquetes/edit/:id", exact: true, name: "Édition", component: EnqueteForm },
  { path: "/parametre/enquetes/validation/:id", exact: true, name: "Validation", component: EnqueteValidation },
  // Zones geographiques
  { path: "/parametre/regions", exact: true, name: "Régions", component: Regions },
  { path: "/parametre/regions/edit/:id", exact: true, name: "Édition", component: Regions },
  { path: "/parametre/regions/departements/:parent_id", exact: true, name: "Départements", component: Departements },
  { path: "/parametre/regions/departement/edit/:parent_id/:id", exact: true, name: "Édition", component: Departements },
  { path: "/parametre/regions/departements/communes/:parent_id", exact: true, name: "Communes", component: Communes },
  { path: "/parametre/regions/departements/commune/edit/:parent_id/:id", exact: true, name: "Édition", component: Communes },
  // Stations de suivi
  { path: "/parametre/stations", exact: true, name: "Stations", component: Stations },
  { path: "/parametre/stations/add", exact: true, name: "Nouveau", component: StationCreate },
  { path: "/parametre/stations/edit/:id", exact: true, name: "Édition Station", component: StationCreate },

  // Requetes 
  { path: "/requetes/mesures", exact: true, name: "Requêtes sur les mesures", component: RequeteMesures },
  { path: "/requetes/analyses", exact: true, name: "Requêtes sur les analyses", component: RequeteAnalyses },
  // Graphiques 
  { path: "/graphiques/piezometries", exact: true, name: "Piézométrie", component: GraphiquePiezometrie },
  { path: "/graphiques/hydrometries", exact: true, name: "Hydrométrie", component: GraphiqueHydrometrie },
  { path: "/graphiques/pluviometries", exact: true, name: "Pluviométrie", component: GraphiquePluviometrie },

  // Cartes 
  { path: "/cartes/statiques", exact: true, name: "Cartes statiques", component: CarteStatiques },
  { path: "/cartes/statiques/add", exact: true, name: "Nouveau", component: CarteStatiquesCreate },
  { path: "/cartes/statiques/edit/:id", exact: true, name: "Nouveau", component: CarteStatiquesEdit },
  { path: "/cartes/statiques/show/:id", exact: true, name: "Nouveau", component: CarteStatiquesShow },
  { path: "/cartes/dynamiques", exact: true, name: "Cartes dynamiques", component: CarteMenu },
  { path: "/cartes/dynamiques/stations", exact: true, name: "Stations de suivi", component: CarteStationSuivi },
  //Users routes
  { path: "/users", exact: true, name: "Utilisateurs", component: Users },
  { path: "/users/add", exact: true, name: "Nouvel utilisateur", component: UserCreate },
  { path: "/users/show", exact: true, name: "Utilisateur details", component: UserCreate },
  { path: "/users/edit/:id", exact: true, name: "Édition utilisateur", component: UserCreate },
  { path: "/users/reset/:id", exact: true, name: "Mot de passe", component: UserPasswordReset },
  { path: "/users/site/:id", exact: true, name: "Choix du site", component: SelectSite }

];

export default routes;
