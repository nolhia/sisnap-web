
const dev = { 
    backendUrl: "http://127.0.0.1:4002/api/v1" 
}

const prod = { 
    backendUrl: "http://167.114.2.200:4002/api/v1"
}

const config = process.env.REACT_APP_STAGE === 'production'
  ? prod
  : dev;
 
export default { 
  ...config
};