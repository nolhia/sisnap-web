/*
  Le conteneur qui pr/sente le corps de l'application
*/
import React, { Component, Suspense } from 'react';
import { Redirect, Switch } from 'react-router-dom';
import { Container } from 'reactstrap';
import { PrivateRoute } from '../../components';   
import { AppBreadcrumb, AppFooter, AppHeader, AppSidebar, AppSidebarFooter,
  AppSidebarForm, AppSidebarHeader, AppSidebarMinimizer, AppSidebarNav } from '@coreui/react';
import { userService, parametersService } from '../../services';
// sidebar nav config
import navigation_collecte from '../../_nav_collecte';
import navigation_admin from '../../_nav_admin'; // menu admin
import navigation_consultation from '../../_nav_consultation'; // menu consultation
import navigation_help from '../../_nav_help'; // menu aide

// routes config
import routes from '../../routes'; 

const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

class DefaultLayout extends Component {
  // constructeur
  constructor(props) {
      super(props);
      this.state = { 
          menu:  navigation_consultation
      };  
  }
  // initialisation du composant
  async componentDidMount() {
      let menu = navigation_consultation.items;
      const menu_help = navigation_help.items; 
      const menu_admin = navigation_admin.items; 
      const menu_collecte = navigation_collecte.items; 
      const user = await parametersService.getUserConnected();
      if(user && user.user_info ){ 
        if(user.user_info.role <= 3){
          menu = menu.concat(menu_collecte);
        }
        if(user.user_info.role === 1){
          menu = menu.concat(menu_admin);
        }
      }
      menu = menu.concat(menu_help);  
      this.setState({menu: {items: menu}}) 
  } 

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  // deconnexion
  signOut(e) {
    e.preventDefault()
    userService.logout();  
    this.props.history.push('/login')
  }

  render() {
     
    return (
      <div className="app">
        <AppHeader fixed>
          <Suspense  fallback={this.loading()}>
            <DefaultHeader onLogout={e=>this.signOut(e)}/>
          </Suspense>
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            <Suspense>
            <AppSidebarNav navConfig={this.state.menu} {...this.props} />
            </Suspense>
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            <AppBreadcrumb appRoutes={routes}/>
            <Container fluid>
              <Suspense fallback={this.loading()}>
                <Switch> 
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <PrivateRoute key={idx} path={route.path} exact={route.exact}  name={route.name} component={route.component} /> 
                    ) : (null);
                  })}
                  <Redirect from="/" to="/dashboard" />
                </Switch>

              </Suspense>
            </Container>
          </main> 
        </div>
        <AppFooter>
          <Suspense fallback={this.loading()}>
            <DefaultFooter />
          </Suspense>
        </AppFooter>
      </div>
    );
  }
}

export default DefaultLayout;
