/*
  Entete de l'application
*/
import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom"; 
import { DropdownItem, DropdownMenu, DropdownToggle, Nav, NavItem } from "reactstrap";
import PropTypes from "prop-types";

import { AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from "@coreui/react";
import logo from "../../assets/img/brand/logo.png";
import sygnet from "../../assets/img/brand/sygnet.svg";

import { parametersService } from '../../services';

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {
  
  constructor(props) {
      super(props); 
      this.state = {
          currentUser: null 
      };  
  }

  async componentDidMount() {
    const user = await parametersService.getUserConnected();
    this.setState({ currentUser: user })
  }
  
  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    const { currentUser } = this.state;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 35, alt: "SISNA" }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: "SISNA" }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          <h5 className="text-center">
              SYSTÈME D'INFORMATION DE SUIVI DES NAPPES ALLUVIALES
          </h5>
        </Nav>
        <Nav className="ml-auto" navbar>
          <NavItem className="px-3">
              {currentUser && 
              <NavLink to="/dashboard" className="nav-link">
                    Connecté, { currentUser.user_info.prenoms+ ' '+currentUser.user_info.nom}
              </NavLink>
              }
          </NavItem> 
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
            <i className="icon-menu" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: "auto" }}>
              <DropdownItem header tag="div" className="text-center">
                <strong>Compte</strong>
              </DropdownItem>
              <DropdownItem>
                {currentUser &&
                  <Link to={'/users/site/'+currentUser.user_info.id}><i className="fa fa-building" /> Choix du site</Link>
                }
              </DropdownItem>
              <DropdownItem>
                {currentUser &&
                  <Link to={'/users/reset/'+currentUser.user_info.id}><i className="fa fa-key" /> Mot de passe</Link>
                }
              </DropdownItem>
               
              <DropdownItem onClick={e => this.props.onLogout(e)}>
                <i className="fa fa-plug" /> Se deconnecter
              </DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
