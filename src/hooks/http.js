import { useState, useEffect } from 'react';
import { baseUrl, authHeader } from '../helpers';

// requetes http
export const useHttp = (url, dependencies) => {
  const [isLoading, setIsLoading] = useState(false);
  const [fetchedData, setFetchedData] = useState(null);

  //   fetch('https://swapi.co/api/people')
  useEffect(() => {
    setIsLoading(true); 
    fetch(`${baseUrl()}${url}`, {
        method: 'get', 
        headers: authHeader(),
      })
      .then(response => {
        if (!response.ok) {
          throw new Error('Failed to fetch.');
        }
        return response.json();
      })
      .then(data => {
        setIsLoading(false);
        setFetchedData(data);
      })
      .catch(err => {
        console.log(err);
        setIsLoading(false);
      });
  }, dependencies);

  return [isLoading, fetchedData];
};

// requetes http de type suppression
export const useDeleteHttp = (url, dependencies) => {
    const [isLoading, setIsLoading] = useState(false);
    const [fetchedData, setFetchedData] = useState(null);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        setIsLoading(true);
        console.log('Sending Http request to delete URL: ' + url);
        fetch(`${baseUrl()}${url}`, {
            method: 'delete', 
            headers: authHeader(),
          })
          .then(response => {
            if (!response.ok) {
                setIsError(true);
              throw new Error('Failed to fetch.');
            }
            return response.json();
          })
          .then(data => {
            setIsLoading(false);
            setFetchedData(data);
          })
          .catch(err => {
            console.log(err);
            setIsLoading(false);
          });
      }, dependencies);
    return [isLoading, fetchedData];
};  