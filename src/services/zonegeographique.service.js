
import { callMaker } from '../helpers'; 
export const zonegeographiqueService = {
    getAll,
    getRegions,
    getDepartements,
    getCommunes,
    saveRegion,
    saveDepartement,
    saveCommune,
    removeRegion,
    removeDepartement,
    removeCommune,
    findRegion,
    findDepartement,
    findCommune 
}
// retourner toute la liste
async function getAll() {
    try {
        return await callMaker('get', `/parametre/regions/all`)
    } catch (error) {
        console.error(error);
    }
}
// retourner les regions
async function getRegions() {
    try {
        return await callMaker('get', `/parametre/regions`)
    } catch (error) {
        console.error(error);
    }
}
// retourner les departements
async function getDepartements(id) {
    try {
        return await callMaker('get', `/parametre/regions/departements/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// retourner les communes
async function getCommunes(id) {
    try {
        return await callMaker('get', `/parametre/regions/departements/communes/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// retourner une region
async function findRegion(id) {
    try {
        return await callMaker('get', `/parametre/regions/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// retourner un departement
async function findDepartement(id) {
    try {
        return await callMaker('get', `/parametre/regions/departement/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// retourner une commune
async function findCommune(id) {
    try {
        return await callMaker('get', `/parametre/regions/departements/commune/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// enregistrer une region
async function saveRegion(objet) {
    try {
        if (objet && objet.id) {
            return callMaker('put', '/parametre/regions', objet);
        } else {
            return callMaker('post', '/parametre/regions', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// enregistrer un departement
async function saveDepartement(objet) {
    try {
        if (objet && objet.id) {
            return callMaker('put', '/parametre/regions/departements', objet);
        } else {
            return callMaker('post', '/parametre/regions/departements', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// enregistrer une commune
async function saveCommune(objet) {
    try {
        if (objet && objet.id) {
            return callMaker('put', '/parametre/regions/departements/communes', objet);
        } else {
            return callMaker('post', '/parametre/regions/departements/communes', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// supprimer une region
async function removeRegion(objet) {
    try {
        return await callMaker('delete', `/parametre/regions/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
// supprimer un departement
async function removeDepartement(objet) {
    try {
        return await callMaker('delete', `/parametre/regions/departements/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
// supprimer une commune
async function removeCommune(objet) {
    try {
        return await callMaker('delete', `/parametre/regions/departements/communes/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
