//import config from 'config';
import { BehaviorSubject } from 'rxjs'; 
import { baseUrl, callMaker } from '../helpers';

const currentUserSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('user')));

export const userService = {
    login,
    logout,
    resetPassword,
    forgotPassword,
    currentUser: currentUserSubject.asObservable(),
    get currentUserValue () { return currentUserSubject.value }, 
    getAll,
    save,
    remove,
    find
};
// se connecter
function login(email, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password })
    };

    return fetch(`${baseUrl()}/auth/authenticate`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a user in the response
            if (user) {
                // store user details and basic auth credentials in local storage 
                // to keep user logged in between page refreshes
                user.authdata = window.btoa(email + ':' + password);
                localStorage.setItem('user', JSON.stringify(user));
                
            }

            return user;
        });
}
// se deconnecter
function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user'); 
    currentUserSubject.next(null);
}
// retourner la liste des utilisateurs
async function getAll() {
    try {
        return await callMaker('get', '/users')
    } catch (error) {
      console.error(error);
    }
}
// retourner un utilisateur
async function find(id) {
    try {
        return await callMaker('get', `/users/${id}`)
    } catch (error) {
      console.error(error);
    }
}
// enregistrer ou modifier un utilisateur
function save(user) { 
    if (user && user.id) {
        return callMaker('put', '/users', user);
    } else {
        return callMaker('post', '/users', user);
    }  
}
// supprimer un utilisateur
async function remove(user) {
    try {
        return await callMaker('put', `/users/${user.id}`, user); 
    } catch (error) {
      console.error(error);
    }
}

function handleResponse(response) {
    
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (data.status !== 200) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                //logout();
                //window.location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }
        
        return data;
    });
}
// signaler un mot de passe perdu
async function forgotPassword(email, mot_de_passe) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, mot_de_passe })
    };
    
    return fetch(`${baseUrl()}/auth/forgotpassword`, requestOptions)
        .then(handleResponse)
        .then(user => { 
            currentUserSubject.next(user);

            return user;
        });
}
// reinitialiser un mot de passe
async function resetPassword(telephone, mot_de_passe) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ telephone, mot_de_passe })
    };

    return fetch(`${baseUrl()}/auth/passwordreset`, requestOptions)
        .then(handleResponse)
        .then(user => {
            // login successful if there's a user in the response
            if (user) {
                // store user details and basic auth credentials in local storage 
                // to keep user logged in between page refreshes
                user.authdata = window.btoa(telephone + ':' + mot_de_passe);
                localStorage.setItem('user', JSON.stringify(user));
            }
            currentUserSubject.next(user);

            return user;
        });
}