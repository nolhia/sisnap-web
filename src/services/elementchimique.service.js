
import { callMaker } from '../helpers';

export const elementchimiqueService = {
    getAll,
    save,
    remove,
    find,
    getByAnalyse,
};
// retourner tout
async function getAll() {
    try {
        return await callMaker('get', '/parametre/elementchimiques')
    } catch (error) {
        console.error(error);
    }
}
// trouver un
async function find(id) {
    try {
        return await callMaker('get', `/parametre/elementchimiques/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// sauvegarder
async function save(objet) {
    try {
        if (objet && objet.id) {
            return callMaker('put', '/parametre/elementchimiques', objet);
        } else {
            return callMaker('post', '/parametre/elementchimiques', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// supprimer
async function remove(objet) {
    try {
        return await callMaker('delete', `/parametre/elementchimiques/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
// recherche par type d'analyse
async function getByAnalyse(analyseType) {
    try {
        return await callMaker('get', `/parametre/elementchimiques/analyseType/${analyseType}`);
    } catch (error) {
        console.error(error);
    }
}
