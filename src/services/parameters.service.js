
import { asyncLocalStorage } from '../helpers';
const jsonData = require('../data/data.json');

export const parametersService = {
    getTypeAnalyse,
    getTypeMesure,
    getTypeCarte,
    getTypeEnregistreur,
    getRoles,
    getNiveauEntrees,
    getUserStatus,
    getEnqueteStatus,
    getUserInformation,
    getUserConnected,
    getTypeNappes,
    getStatusStations,
    getEnqueteTypes,
    getNormes
};
// retourner les types d'analyse
async function getTypeAnalyse() {
    try {
        return jsonData.type_analyse;
    } catch (error) {
        console.error(error);
    }
}
// retourner  les types de mesures
async function getTypeMesure() {
    try {
        return jsonData.type_mesure;
    } catch (error) {
        console.error(error);
    }
}
// retourner les types de cartes
async function getTypeCarte() {
    try {
        return jsonData.type_carte;
    } catch (error) {
        console.error(error);
    }
}
// retourner les types d'enregistreur
async function getTypeEnregistreur() {
    try {
        return jsonData.type_enregistreurs;
    } catch (error) {
        console.error(error);
    }
}
// retourner les roles
async function getRoles() {
    try {
        return jsonData.role_utilisateur;
    } catch (error) {
        console.error(error);
    }
}
// retourner les niveaux d'entree
async function getNiveauEntrees() {
    try {
        return jsonData.niveau_utilisateur;
    } catch (error) {
        console.error(error);
    }
}
// retourner les status des utilisateurs
async function getUserStatus() {
    try {
        return jsonData.status_utilisateur;
    } catch (error) {
        console.error(error);
    }
}
// retourner les status des enquetes
async function getEnqueteStatus() {
    try {
        return jsonData.status_enquete;
    } catch (error) {
        console.error(error);
    }
}
// retourner les types d'enquetes
async function getEnqueteTypes() {
    try {
        return jsonData.type_enquete;
    } catch (error) {
        console.error(error);
    }
}
// retourner la liste des normes statiques
async function getNormes() {
    try {
        return jsonData.normes;
    } catch (error) {
        console.error(error);
    }
}
// retourner les informations sur un utilisateur
async function getUserInformation() {
    try {
        const user = await asyncLocalStorage.getItem('user');
        return user.user_info;
    } catch (error) {
        console.error(error);
    }
}
// retourner les informations sur un utilisateur connecte
async function getUserConnected() {
    try {
        const user = await asyncLocalStorage.getItem('user');
        return user;
    } catch (error) {
        console.error(error);
    }
}
// retourner les types de nappes
async function getTypeNappes() {
    try {
        return jsonData.type_nappes;
    } catch (error) {
        console.error(error);
    }
}
// retourner les status de stations
async function getStatusStations() {
    try {
        return jsonData.status_station;
    } catch (error) {
        console.error(error);
    }
}