import { callMaker } from '../helpers';

export const typeStationService = {
    save,
    getAll,
    find
};
// enregistrer
function save(typeStation) {
    if (typeStation && typeStation.id) {
        return callMaker('put', '/typestations', typeStation);
    } else {
        return callMaker('post', '/typestations', typeStation);
    }
}
// retourner tous
async function getAll() {
    try {
        return await callMaker('get', '/typestations')
    } catch (error) {
        console.error(error);
    }
}
// trouver un
async function find(id) {
    try {
        return await callMaker('get', `/typestations/${id}`)
    } catch (error) {
        console.error(error);
    }
}