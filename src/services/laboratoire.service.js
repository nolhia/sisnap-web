
import { callMaker } from '../helpers';

export const laboratoireService = {
    getAll,
    save,
    remove,
    find
};
// retourner tout
async function getAll() {
    try {
        return await callMaker('get', '/parametre/laboratoires')
    } catch (error) {
        console.error(error);
    }
}
// trouver un
async function find(id) {
    try {
        return await callMaker('get', `/parametre/laboratoires/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// enregistrer   
async function save(objet) {
    try {
        if (objet && objet.id) {
            return callMaker('put', '/parametre/laboratoires', objet);
        } else {
            return callMaker('post', '/parametre/laboratoires', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// supprimer
async function remove(objet) {
    try {
        return await callMaker('delete', `/parametre/laboratoires/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
