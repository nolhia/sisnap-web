import { callMaker } from '../helpers';

export const piezometrieService = {
    getAll,
    find
};
// retourner tout
async function getAll() {
    try {
        return await callMaker('get', '/collecte/piezometrie')
    } catch (error) {
        console.error(error);
    }
}
// trouver un
async function find(id) {
    try {
        return await callMaker('get', `/collecte/piezometrie/${id}`)
    } catch (error) {
        console.error(error);
    }
}