
import { callMaker } from '../helpers';

export const nappeService = {
    getAll,
    save,
    remove,
    find
};
// retourner tous
async function getAll() {
    try {
        return await callMaker('get', '/parametre/nappes')
    } catch (error) {
        console.error(error);
    }
}
// trouver un
async function find(id) {
    try {
        return await callMaker('get', `/parametre/nappes/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// enregistrer   
async function save(objet) {
    try {
        if (objet && objet.id) {
            return callMaker('put', '/parametre/nappes', objet);
        } else {
            return callMaker('post', '/parametre/nappes', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// supprimer
async function remove(objet) {
    try {
        return await callMaker('delete', `/parametre/nappes/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
