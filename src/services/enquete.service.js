
import { callMaker } from '../helpers';

export const enqueteService = {
    getAll,
    save,
    remove,
    find,
    updateStatus
};

async function getAll() {
    try {
        return await callMaker('get', '/parametre/enquetes')
    } catch (error) {
        console.error(error);
    }
}

async function find(id) {
    try {
        return await callMaker('get', `/parametre/enquetes/${id}`)
    } catch (error) {
        console.error(error);
    }
}
   
async function save(objet) {
    try {
        
        if (objet && objet.id && objet.id !==0 && objet.id !== '') {  
            return callMaker('put', '/parametre/enquetes', objet);
        } else { 
            
            return callMaker('post', '/parametre/enquetes', objet);
        }
    } catch (error) {
        console.error(error);
    }
}

async function updateStatus(objet) {
    try { 
        return callMaker('put', '/parametre/enquetes/validate', objet); 
    } catch (error) {
        console.error(error);
    }
}

async function remove(objet) {
    try {
        return await callMaker('delete', `/parametre/enquetes/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
