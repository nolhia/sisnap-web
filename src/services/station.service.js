import { callMaker } from '../helpers';

export const stationService = {
    save,
    getAll,
    find,
    remove,
    exportData
};
// enregistrer
function save(station) {
    if (station && station.id) {
        return callMaker('put', '/stations', station);
    } else {
        return callMaker('post', '/stations', station);
    }
}
// retourner tous
async function getAll() {
    try {
        return await callMaker('get', '/stations')
    } catch (error) {
        console.error(error);
    }
}
// retourner un
async function find(id) {
    try {
        return await callMaker('get', `/stations/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// exporter les donnees
async function exportData() {
    try {
        return await callMaker('post', '/stations/export')
    } catch (error) {
        console.error(error);
    }
}
// supprimer
async function remove(objet) {
    try {
        return await callMaker('delete', `/stations/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
