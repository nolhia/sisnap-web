import { callMaker } from '../helpers';
import { parametersService } from '../services';

export const analysechimiquesService = {
    getAll,
    save,
    remove,
    find,
    exportData
};

// trouver tout
async function getAll() {
    try {
        return await callMaker('get', '/collecte/analyse/chimiques')
    } catch (error) {
        console.error(error);
    }
}
// trouver un
async function find(id) {
    try {
        return await callMaker('get', `/collecte/analyse/chimiques/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// sauvegarder
async function save(objet) {
    try {
        const user = await parametersService.getUserInformation(); 
        if (objet && objet.id) {
            if(user){
                objet['modifier_par'] = user.id;
                objet['modifier_le'] = new Date();
            }
            return callMaker('put', '/collecte/analyse/chimiques', objet);
        } else {
            if(user){
                objet['ajouter_par'] = user.id;
            }
            return callMaker('post', '/collecte/analyse/chimiques', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// supprimer
async function remove(objet) {
    try {
        return await callMaker('delete', `/collecte/analyse/chimiques/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
// exporter
async function exportData() {
    try {
        const result =  await callMaker('post', '/collecte/analyse/chimiques/export')
        if(result && result.length > 0){
            return result;
        }
        return [];
    } catch (error) {
        console.error(error);
    }
}