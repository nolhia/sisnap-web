
import { callMaker } from '../helpers';

export const coupegeologiqueService = {
    getAll,
    save,
    remove,
    find,
    getCodesLithologiques
};
// trouver tout
async function getAll() {
    try {
        return await callMaker('get', '/parametre/coupegeologiques')
    } catch (error) {
        console.error(error);
    }
}
// trouver un
async function find(id) {
    try {
        return await callMaker('get', `/parametre/coupegeologiques/one/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// sauvegarder   
async function save(objet) {
    try {
        if (objet && objet.id) {
            return callMaker('put', '/parametre/coupegeologiques', objet);
        } else {
            return callMaker('post', '/parametre/coupegeologiques', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// supprimer
async function remove(objet) {
    try {
        return await callMaker('delete', `/parametre/coupegeologiques/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
// retourner les code lithologiques
async function getCodesLithologiques() {
    try {
        return await callMaker('get', '/parametre/coupegeologiques/codeslithologiques')
    } catch (error) {
        console.error(error);
    }
}
