import { callMaker } from '../helpers';

export const typeOuvrageService = {
    save,
    getAll,
    find
};

// enregistrer
function save(typeOuvrage) {
    if (typeOuvrage && typeOuvrage.id) {
        return callMaker('put', '/typeouvrages', typeOuvrage);
    } else {
        return callMaker('post', '/typeouvrages', typeOuvrage);
    }
}
// retourner tous
async function getAll() {
    try {
        return await callMaker('get', '/typeouvrages')
    } catch (error) {
        console.error(error);
    }
}
// retourner un
async function find(id) {
    try {
        return await callMaker('get', `/typeouvrages/${id}`)
    } catch (error) {
        console.error(error);
    }
}