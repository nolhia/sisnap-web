import { callMaker } from '../helpers';
import { parametersService } from '../services';

export const bacteriologieService = {
    getAll,
    save,
    remove,
    find,
    exportData
};
// trouver tout
async function getAll() {
    try {
        return await callMaker('get', '/collecte/analyse/bacteriologie')
    } catch (error) {
        console.error(error);
    }
}
// trouver un
async function find(id) {
    try {
        return await callMaker('get', `/collecte/analyse/bacteriologie/${id}`)
    } catch (error) {
        console.error(error);
    }
}
// enregistrer
async function save(objet) {
    try {
        const user = await parametersService.getUserInformation(); 
        if (objet && objet.id) {
            if(user){
                objet['modifier_par'] = user.id;
                objet['modifier_le'] = new Date();
            }
            return callMaker('put', '/collecte/analyse/bacteriologie', objet);
        } else {
            if(user){
                objet['ajouter_par'] = user.id;
            }
            return callMaker('post', '/collecte/analyse/bacteriologie', objet);
        }
    } catch (error) {
        console.error(error);
    }
}
// supprimer
async function remove(objet) {
    try {
        return await callMaker('delete', `/collecte/analyse/bacteriologie/${objet.id}`);
    } catch (error) {
        console.error(error);
    }
}
// exporter tout
async function exportData() {
    try {
        const result = await callMaker('post', '/collecte/analyse/bacteriologie/export');
        if(result && result.length > 0){
            return result;
        }
        return [];
    } catch (error) {
        console.error(error);
    }
}