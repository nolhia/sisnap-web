// Menu du guide utilisateur
export default {
  items: [ 
    {
      divider: true
    },
    {
      title: true,
      name: 'Aides'
    },
    {
      name: 'Guide utilisateur',
      url:
        'https://drive.google.com/file/d/1shw2COd94T6Ot7xtHjWX-ofLlfnZ1HVw/view?usp=sharing',
      icon: 'icon-star',
      class: 'mt-auto',
      variant: 'success',
      attributes: { target: '_blank', rel: 'noopener' }
    }
  ]
};
