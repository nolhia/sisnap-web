import React, { Component } from 'react';
import { Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import { parametersService } from '../../services';
import { callMaker, asyncLocalStorage, getParameterValueById } from '../../helpers'; 
 

class Dashboard extends Component {
  constructor(props) {
      super(props);    
      this.state = {
          region_id: '',
          departement_id: '',
          enquete_id: '',
          niveau_entree: '', 
          loading: false 
      }; 
  }
// initialiser
  async componentDidMount() {   
      const niveaux = await parametersService.getNiveauEntrees();
      const enquetes = await callMaker('get', `/parametre/enquetes`);
      const parametre = await asyncLocalStorage.getItem('parametre'); 
      const zones = await asyncLocalStorage.getItem('zonegeographique'); 
       
      if(zones !== ''){ 
          this.setState({ 
            region: getParameterValueById(zones.regions, parametre.region_id), 
            departement: getParameterValueById(zones.departements, parametre.departement_id)
          });
      } 

      this.setState({   
          niveau_entree: getParameterValueById(niveaux, parametre.niveau_entree),
          enquete: getParameterValueById(enquetes, parametre.enquete_id)
      });
  }
  
  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>
// Affichage
  render() {
    const { niveau_entree, region, departement, enquete } = this.state;
    return (
      <div className="animated fadeIn"> 
        <Card>
            <CardHeader  style={{color: '#246195'}}>
            <i className="fa fa-align-justify"></i><span >Informations sur la connexion</span> 
            </CardHeader>
            <CardBody>  
                <Row>
                    <Col xs="3"> 
                        <span style={{color: '#246195'}}>Niveau d'entrée: </span> {niveau_entree}
                    </Col>
                    <Col xs="3"> 
                      <span style={{color: '#246195'}}>Région:</span> {region} 
                    </Col>
                    <Col xs="3"> 
                      <span style={{color: '#246195'}}>Département:</span> {departement} 
                    </Col>
                    <Col xs="3"> 
                      <span style={{color: '#246195'}}>Enquête: </span> {enquete}
                    </Col>
                </Row> 
            </CardBody>  
        </Card>
  
      </div>
    );
  }
}

export default Dashboard;
