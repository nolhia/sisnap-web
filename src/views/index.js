import Dashboard from './Dashboard';  
import { Login, Page404, Page500 } from './Pages'; 

export { 
  Page404,
  Page500, 
  Login, 
  Dashboard
};

