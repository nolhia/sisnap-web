import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, Row, Table, FormGroup, Input, Label , Form } from 'reactstrap';
import { userService, zonegeographiqueService } from '../../../services';
 // lignes du tableau
function RegionRow(props) {
  const objet = props.region
  const objetLink = `/parametre/regions/departements/${objet.id}`;
  const objetEditLink = `/parametre/regions/edit/${objet.id}`;
  
  return (
    <tr key={objet.id.toString()}>
      <th scope="row"><Link to={objetLink}>{objet.code}</Link></th>
      <td>{objet.titre}</td>  
      <td>
        <Link to={objetEditLink}><i className="icon-note"></i></Link>
      </td>
    </tr>
  )
}

class Regions extends Component {
  
  constructor(props){
      super(props);
      this.state = { 
          isLoaded : false,
          data : [],
          id: '',
          code: '',
          titre: '', 
          mode_edition: 'no',
          msg: '',
          error: ''         
      };  

      this.handleSubmit = this.handleSubmit.bind(this);
      this.baseState = this.state;
      this.handleChange = this.handleChange.bind(this);
      this.resetForm = this.resetForm.bind(this);
  }
// initialiser
  async componentDidMount() { 
    const data = await zonegeographiqueService.getRegions();  
    if (this.props.match.params.id) {  
      // region to update
      const objet = await zonegeographiqueService.findRegion(this.props.match.params.id);
      this.setState({ mode_edition: 'yes', ...objet });
      this.baseState = this.state;
    }
    this.setState({ isLoaded: true, data: data });
  }  

// soumission de la requete
  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      id: event.target.id.value,
      mode_edition: event.target.mode_edition.value,
      code: event.target.code.value,
      titre: event.target.titre.value 
    }
    if (!this.validateForm(data)) {
      console.log("Form Validation Failed => " + data)
      return;
    }

    if (data.id === '') {
      delete data['id'];
    }

    delete data['mode_edition'];

    this.setState({ loading: true });

    zonegeographiqueService.saveRegion(data)
      .then(
        response => {
          if (response.status === 401) {
            this.setState({ error: response.message, loading: false });
            const { from } = this.props.location.state || { from: { pathname: "/" } };
            this.props.history.push(from);
            userService.logout();
            window.location.reload(true);
          } else if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => this.setState({ error, loading: false })
      );
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
// valider
  validateForm(data) {
    let message = "";
    this.setState({ error: message });

    return message === "" ? true : false;
  }
// reinitialiser
  resetForm = () => {
    this.setState(() => this.baseState)
  }
  
  render() {
    const { isLoaded, data, id, code, titre, mode_edition, error, msg } = this.state; 
    const listLink = `/parametre/regions`;

    if (!isLoaded) {
        return <div>Loading ...</div>
    } else{
      return (
        <div className="animated fadeIn"> 
          <br />
          {error &&
            <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
            <div className={'alert alert-success'}>{msg}</div>
          }
          <Form onSubmit={this.handleSubmit} method="POST">
            <Row>
              <Col>
                <Card>
                  <CardHeader>
                    <i className="fa fa-align-justify"></i>
                    <div className="card-header-actions">
                      <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button> 
                      &nbsp;&nbsp;<Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                      &nbsp;&nbsp; <Link to={listLink} ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                    </div>
                  </CardHeader>
                  <CardBody> 
                    <Row>
                      <Col xs="3">
                        <FormGroup>
                        <Label htmlFor="code">Code *</Label>
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id'/> 
                        <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition'/>
                        <Input type="text" value={code} onChange={this.handleChange} id="code" name="code" required />
                        </FormGroup>
                      </Col>
                      <Col xs="9">
                        <FormGroup>
                        <Label htmlFor="titre">Libellé *</Label>
                        <Input type="text" value={titre} onChange={this.handleChange} id="titre" name="titre" required />
                        </FormGroup>
                      </Col> 
                    </Row>    
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Form>
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> Régions 
                </CardHeader>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">Code</th>
                        <th scope="col">Libelle</th>  
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data && data.length > 0 && data.map((region, index) =>
                        <RegionRow key={index} region={region}/>
                      )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }
}

export default Regions;
