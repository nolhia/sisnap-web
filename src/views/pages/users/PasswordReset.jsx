import React, { Component } from 'react'; 
import { Card, CardBody, CardFooter, CardHeader, Col, Form, FormGroup, Input, Label, Row } from 'reactstrap';
import { parametersService } from '../../../services';
import { callMaker } from '../../../helpers';

class PasswordReset extends Component {
    constructor(props) {
        super(props);   

        this.state = {
            id: '', 
            password: '',
            password_confirm: '',
            submitted: false,
            loading: false,
            error: '',
            msg: ''
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this); 
    }

    async componentDidMount() {   
        const user = await parametersService.getUserConnected();
        this.setState({ currentUser: user })
        if(this.props.match.params.id){ 
            this.setState({ id: this.props.match.params.id });
        } 
         
    }
  
    handleSubmit(e) {
        e.preventDefault(); 
        this.setState({ submitted: true }); 
        const data = {
            id: e.target.id.value, 
            token: this.state.currentUser.token,
            password: e.target.password.value,
            password_confirm: e.target.password_confirm.value 
        }
          
        if (!this.validateForm(data)) { 
            return;
        }
        this.setState({ loading: true }); 
        delete data['password_confirm'];
        
        callMaker('post', `/auth/passwordreset`, data)
        .then(
            response => { 
                if(response.status >= 400){
                    this.setState({ error: response.message, loading: false });
                }
                else{ 
                    this.setState({ loading: false, msg: "Traitement effectué avec succès" });
                } 
            },
            error => this.setState({ error, loading: false })
        );
    }
  
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    validateForm(data) {
        let message = "";
       
        if (data.password !== data.password_confirm) {
            message = message + `   Les 2 mots de passe ne sont pas identiques. `;
        }

        if (data.password.length < 6) {
            message = message + `  Le mot de passe doit contenir au moins 6 caractères. `;
        }
        
        this.setState({ error: message });

        return message === "" ? true : false;
    } 
  
    render(){  
        const { loading, error, msg, password, password_confirm, id } = this.state;
        return (
            <div className="animated fadeIn">
                <br/>
                {error &&
                    <div className={'alert alert-danger'}>{error}</div>
                }
                {msg &&
                    <div className={'alert alert-success'}>{msg}</div>
                }
                <Form onSubmit={this.handleSubmit} method="POST"> 
                    <Card>
                        <CardHeader>
                            <h5>Réinitialiser le mot de passe</h5>  
                        </CardHeader>
                            <CardBody>  
                                <Row>
                                    <Col xs="12">
                                        <FormGroup>
                                            <Label htmlFor="password">Nouveau mot de passe</Label>
                                            <Input type="hidden" value={id} onChange={this.handleChange} name='id'/>
                                            <Input type="password" name="password" id="password" value={password} onChange={this.handleChange} placeholder="Mot de passe" required />  
                                        </FormGroup>
                                    </Col>
                                </Row> 
                                <Row>
                                    <Col xs="12">
                                        <FormGroup>
                                            <Label htmlFor="password_confirm">Confirmer mot de passe</Label>
                                            <Input type="password" name="password_confirm" id="password_confirm" value={password_confirm} onChange={this.handleChange} placeholder="Confirmer" required />  
                                        </FormGroup> 
                                    </Col>
                                </Row> 
                            </CardBody> 
                        <CardFooter>
                            <Row>
                                <Col xs="6">
                                    <button type="submit" className="btn btn-outline-dark" disabled={loading}>Valider le mot de passe</button> 
                                    {loading &&
                                        <img alt='Loading...' src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                    }
                                </Col> 
                            </Row>
                        </CardFooter>
                    </Card>
                </Form> 
            </div> 
        )
    
      }
}

export default PasswordReset ;