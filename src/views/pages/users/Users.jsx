import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Badge, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { userService, parametersService } from '../../../services';
import { callMaker, getParameterValueById } from '../../../helpers';
import { Input, Label } from 'reactstrap';


function UserRow(props) {
  const user = props.user;
  // const userLink = `/users/${user.id}`;
  const userEditLink = `/users/edit/${user.id}`;

  const getBadge = (status) => {
    return status === 'active' ? 'success' :
      status === 'inactive' ? 'secondary' :
        status === 'pending' ? 'warning' :
          status === 'banned' ? 'danger' :
            'primary'
  }

  return (
    <tr key={user.id.toString()}>
      <td>{user.prenoms} {user.nom}</td>
      <td>{getParameterValueById(props.niveaux, user.niveau_entree)}</td>
      <td>{user.email}</td>
      <td>{getParameterValueById(props.roles, user.role)} </td>
      <td><Badge color={getBadge(user.status)}>{getParameterValueById(props.status_utilisateur, user.status)}</Badge></td>
      <td>
        <Link to={userEditLink}><i className="icon-note" title='Editer'></i></Link>&nbsp;&nbsp;
        <span style={{color: 'green'}} onClick={() => props.updateStatus(user)}><i className="icon-power" title='Activer / Desactiver'></i></span>
      </td>
    </tr>
  )
}

class Users extends Component {

  constructor(props){
      super(props);
      this.state = {
          error : null,
          msg: null,
          isLoaded : false,
          users: [],
          usersAll: [],
          roles: [],
          niveaux: [],
          searchValue: ''
      };
      this.handleSearch = this.handleSearch.bind(this);
  }

  async componentDidMount() {
      const data = await userService.getAll();
      const roles = await parametersService.getRoles();
      const niveaux = await parametersService.getNiveauEntrees();
      const status_utilisateur = await parametersService.getUserStatus();
      this.setState({ isLoaded: true, users: data, usersAll: data, roles: roles, niveaux: niveaux, status_utilisateur: status_utilisateur });
  }

  async getUsers() {
      const data = await userService.getAll();
      this.setState({ isLoaded: true, users: data, usersAll: data });
  }

  handleChange(e){
    this.setState({[e.target.name]: e.target.value});
  }

  handleSearch(e){
    let value = e.target.value;
    let results = this.state.usersAll;
    if (value.length > 0) {
      results = this.state.usersAll.filter((user) => {
        return user.prenoms.toLowerCase().includes(value.toLowerCase())
          || user.nom.toLowerCase().includes(value.toLowerCase())
          || user.email.toLowerCase().includes(value.toLowerCase())
      });
    }
    this.setState({users: results});
  }

  updateStatus = async(data) => {
      try {
        const _data = {
          id: data.id,
          status: data.status === 'active' ? 'inactive' : 'active'
        }
        this.setState({ isLoaded: false });
        await callMaker('put', `/users`, _data);
        window.location.reload(true);
        this.setState({ isLoaded: true, msg: "Traitement effectué avec succès" });
      } catch (error) {
          this.setState({ isLoaded: false, msg: "Erreur pendant le traitement" });
          console.error(error);
      }
  };

  render() {
    const { isLoaded, users, roles, niveaux, status_utilisateur, error, msg } = this.state;

    if (!isLoaded) {
        return <div>Loading ...</div>
    }else{
      return (
        <div className="animated fadeIn">
          <br/>
          {error &&
              <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
              <div className={'alert alert-success'}>{msg}</div>
          }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> <strong>Liste des utilisateurs</strong>
                  <div className="card-header-actions">
                    <Link to='/users/add'>
                      <Button color="primary" size="sm" className="btn-square">
                      <i className="icon-plus"></i>&nbsp;Nouveau</Button>
                    </Link>
                  </div>
                </CardHeader>
                <CardBody>
                <Row>
                  <Col xs="12" md="2">
                    <Label>
                      <b>Recherche</b>
                    </Label>
                  </Col>
                  <Col xs="12" md="10">
                    <Input
                      type="text"
                      placeholder="Commencer par saisir un texte"
                      onChange={this.handleSearch}
                      name="recherche"
                      value={this.searchValue}
                    />
                  </Col>
                </Row>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">Nom & Prénoms</th>
                        <th scope="col">Niveau</th>
                        <th scope="col">Courriel</th>
                        <th scope="col">Rôle</th>
                        <th scope="col">Statut</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {users && users.length > 0 && users.map((user, index) =>
                        <UserRow key={index} user={user} roles={roles} niveaux={niveaux} status_utilisateur={status_utilisateur} updateStatus={this.updateStatus}/>
                      )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }
}

export default Users;
