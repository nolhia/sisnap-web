import React, { Component } from 'react'; 
import { Card, CardBody, CardFooter, CardHeader, Col, Form, FormGroup, Input, Label, Row } from 'reactstrap';
import { parametersService } from '../../../services';
import { callMaker, asyncLocalStorage } from '../../../helpers'; 
import _ from 'lodash';
var zones = '';

class SelectSite extends Component {
    constructor(props) {
        super(props);   

        this.state = {
            region_id: '',
            departement_id: '',
            enquete_id: '',
            niveau_entree: '',
            submitted: false,
            loading: false,
            error: '',
            msg: ''
        };
        this.handleRegion = this.handleRegion.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this); 
    }

    async componentDidMount() {   
        const parametre = await asyncLocalStorage.getItem('parametre'); 
        const user = await parametersService.getUserConnected(); 
        const niveaux = await parametersService.getNiveauEntrees();
        const enquetes = await callMaker('get', `/parametre/enquetes`);
        zones = localStorage.getItem('zonegeographique') || '';
        if(zones !== ''){
            zones = JSON.parse(zones);
            this.setState({ regions: zones.regions, departements: zones.departements });
        }
      
        this.setState({  
            currentUser: user,
            niveaux: niveaux,
            enquetes: enquetes,
            ...parametre
        });
    }
  
    handleSubmit(e) {
        e.preventDefault(); 
        this.setState({ submitted: true }); 
        const data = { 
            niveau_entree: e.target.niveau_entree.value,
            region_id: e.target.region_id.value,
            departement_id: e.target.departement_id.value,
            enquete_id: e.target.enquete_id.value
        }
           
        this.setState({ loading: true });  
        asyncLocalStorage.setItem('parametre', JSON.stringify(data))
        .then(
            response => {  
                this.setState({ loading: false, msg: "Traitement effectué avec succès" }); 
            },
            error => this.setState({ error, loading: false })
        );
    }
  
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    } 

    handleRegion(e) {
        const { name, value } = e.target;  
        const departements = _.filter(zones.departements, function(o) {  
            return o.region_id === value; 
        }); 
        this.setState({ [name]: value, departements: departements });  
    } 
  
    render(){  
        const { loading, error, msg, region_id, departement_id, departements, regions, niveaux, enquete_id, niveau_entree, enquetes } = this.state;
        return (
            <div className="animated fadeIn">
                <br/>
                {error &&
                    <div className={'alert alert-danger'}>{error}</div>
                }
                {msg &&
                    <div className={'alert alert-success'}>{msg}</div>
                }
                <Form onSubmit={this.handleSubmit} method="POST"> 
                    <Card>
                        <CardHeader style={{color: '#246195'}}>
                        <i className="fa fa-align-justify"></i><strong>Paramétrage du site et de la collecte</strong> 
                        </CardHeader>
                        <CardBody>  
                            <Row>
                                <Col xs="4">
                                    <FormGroup>
                                    <Label htmlFor="niveau_entree">Niveau d'entrée *</Label>
                                    <Input type="select" name="niveau_entree" id="niveau_entree" value={niveau_entree} onChange={this.handleChange} required>  
                                        {niveaux && niveaux.length && niveaux.map(niveau => <option key={niveau.id} value={niveau.id}>{niveau.title}</option>)}
                                    </Input>
                                    </FormGroup> 
                                </Col>
                                <Col xs="4">
                                    <FormGroup>
                                    <Label htmlFor="region_id">Région</Label>
                                    <Input type="select" id="region_id" name="region_id" value={region_id} onChange={this.handleRegion} >
                                        <option value='' key=''></option>
                                        { niveau_entree > 1 && regions && regions.length > 0 && regions.map(region => <option key={region.id} value={region.id}>{region.titre}</option>)}
                                    </Input> 
                                    </FormGroup>
                                </Col>
                                <Col xs="4">
                                    <FormGroup>
                                    <Label htmlFor="departement_id">Département</Label>
                                    <Input type="select" id="departement_id" name="departement_id" value={departement_id} onChange={this.handleChange} >
                                        <option value='' key=''></option>
                                        { niveau_entree > 2 && departements && departements.length > 0 && departements.map(departement => <option key={departement.id} value={departement.id}>{departement.titre}</option>)}
                                    </Input> 
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="6">
                                    <FormGroup>
                                    <Label htmlFor="enquete_id">Enquête</Label>
                                    <Input type="select" name="enquete_id" id="enquete_id" value={enquete_id} onChange={this.handleChange}>  
                                        <option value='' key=''></option>
                                        {enquetes && enquetes.length && enquetes.map(enquete => <option key={enquete.id} value={enquete.id}>{enquete.code} : {enquete.titre}</option>)}
                                    </Input>
                                    </FormGroup> 
                                </Col>
                            </Row>
                        </CardBody> 
                        <CardFooter>
                            <Row>
                                <Col xs="6">
                                    <button type="submit" className="btn btn-outline-dark" disabled={loading}>Enregistrer</button> 
                                    {loading &&
                                        <img alt='Loading...' src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                                    }
                                </Col> 
                            </Row>
                        </CardFooter>
                    </Card>
                </Form> 
            </div> 
        )
    
      }
}

export default SelectSite ;