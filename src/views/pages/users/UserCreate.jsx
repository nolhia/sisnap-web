import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, Form, Input, FormGroup, Label, Row } from 'reactstrap';
import { userService, parametersService } from '../../../services';
// import { RegionDepartement }  from '../../../components'; 
import _ from 'lodash';
var zones = '';

class UserCreate extends Component {
  
  constructor(props) {
      super(props)
      this.state = {
          id: '',
          nom: '',
          prenoms: '',
          email: '',
          password: '',
          password_confirm: '',
          role: '', 
          niveau_entree: '',
          matricule: '',
          direction: '',
          telephone: '', 
          loading: false,
          status: 'active',
          mode_edition: 'no',
          region_id: '',
          departement_id: '', 
          regions: [],
          departements: [],
          msg: '',
          error: ''
      }
      this.baseState = this.state;
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);  
      this.handleRegion = this.handleRegion.bind(this);  
  }

  async componentDidMount() {  
    const roles = await parametersService.getRoles();
    const niveaux = await parametersService.getNiveauEntrees();
    zones = localStorage.getItem('zonegeographique') || '';
    if(zones !== ''){
        zones = JSON.parse(zones);
        this.setState({ regions: zones.regions, departements: zones.departements });
    }
    if(this.props.match.params.id){
        const user= await userService.find(this.props.match.params.id);
        user.password = '';
        this.setState({ isLoaded: true, mode_edition:'yes', ...user });
    }
    this.setState({  
        niveaux: niveaux,
        roles: roles,
    });
  }
  
  handleSubmit = async  (event) => {
      event.preventDefault()
      const data = {
          id: event.target.id.value,
          mode_edition: event.target.mode_edition.value,
          nom: event.target.nom.value,
          prenoms: event.target.prenoms.value,
          email: event.target.email.value,
          password: event.target.password.value,
          password_confirm: event.target.password_confirm.value,
          role: event.target.role.value, 
          niveau_entree: event.target.niveau_entree.value, 
          matricule: event.target.matricule.value,
          direction: event.target.direction.value,
          telephone: event.target.telephone.value,
          region_id: event.target.region_id.value,
          departement_id: event.target.departement_id.value,
          status: 'active'
      }

      if (!this.validateForm(data)) { 
        return;
      }
      if(data.id === ''){
        delete data['id'];
      }
      if(data.password === ''){
          delete data['password'];
      }
      delete data['password_confirm'];
      delete data['mode_edition'];
      if(data.niveau_entree === '1'){
        delete data['region_id'];
        delete data['departement_id'];
      }
      else if(data.niveau_entree === '2'){
        delete data['departement_id'];
      }
      this.setState({ loading: true }); 
      userService.save(data)
      .then(
          response => { 
            if(response.status >= 400){
              this.setState({ error: response.message, loading: false });
            }
            else{ 
              this.setState({ loading: false, msg: "Traitement effectué avec succès" });
            } 
          },
          error => this.setState({ error, loading: false })
      );
    
  }

  handleChange(e){
      this.setState({[e.target.name]: e.target.value});  
  }

  handleRegion(e) {
      const { name, value } = e.target;  
      const departements = _.filter(zones.departements, function(o) {  
          return o.region_id === value; 
      }); 
      this.setState({ [name]: value, departements: departements });  
  } 

  validateForm(data) {
      let message = "";
      
     // mode creation
      if(data.mode_edition === 'no'){
          if (data.password !== data.password_confirm) {
            message = message + `   Les 2 mots de passe ne sont pas identiques. `;
          }

          if (data.password.length < 6) {
            message = message + `  Le mot de passe doit contenir au moins 6 caractères. `;
          }
      }
      this.setState({ error: message });

      return message === "" ? true : false;
  }

  resetForm = () => {
    this.setState(...this.baseState)
  }

  render() {
    const { id, nom, prenoms, email, password, password_confirm, matricule, direction, telephone, 
      role, niveau_entree, error, msg,mode_edition, niveaux, roles, departements, regions, departement_id, region_id  } = this.state;
    return (
      <div className="animated fadeIn">
        <br/>
          {error &&
              <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
              <div className={'alert alert-success'}>{msg}</div>
          }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Utilisateurs de la plateforme</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button> 
                    &nbsp;&nbsp;<Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/users" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="lastname">Nom *</Label>
                          <Input type="hidden" value={id} onChange={this.handleChange} name='id'/>
                          <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition'/>
                          <Input type="text" value={nom} onChange={this.handleChange} autoComplete="nom" name='nom' required/>
                        </FormGroup> 
                      </Col>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="firstname">Prénoms *</Label>
                          <Input type="text" value={prenoms} onChange={this.handleChange} autoComplete="prenoms" name='prenoms' required/>
                        </FormGroup>
                      </Col>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="email">Courriel *</Label>
                          <Input type="text" value={email} onChange={this.handleChange} autoComplete="email" name='email' required/>
                        </FormGroup>
                      </Col> 
                  </Row>
                  <Row>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="password">Mot de passe *</Label>
                          <Input type="password" value={password} onChange={this.handleChange} autoComplete="password" name='password' required/>
                        </FormGroup> 
                      </Col>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="password_confirm">Confirmer mot de passe *</Label>
                          <Input type="password" value={password_confirm} onChange={this.handleChange} autoComplete="password_confirm" name='password_confirm' required/>
                        </FormGroup>
                      </Col>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="role">Rôle *</Label>
                          <Input type="select" name="role" id="role" value={role} onChange={this.handleChange} required> 
                              <option value='' key=''></option>
                              {roles && roles.length && roles.map(role => <option key={role.id} value={role.id}>{role.title}</option>)}
                          </Input>  
                        </FormGroup>
                      </Col>
                  </Row>
                  <Row>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="matricule">Matricule</Label>
                          <Input type="text" value={matricule} onChange={this.handleChange} autoComplete="matricule" name='matricule'/>
                        </FormGroup> 
                      </Col>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="telephone">Téléphone </Label>
                          <Input type="number" value={telephone} onChange={this.handleChange} autoComplete="telephone" name="telephone"/>
                        </FormGroup>
                      </Col>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="direction">Structure</Label>
                          <Input type="text" placeholder="Service / Direction" value={direction} onChange={this.handleChange} autoComplete="direction" name="direction"/>
                        </FormGroup>
                      </Col>
                  </Row>
                  <Row>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="niveau_entree">Niveau d'entrée *</Label>
                          <Input type="select" name="niveau_entree" id="niveau_entree" value={niveau_entree} onChange={this.handleChange} required>  
                              {niveaux && niveaux.length && niveaux.map(niveau => <option key={niveau.id} value={niveau.id}>{niveau.title}</option>)}
                          </Input>
                        </FormGroup> 
                      </Col>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="region_id">Région</Label>
                          <Input type="select" id="region_id" name="region_id" value={region_id} onChange={this.handleRegion} >
                            <option value='' key=''></option>
                            { niveau_entree > 1 && regions && regions.length > 0 && regions.map(region => <option key={region.id} value={region.id}>{region.titre}</option>)}
                          </Input> 
                        </FormGroup>
                      </Col>
                      <Col xs="4">
                        <FormGroup>
                          <Label htmlFor="departement_id">Département</Label>
                          <Input type="select" id="departement_id" name="departement_id" value={departement_id} onChange={this.handleChange} >
                            <option value='' key=''></option>
                            { niveau_entree > 2 && departements && departements.length > 0 && departements.map(departement => <option key={departement.id} value={departement.id}>{departement.titre}</option>)}
                          </Input> 
                        </FormGroup>
                      </Col>
                      {/*<RegionDepartement niveau={niveau}/> */}
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>  
      </div>
    );
  }
}

export default UserCreate;
