import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Table, Col, Row, Form, FormGroup, Input, Label } from 'reactstrap'; 
import { callMaker } from '../../../../helpers'; 
import { parametersService  } from '../../../../services';  
import _ from 'lodash';
// Afficher les cartes
function Carte(props) {
  const cartes = props.carte;  
  return (  
    <Table responsive hover>
      <thead >
        <tr>
          <th scope="col">Titre</th>
          <th scope="col">Créé le</th>  
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
      {cartes && cartes.length > 0 && cartes.map((carte, index) => 
        <tr key={index}>
          <td><Link to={`/cartes/statiques/show/${carte.id}`}>{ carte.titre}</Link></td>
          <td>{carte.date_creation}</td>  
          <td><Link to={`/cartes/statiques/edit/${carte.id}`}><i className="icon-note"></i></Link></td>
        </tr>  
      )}
      </tbody>
    </Table> 
  )
}
// Lister les cartes par cat/gorie
function getCarteByCategorie(liste, categorie){ 
    return _.filter(liste, function(o) {  
        return o.categorie == categorie; 
    }); 
}

class Statitiques extends Component {
  
  constructor(props){
      super(props);
      this.state = {
          error : null,
          isLoaded : false,
          type_carte : [],
          cartes : [],
          data : []        
      };  
      
  }
  // initialiser
  async componentDidMount() { 
    this.setState({ isLoaded: false });
    const cartes = await callMaker('get', `/cartes/statiques`); 
    const type_carte = await parametersService.getTypeCarte(); 
    this.setState({ isLoaded: true, type_carte:type_carte, cartes:cartes });
  }  
// soumettre la requete
  handleSubmit = async (event) => {
    event.preventDefault(); 
    //const debut= event.target.debut.value;
    //const fin= event.target.fin.value;
    this.setState({ isLoaded: false });
    const cartes = await callMaker('get', `/cartes/statiques`); 
    this.setState({ isLoaded: true, cartes:cartes });
  } 
  // affichage
  render() {
    const { isLoaded, type_carte, cartes } = this.state;  
    
    if (this.state.error){
        return <div>Error in loading</div>
    } else if (!isLoaded) {
        return <div>Loading ...</div>
    } else{
      return (
        <div className="animated fadeIn">
          { this.filtre() }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Cartes statiques</strong>
                  <div className="card-header-actions"> 
                    <Link to='/cartes/statiques/add'><Button color="primary" size="sm" className="btn-square"><i className="icon-plus"></i>&nbsp;Nouveau</Button></Link>&nbsp;&nbsp;
                  </div>
                </CardHeader>
                <CardBody> 
                  <Row>
                    {type_carte && type_carte.length > 0 && type_carte.map((type, index) =>
                    <Col sm="12" xl="6" key={index}>
                      <Card>
                        <CardHeader>
                          <i className="fa fa-align-justify"></i><strong>{type.titre}</strong> 
                        </CardHeader>
                        <CardBody> 
                            <Carte carte={ getCarteByCategorie(cartes, type.id)}/> 
                        </CardBody>
                      </Card>
                    </Col>
                    )}
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }

  filtre() { 
    const { isLoaded } = this.state;  
    if (!isLoaded) {
        return <div>Loading ...</div>
    }
    else{ 
      return (
        <div className="animated fadeIn">
          <Form onSubmit={this.handleSubmit} method="POST">
            <Row>
              <Col>
                <Card>
                  <CardHeader>
                    <i className="fa fa-align-justify"></i><strong>Filtre</strong>
                    <div className="card-header-actions">
                      <Button type="submit" size="sm" color="secondary"><i className="fa fa-dot-circle-o"></i> Executer</Button>
                    </div>
                  </CardHeader>
                  <CardBody>
                    <Row>
                      {/*<Col xs="4">
                        <FormGroup>
                          <Label htmlFor="intitule">Thèmes</Label> 
                          <Input type="select" name="categorie" id="categorie" > 
                            <option value='' key=''></option>
                            {type_carte && type_carte.length > 0 && type_carte.map(type => <option key={type.id} value={type.id}>{type.titre}</option>)}
                          </Input> 
                        </FormGroup>
                      </Col>
                      */}
                      <Col xs="4">
                        <FormGroup>
                        <Label htmlFor="debut">Date debut</Label>
                        <Input type="date" id="debut" name="debut" />
                        </FormGroup>
                      </Col> 
                      <Col xs="4">
                        <FormGroup>
                        <Label htmlFor="fin">Date fin</Label>
                        <Input type="date" id="fin" name="fin" />
                        </FormGroup>
                      </Col> 
                    </Row> 
                  </CardBody>
                </Card>
              </Col>
            </Row>
          </Form>
        </div>
      )
    }
  }
}

export default Statitiques;
