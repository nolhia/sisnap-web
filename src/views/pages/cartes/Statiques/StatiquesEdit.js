import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Form } from 'reactstrap';
import { callMaker } from '../../../../helpers'; 
import { parametersService  } from '../../../../services';

class StatiquesEdit extends Component {
  constructor(props) {
    super(props);
      this.state = {
          id: '',  
          categorie: '',  
          titre: '',  
          date_creation: '',  
          type_carte : [],
          isLoaded: true,  
          msg: '',
          error: ''
      }
      this.baseState = this.state;
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);  
      this.resetForm = this.resetForm.bind(this);   
  }
// initialiser
  async componentDidMount() {  
      this.setState({ isLoaded: false });
      const type_carte = await parametersService.getTypeCarte(); 
      if(this.props.match.params.id){
          const objet = await callMaker('get', `/cartes/statiques/${this.props.match.params.id}`);  
          this.setState({ isLoaded: true, ...objet });
          this.baseState = this.state;
      } 
      this.setState({ type_carte: type_carte });
  }
// soumettre la requete
  handleSubmit = async  (event) => {
      event.preventDefault(); 
      let data = {
          id: event.target.id.value,  
          categorie: event.target.categorie.value, 
          titre: event.target.titre.value,
          date_creation: event.target.date_creation.value 
      };
        
      this.setState({ loading: true }); 
      callMaker('put',`/cartes/statiques`, data)
        .then(
          response => { 
            if(response.status >= 400){
              this.setState({ error: response.message, loading: false });
            }
            else{ 
              this.setState({ loading: false, msg: "Traitement effectué avec succès" });
            } 
          },
          error => {  
            if(typeof error === 'string'){
                this.setState({ error , loading: false })
            }
            else{
                this.setState({ error: 'Connexion refusée.' , loading: false })
            } 
          } 
      );
    
  }
// suivire le changement
  handleChange(e){
      this.setState({[e.target.name]: e.target.value});  
  } 
// reinitialiser les formulaires
  resetForm = () => { 
    this.setState(() => this.baseState) 
  } 
// supprimer
  async supprimer(id) {  
    this.setState({ isLoaded: false });
    callMaker('delete', `/cartes/statiques/${id}`)
      .then(
        response => {
          if(response.status >= 400){
            this.setState({ error: response.message });
          }
          else{ 
            const { from } = { from: { pathname: "/cartes/statiques" } };
            this.props.history.push(from);
            this.setState({ isLoaded: true, msg: "Traitement effectué avec succès" });
          }  
        },
        error => {  
          if (typeof error === 'string') {
            this.setState({ isLoaded: true, error })
          }
          else {
            this.setState({ isLoaded: true, error: 'Connexion serveur refusée.' })
          }
        }
      ); 
  } 
// affichage
  render() {
    const { id, categorie, titre, error, msg, type_carte, date_creation } = this.state; 
    return (
      <div className="animated fadeIn">
         <br/>
          {error &&
              <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
              <div className={'alert alert-success'}>{msg}</div>
          }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Carte statique</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button> 
                    &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm } color="warning"><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/cartes/statiques" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link> 
                    &nbsp;&nbsp;<Button type="button" size="sm" onClick={()=>{this.supprimer(id)}} color="danger"><i className="fa fa-ban"></i> Supprimer</Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row> 
                    <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="titre">Titre *</Label>
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id' id='id'/> 
                        <Input type="text" value={titre} onChange={this.handleChange} id="titre" name="titre" required />
                      </FormGroup>
                    </Col> 
                  </Row> 
                  <Row>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="titre">Catégorie *</Label> 
                        <Input type="select" id="categorie" name="categorie" value={categorie} onChange={this.handleChange} required >
                          <option value='' key=''></option>
                          {type_carte && type_carte.length > 0 && type_carte.map(type => <option key={type.id} value={type.id}>{type.titre}</option>)}
                        </Input> 
                      </FormGroup>
                    </Col> 
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="date_creation">Date de création *</Label> 
                        <Input type="date" value={date_creation} onChange={this.handleChange} id="date_creation" name="date_creation" required />
                      </FormGroup>
                    </Col> 
                  </Row>  
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    );
  } 
   
}

export default StatiquesEdit;
