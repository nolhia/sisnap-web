import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, Row, Form } from 'reactstrap';
import { callMaker } from '../../../../helpers';  

class StatiquesShow extends Component {
  constructor(props) {
    super(props);
      this.state = {
          id: '',  
          categorie: '',  
          titre: '',  
          image: '',  
          date_creation: '',  
          type_carte : [],
          isLoaded: true,  
          msg: '',
          error: ''
      }  
  }
// encoder l'image base64
  arrayBufferToBase64(buffer) {
      var binary = '';
      var bytes = [].slice.call(new Uint8Array(buffer));
      bytes.forEach((b) => binary += String.fromCharCode(b));
      return window.btoa(binary);
  };
// initialiser
  async componentDidMount() {  
      this.setState({ isLoaded: false }); 
      if(this.props.match.params.id){
          const objet = await callMaker('get', `/cartes/statiques/${this.props.match.params.id}`);  
          this.setState({ isLoaded: true, ...objet }); 
      }   
  }
   
// Affichage
  render() {
    const { titre, error, msg, date_creation, image } = this.state; 
     
    return (
      <div className="animated fadeIn">
         <br/>
          {error &&
              <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
              <div className={'alert alert-success'}>{msg}</div>
          }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Carte statique</strong>
                  <div className="card-header-actions"> 
                    <Link to="/cartes/statiques" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row> 
                    <Col xs="8">
                      <strong>Titre :</strong> {titre} 
                    </Col> 
                    <Col xs="4">
                      <strong>Créé le :</strong> {date_creation} 
                    </Col>
                  </Row>  
                  <Row> 
                    <Col xs="12"> 
                        <br/>
                        <img src={`data:image/jpeg;base64,${image}`} alt='Cartes' width='500' height='500'/>
                    </Col> 
                  </Row>  
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    );
  } 
   
}

export default StatiquesShow;
