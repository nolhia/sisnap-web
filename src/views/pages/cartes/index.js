import Dynamiques from './Dynamiques';
import Statiques from './Statiques';

export {
    Dynamiques,
    Statiques
};