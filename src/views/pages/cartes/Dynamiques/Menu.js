import React from 'react';
import { Card, CardBody, CardHeader, Col, Row, ListGroup, ListGroupItem } from 'reactstrap'; 
 
import { Link } from 'react-router-dom';
const Menu = () => {   

    return (
        <>
        <Row>
            <Col xl={12}>
            <Card>
                <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Liste des cartes</strong>
                </CardHeader>
                <CardBody> 
                    <ListGroup>
                    <ListGroupItem tag="a">
                        <Link to='/cartes/dynamiques/stations'>Les stations de suivi</Link>
                    </ListGroupItem>
                    <ListGroupItem tag="a" href="#">Autres cartes</ListGroupItem>
                    </ListGroup>
                </CardBody>
            </Card>
            </Col>
        </Row>
        
        </>
    );
}

export default Menu;