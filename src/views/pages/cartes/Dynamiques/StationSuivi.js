import React from "react";
import { Map as LeafletMap, TileLayer, Marker, Popup, FeatureGroup } from 'react-leaflet';
import { stationService } from '../../../../services';

class StationSuivi extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      stations: [],
      error: null,
      isLoaded: false,
    }
  }
// Initialiser
  async componentDidMount() {  
    this.setState({ isLoaded: true})
    const stations = await stationService.getAll();

    this.setState({ isLoaded: true, stations: stations });
  }

// Affichage
  render() {
    const { isLoaded, stations } = this.state; 
    if (!isLoaded) {
        return <div>Loading ...</div>
    }
    else{ 
      return (
        <LeafletMap
          center={[17.6078, 8.0817]}
          zoom={6}
          maxZoom={10}
          attributionControl={true}
          zoomControl={true}
          doubleClickZoom={true}
          scrollWheelZoom={true}
          dragging={true}
          animate={true}
          easeLinearity={0.35}
        >
          <TileLayer
            url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
          />
          {stations && stations.length > 0 && 
            stations.map(station =>  
                  <FeatureGroup color="red">
                  <Marker key= {station.id} position={[station.latitude, station.longitude]} >
                    <Popup>
                      { station.titre} <br/> 
                      {
                        station.type_station_id === 'bf269a4e-1950-4c2e-ac1d-a80f51b39d5e' ? 'Piezomètre' :
                          station.type_station_id === '8272adb9-c65c-4bfe-9647-81ac0c3328fb' ? 'Pluviomètre' :
                            station.type_station_id === 'd2403103-0f34-4b19-8756-98d6405832d3' ? 'Hydromètre' :
                              station.type_station_id === '44e82222-357d-4790-84d5-d4b4358423b9' ? 'Qualitomètre' :
                                'Piezomètre'
                      }
                      

                    </Popup>
                  </Marker> 
                  </FeatureGroup> 
            )}
        </LeafletMap>
      );
    }
  }
}
 
export default StationSuivi;

