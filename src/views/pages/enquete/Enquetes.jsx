import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody, CardHeader, Col, Row, Table, Button } from 'reactstrap'; 
import { useHttp } from '../../../hooks/http';
import { callMaker, formatDate, getParameterValueById } from '../../../helpers'; 
import { parametersService } from '../../../services'; 
//les lignes de la liste
function EnqueteRow(props) {
  const objet = props.data;
  const id = JSON.stringify(objet)
  const objetEditLink = `/parametre/enquetes/edit/${id}`;
  const objetValidationLink = `/parametre/enquetes/validation/${id}`;
  const status_data = (props.statusEnquete && props.statusEnquete.length > 0) ? props.statusEnquete : [{"id": "pending","title": "En attente"},{"id": "active","title": "Actif"},{ "id": "validated", "title": "Validé" },{ "id": "closed", "title": "Fermé" }];
  return (
    <tr key={objet.id.toString()}>
      <td>{objet.code}</td>
      <td>{objet.titre}</td>
      <td>{formatDate(objet.debut)}</td>  
      <td>{formatDate(objet.fin)}</td>  
      <td>{objet.type_enquete}</td>
      <td>{getParameterValueById(status_data, objet.status)}</td>   
      <td>
        <Link to={objetEditLink} ><i className="icon-note" title='Editer'></i></Link>&nbsp;&nbsp;&nbsp;
        <span style={{color: 'red'}} onClick={() => props.deleteEnquete(objet.id)}><i className="icon-trash" title='Supprimer'></i></span>&nbsp;&nbsp;&nbsp;
        <Link to={objetValidationLink} ><i className="icon-power" title='Activer, Valider ou Fermer'></i></Link>
        {/*<span style={{color: 'green'}} onClick={() => props.updateStatus(objet)}><i className="icon-power" title='Activer / Desactiver'></i></span> */}
      </td>
    </tr>
  )
}
 
 // Liste
const Enquetes = props => {    
    const status_enquete = parametersService.getEnqueteStatus();
    const [reloadList, setReloadList] = useState(false);   
    const [isLoading, enquetes] = useHttp( '/parametre/enquetes', [reloadList] ); 
    const [statusEnquete, setStatusEnquete] = useState(status_enquete);
    const [msg, setMsg] = useState('');
    const [error, setError] = useState(''); 
    const enqueteCallback = () => {
      setReloadList(!reloadList)
    }; 

    const deleteEnquete = async(id) => {
      try {
        await callMaker('delete', `/parametre/enquetes/${id}`); 
        setMsg('Traitement effectué avec succès');  
      } catch (error) {
        setError(error);  
          console.error(error);
      }
      setReloadList(!reloadList)
    }; 

    const updateStatus = async(data) => {
      try {
        const _data = {
          id: data.id,
          status: data.status === 'pending' ? 'active' : 'pending'
        }
        await callMaker('put', `/parametre/enquetes`, _data);
        
        setMsg('Traitement effectué avec succès');  
      } catch (error) {
        setError(error);  
          console.error(error);
      }
      setReloadList(!reloadList)
    }; 
   
    let content = <div>Loading ...</div>;

    if (!isLoading ) {  
      
      content = ( 
          <div className="animated fadeIn"> 
            <br />  
            {error &&
                <div className={'alert alert-danger'}>{error}</div>
                }
                {msg &&
                <div className={'alert alert-success'}>{msg}</div>
                }
            <Row>
              <Col xl={12}>
              {isLoading ? (
                  <div>Loading ...</div>
                ) : ( 
                  <Card>
                    <CardHeader>
                      <i className="fa fa-align-justify"></i> <strong>Tournées / Campagnes</strong> 
                      <div className="card-header-actions">
                      <Link to='/parametre/enquetes/edit/0' >
                      <Button color="primary" size="sm" className="btn-square">
                        <i className="icon-plus"></i>&nbsp;Nouvelle campagne</Button></Link>&nbsp;&nbsp;
                      <Button color="secondary" size="sm" className="btn-square" onClick={() => { enqueteCallback(); }}>
                        <i className="icon-list"></i>&nbsp;Actualiser la liste</Button>
                      </div>
                    </CardHeader>
                    <CardBody>
                      <Table responsive hover>
                        <thead>
                          <tr>
                            <th scope="col">Code</th>
                            <th scope="col">Libellé</th>
                            <th scope="col">Date début</th>  
                            <th scope="col">Date fin</th>  
                            <th scope="col">Type</th>  
                            <th scope="col">Statut</th>  
                            <th scope="col">Actions</th>
                          </tr>
                        </thead>
                        <tbody>
                          {enquetes && enquetes.length > 0 && enquetes.map((data, index) =>
                            <EnqueteRow key={index} data={data} statusEnquete={statusEnquete} deleteEnquete={deleteEnquete} updateStatus={updateStatus}/>
                          )}
                        </tbody>
                      </Table>
                    </CardBody>
                  </Card>
                  )
                }
              </Col>
            </Row>
          </div>
      ); 
    }
  return content;
}

export default Enquetes;
