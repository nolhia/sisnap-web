import Enquetes from './Enquetes'; 
import EnqueteForm from './EnqueteForm'; 

export {
    Enquetes,
    EnqueteForm
};