import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, Row, FormGroup, Input, Label , Form } from 'reactstrap';
import { enqueteService, parametersService } from '../../../services';

const EnqueteValidation = props => { 
    const initialFormState = { id: '',  status: '', type_enquete: null };
    const [enquete, setEnquete] = useState(initialFormState);
    const [enqueteStatus, setEnqueteStatus] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [msg, setMsg] = useState('');
    const [error, setError] = useState(''); 
// ecoute des changements
    useEffect( () => { 
        async function fetchData() {
            const _enquete_status = await parametersService.getEnqueteStatus()
            if (props.match.params.id && props.match.params.id !== 0) {  
                setEnquete(JSON.parse(props.match.params.id)); 
                setEnqueteStatus(_enquete_status); 
            }
        }
        fetchData();
    }, []); 
    // validation
    const handleSubmit = event => {
        event.preventDefault();

        const data = {
            id: event.target.id.value, 
            status: event.target.status.value,
            type_enquete: event.target.type_enquete.value
        }

        setIsLoading(true); 
 
        enqueteService.updateStatus(data)
            .then(
            response => {
                if (response.status >= 400) {
                    setIsLoading(false);  
                    setError(response.message);   
                }
                else {
                    setIsLoading(false);  
                    setMsg('Traitement effectué avec succès');   
                }
            },
            error => {
                setIsLoading(false);  
                setError(error);   
            }
        );
    }  
 // gestion des changements
    const handleChange = e => {
      const { name, value } = e.target
      setEnquete({ ...enquete, [name]: value }); 
    };
    
    let content = <div>Loading ...</div>; 
     
    content = ( 
            <div>
                {error &&
                <div className={'alert alert-danger'}>{error}</div>
                }
                {msg &&
                <div className={'alert alert-success'}>{msg}</div>
                }
                <Form onSubmit={handleSubmit} method="POST">
                <Row>
                <Col>
                    <Card>
                    <CardHeader>
                        <i className="fa fa-align-justify"></i><strong>Tournées / Campagnes</strong>
                        <div className="card-header-actions">
                        <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button> 
                        &nbsp;&nbsp;
                        <Link to='/parametre/enquetes' ><Button color="secondary" size="sm" className="btn-square">
                        <i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                        </div>
                    </CardHeader>
                    <CardBody> 
                        <Row>
                            <Col xs="2">
                                <FormGroup>
                                <Label htmlFor="code">Code: </Label> {enquete.code}
                                </FormGroup>
                            </Col>
                            <Col xs="4">
                                <FormGroup>
                                <Label htmlFor="titre">Libellé: </Label> {enquete.titre}
                                </FormGroup>
                            </Col>
                            <Col xs="3">
                                <FormGroup>
                                <Label htmlFor="debut">Date début: </Label>{enquete.debut}
                                </FormGroup>
                            </Col> 
                            <Col xs="3">
                                <FormGroup>
                                <Label htmlFor="fin">Date fin: </Label>{enquete.fin}
                                </FormGroup>
                            </Col> 
                        </Row>  
                        <Row>
                            <Col xs="3">
                                <FormGroup>
                                <Label htmlFor="status">Statut *</Label>
                                <Input type="hidden" value={enquete.id} onChange={handleChange} name='id'/> 
                                <Input type="hidden" value={enquete.type_enquete} onChange={handleChange} name='type_enquete'/> 
                                <Input type="select" id="status" name="status" value={enquete.status} required onChange={handleChange} >
                                    { enqueteStatus && enqueteStatus.length > 0 && enqueteStatus.map(param_status => <option key={param_status.id} value={param_status.id}>{param_status.title}</option>)}
                                </Input>
                                </FormGroup>
                            </Col> 
                            <Col xs="9"><br/>
                                <div className={'alert alert-info'}>La validation de l'enquête implique la validation de toutes les analyses et les mesures enregistrées sur la période</div>
                            </Col> 
                        </Row>  
                         
                    </CardBody>
                    </Card>
                </Col>
                </Row>
                </Form>
            </div>
        )
    return content;

};

export default React.memo(EnqueteValidation);