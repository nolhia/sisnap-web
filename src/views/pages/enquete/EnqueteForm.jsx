import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, Row, FormGroup, Input, Label , Form } from 'reactstrap';
import { enqueteService, parametersService } from '../../../services';

const EnqueteForm = props => { 
    const initialFormState = { id: '', code: '', titre: '', debut: '', fin: '',  status: 'pending', type_enquete: '' };
    const [enquete, setEnquete] = useState(initialFormState);
    const [enqueteTypes, setEnqueteTypes] = useState([]);
    const [isLoading, setIsLoading] = useState(false);
    const [msg, setMsg] = useState('');
    const [error, setError] = useState(''); 
    // ecouter les changements
    useEffect( () => { 
        async function fetchData() {
            if (props.match.params.id && props.match.params.id !== 0) {  
                setEnquete(JSON.parse(props.match.params.id))
            }
            setEnqueteTypes(await parametersService.getEnqueteTypes());
        }
        fetchData(); 
    }, []);
    // soumission
    const handleSubmit = event => {
        event.preventDefault();

        const data = {
            id: event.target.id.value, 
            code: event.target.code.value,
            titre: event.target.titre.value,
            debut: event.target.debut.value, 
            fin: event.target.fin.value,
            status: event.target.status.value,
            type_enquete: event.target.type_enquete.value
        }

        if (!validateForm(data)) {
            console.log("Form Validation Failed => " + data)
            return;
        }

        if (data.id === '' || data.id === 0) {
            delete data['id'];
        }
        else{ // Ne pas toucher le status pendant la modification
            delete data['status'];
        }
         
        setIsLoading(true); 
 
        enqueteService.save(data)
            .then(
            response => {
                if (response.status >= 400) {
                    setIsLoading(false);  
                    setError(response.message);   
                }
                else {
                    setIsLoading(false);  
                    setMsg('Traitement effectué avec succès');   
                }
            },
            error => {
                setIsLoading(false);  
                setError(error);   
            }
        );
    }  
// validation
    const validateForm = (data) => {  
      let message = ""; 
      if (data.date_fin < data.date_debut) {
        message = message + ` La date de début doit être antérieure à la date de fin de campagne. `;
        setError( message);
      }
        
      return message === "" ? true : false;
    }
// reinitialiser
    const resetForm = () => {
      //setEnquete({ ...props.data })
    }
// changement gestion
    const handleChange = e => {
      const { name, value } = e.target
      setEnquete({ ...enquete, [name]: value }); 
    };
    
    let content = <div>Loading ...</div>; 
     
    content = ( 
            <div>
                {error &&
                <div className={'alert alert-danger'}>{error}</div>
                }
                {msg &&
                <div className={'alert alert-success'}>{msg}</div>
                }
                <Form onSubmit={handleSubmit} method="POST">
                <Row>
                <Col>
                    <Card>
                    <CardHeader>
                        <i className="fa fa-align-justify"></i><strong>Tournées / Campagnes</strong>
                        <div className="card-header-actions">
                        <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button> 
                        &nbsp;&nbsp;<Button type="reset" size="sm" color="danger" onClick={() => { resetForm() }}><i className="fa fa-ban"></i> Réinitialiser</Button> 
                        &nbsp;&nbsp;<Link to='/parametre/enquetes' >
                            <Button color="secondary" size="sm" className="btn-square">
                        <i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                        </div>
                    </CardHeader>
                    <CardBody> 
                        <Row>
                            <Col xs="4">
                                <FormGroup>
                                <Label htmlFor="code">Code *</Label>
                                <Input type="hidden" value={enquete.id} onChange={handleChange} id="id" name='id'/> 
                                <Input type="hidden" value={enquete.status} onChange={handleChange} id="status" name='status'/>  
                                <Input type="text" value={enquete.code} onChange={handleChange} id="code" name="code" required />
                                </FormGroup>
                            </Col>
                            <Col xs="8">
                                <FormGroup>
                                <Label htmlFor="titre">Libellé *</Label> 
                                <Input type="text" value={enquete.titre} onChange={handleChange} id="titre" name="titre" required />
                                </FormGroup>
                            </Col> 
                        </Row>    
                        <Row> 
                            <Col xs="4">
                                <FormGroup>
                                <Label htmlFor="type_enquete">Type de campagne *</Label> 
                                <Input type="select" id="type_enquete" name="type_enquete" onChange={handleChange} value={enquete.type_enquete} required >
                                    { enqueteTypes && enqueteTypes.length > 0 && enqueteTypes.map(param_status => <option key={param_status.id} value={param_status.id}>{param_status.title}</option>)}
                                </Input>
                                </FormGroup>
                            </Col>
                            <Col xs="4">
                                <FormGroup>
                                <Label htmlFor="debut">Date début *</Label>
                                <Input type="date" value={enquete.debut} onChange={handleChange} id="debut" name="debut" required />
                                </FormGroup>
                            </Col> 
                            <Col xs="4">
                                <FormGroup>
                                <Label htmlFor="fin">Date fin *</Label>
                                <Input type="date" value={enquete.fin} onChange={handleChange} id="fin" name="fin" required />
                                </FormGroup>
                            </Col> 
                        </Row>    
                    </CardBody>
                    </Card>
                </Col>
                </Row>
                </Form>
            </div>
        )
    return content;

};

export default React.memo(EnqueteForm);