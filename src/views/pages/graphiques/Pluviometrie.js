import React, { Component } from 'react';
import { Line } from 'react-chartjs-2'; 
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { Button, Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Input, Label } from 'reactstrap';
import { callMaker } from '../../../helpers'; 
import _ from 'lodash';  
import moment from 'moment';  

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: true
}

class Pluviometrie extends Component {
  
  constructor(props){
      super(props);
      this.state = {
          enquete : [],
          enquetes : [], 
          mois : [], 
          error : null,
          msg : '',
          isLoaded : false,
          data : {
            labels: [],
            datasets: []
          }        
      };  
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChangeMois = this.handleChangeMois.bind(this);   
      this.handleChangeEnquete = this.handleChangeEnquete.bind(this); 
  }
// initialiser
  async componentDidMount() { 
    const enquetes = await callMaker('get', `/parametre/enquetes`); 
    this.setState({ isLoaded: true, enquetes: enquetes });

  }  
// formattage des donnees
  courbeDataFormat(tab, period) {     
      const _data = [];
      const _labels = [];
      if(period.length > 0){
          tab = _.map(tab, function(o) { 
              if (period.indexOf(o.date_pluie.slice(5, 7)) != -1 ) return o;
          });
      }
      tab = _.without(tab, undefined);
       
      tab.map(objet => {
          _labels.push(moment(objet.date_pluie).format('DD-MM'));
          _data.push(Number(objet.quantite));
      }); 

      return {
          labels: _labels,
          title: (tab.length > 0) ? tab[0].enquete_titre : 'Titre ',
          data: _data 
      }; 
  }
// soumission de la requete
  handleSubmit = async  (event) => {
      event.preventDefault()
      const data = {
          enquete: this.state.enquete  
      } 
      this.setState({ isLoaded: false });

      callMaker('post', `/requete/graphique/pluviometries`, data)
        .then(
          response => {  
              let results = [];
              if (response.status >= 400) {
                this.setState({ error: response.message, loading: false });
              }
              else {
                if(data.enquete && data.enquete.length>0 ){
                    data.enquete.map(enquete => {
                        let value = _.filter(response, function(o) {  
                                        return o.enquete_id === enquete; 
                                    });
                        results.push(value); 
                    })
                } 
                 
                let _data =  {
                  labels: [],
                  datasets: []
                };
                let line_data = {};
                let color_list = ['#0B1C9B','#B50621','#31B506','#9D06B5','#BA4A00','#B7950B','#E74C3C','#145A32','#17202A'];
                results.map((objet, index) => { 
                    if(objet.length > 0){
                        line_data = this.courbeDataFormat(objet, this.state.mois); 
                        let color = color_list[index]; 
                        let dataset = {
                            label: line_data.title,
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: color,
                            borderColor: color,
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: color,
                            pointBackgroundColor: '#fff',
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: color,
                            pointHoverBorderColor: color,
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            data: line_data.data,
                        }  
                        _data.datasets.push(dataset)
                    } 
                })
                _data.labels = line_data.labels;
                this.setState({ data: _data, isLoaded: true, msg: "Traitement effectué avec succès" });
              }
          },
          error => {
            if (typeof error === 'string') {
              this.setState({ error, isLoaded: true })
            }
            else {
              console.log(error)
              this.setState({ error: 'Connexion refusée.', isLoaded: true })
            }
          }
        );
        
  }
// changement d'enquete
  handleChangeEnquete(e){
    var options = e.target.options;
    var value = [];
    for (var i = 0, l = options.length; i < l; i++) {
      if (options[i].selected) {
        value.push(options[i].value);
      }
    }
    this.setState({enquete: value});
  } 
// changement de moi
  handleChangeMois(e){
    var options = e.target.options;
    var value = [];
    for (var i = 0, l = options.length; i < l; i++) {
      if (options[i].selected) {
        value.push(options[i].value);
      }
    }
    this.setState({mois: value});
  } 

  // affichage
  render() {  
    const { isLoaded, data, enquetes,  enquete, error, msg, mois } = this.state;  

    if (!isLoaded) {
        return <div>Loading ...</div>
    } else{
      return (
        <div className="animated fadeIn">
        <br/>
        {error &&
            <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
            <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader style={{color: '#246195'}}>
                  <i className="fa fa-align-justify"></i><strong>Pluviométrie</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Exécuter</Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12">
                      <Card> 
                        <CardBody>
                          <Row>  
                            <Col xs="6">
                              <FormGroup>
                                <Label htmlFor="intitule">Campagnes *</Label>
                                <Input type="select" name="station" id="station" multiple required value={enquete} onChange={this.handleChangeEnquete}> 
                                {enquetes && enquetes.length && enquetes.map(enquete => <option key={enquete.id} value={enquete.id}>{enquete.code} : {enquete.titre}</option>)}
                                </Input> 
                              </FormGroup>
                            </Col> 
                            <Col xs="3">
                              <FormGroup>
                                <Label htmlFor="debut">Mois *</Label>
                                <Input type="select" name="mois" id="mois" multiple value={mois} onChange={this.handleChangeMois}> 
                                  <option key="01" value="01">Janvier</option>
                                  <option key="02" value="02">Fevrier</option>
                                  <option key="03" value="03">Mars</option>
                                  <option key="04" value="04">Avril</option>
                                  <option key="05" value="05">Mai</option>
                                  <option key="06" value="06">Juin</option>
                                  <option key="07" value="07">Juillet</option>
                                  <option key="08" value="08">Aout</option>
                                  <option key="09" value="09">Septembre</option>
                                  <option key="10" value="10">Octobre</option>
                                  <option key="11" value="11">Novembre</option>
                                  <option key="12" value="12">Decembre</option>
                                </Input> 
                              </FormGroup>
                            </Col> 
                          </Row>
                        </CardBody>
                      </Card>
                      </Col>
                  </Row>
                  <Row>
                    <Col xs="12">
                      <Card> 
                        <CardBody>
                          <div className="chart-wrapper"> 
                            <Line data={data} options={options} /> 
                          </div> 
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                  
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form> 
        </div>
      )
    }
  }
 
}

export default Pluviometrie;
