import React, { Component } from 'react';
import { Line } from 'react-chartjs-2'; 
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { Button, Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Input, Label } from 'reactstrap';
import { callMaker } from '../../../helpers'; 
import _ from 'lodash';
import moment from 'moment'; 

var stations_suivi = [];
const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: true
}

class Piezometrie extends Component {
  
  constructor(props){
      super(props);
      this.state = {
          nappe : '',
          station : [],
          debut : '',
          fin : '',
          error : null,
          msg : '',
          isLoaded : false,
          data : {
            labels: [],
            datasets: []
          }        
      };  
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);  
      this.handleChangeStation = this.handleChangeStation.bind(this);  
      this.handleNappeChange = this.handleNappeChange.bind(this);  
  }
// initialiser
  async componentDidMount() { 
      const nappes = await callMaker('get', `/parametre/nappes`);
      stations_suivi = await callMaker('get', `/stations`);
      this.setState({ isLoaded: true, nappes: nappes, stations: stations_suivi }); 
  }  
// formatage des donnees
  courbeDataFormat(tab) {  
      const _data = [];
      const _labels = [];
       
      tab.map(objet => {
          _labels.push(moment(objet.date_mesure).format('DD-MM-Y'));
          _data.push(Number(objet.repere_eau));
      }) 

      return {
        labels: _labels,
        title: (tab.length > 0) ? tab[0].station_titre : 'Titre ',
        data: _data 
      }; 
  }
// soumettre la requete
  handleSubmit = async  (event) => {
      event.preventDefault()
      const data = {
          nappe: this.state.nappe,
          station: this.state.station,
          debut: this.state.debut,
          fin: this.state.fin 
      } 
      this.setState({ isLoaded: false });

      callMaker('post', `/requete/graphique/piezometries`, data)
        .then(
          response => { 
              let results = [];
              if (response.status >= 400) {
                this.setState({ error: response.message, loading: false });
              }
              else {
                if(data.station && data.station.length>0 ){
                    data.station.map(station => {
                        let value = _.filter(response, function(o) {  
                                        return o.station_suivi_id === station; 
                                    });
                        results.push(value); 
                    })
                } 
                  
                let _data =  {
                  labels: [],
                  datasets: []
                };
                let line_data = {};
                let color_list = ['#0B1C9B','#B50621','#31B506','#9D06B5','#BA4A00','#B7950B','#E74C3C','#145A32','#17202A'];
                
                results.map((objet, index) => {
                    if(objet.length > 0){
                        line_data = this.courbeDataFormat(objet, this.state.mois); 
                        let color = color_list[index]; 
                        let dataset = {
                            label: line_data.title,
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: color,
                            borderColor: color,
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: color,
                            pointBackgroundColor: '#fff',
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: color,
                            pointHoverBorderColor: color,
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            data: line_data.data,
                        }  
                        _data.datasets.push(dataset)
                    } 
                })
                _data.labels = line_data.labels; 
                this.setState({ data: _data, isLoaded: true, msg: "Traitement effectué avec succès" });
              }
          },
          error => {
            if (typeof error === 'string') {
              this.setState({ error, isLoaded: true })
            }
            else {
              this.setState({ error: 'Connexion refusée.', isLoaded: true })
            }
          }
        );
        
  }
// tout autre changement
  handleChange(e){
    this.setState({[e.target.name]: e.target.value});  
  }
// changement de station
  handleChangeStation(e){
      var options = e.target.options;
      var value = [];
      for (var i = 0, l = options.length; i < l; i++) {
        if (options[i].selected) {
          value.push(options[i].value);
        }
      }
      this.setState({station: value});
  }

  // changement de nappe
  handleNappeChange(e) {
      const { name, value } = e.target;  
      const stations = _.filter(stations_suivi, function(o) {  
          return o.nappe_id === value && o.status === 'actif'; 
      });  
      this.setState({ [name]: value, stations: stations });  
       
  } 
  
  // affichage
  render() {  
    const { isLoaded, data, nappes, stations, nappe, station, debut, fin, error, msg } = this.state; 
      
    if (!isLoaded) {
        return <div>Loading ...</div>
    } else{
      return (
        <div className="animated fadeIn">
        <br/>
        {error &&
            <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
            <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader style={{color: '#246195'}}>
                  <i className="fa fa-align-justify"></i><strong>Profil piézométrique</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Executer</Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="12">
                      <Card> 
                        <CardBody>
                          <Row> 
                            <Col xs="3">
                              <FormGroup>
                                <Label htmlFor="intitule">Nappe *</Label>
                                <Input type="select" name="nappe" id="nappe" value={nappe} onChange={this.handleNappeChange} required> 
                                  <option value='' key=''></option>
                                  {nappes && nappes.length>0 && nappes.map(nappe => <option key={nappe.id} value={nappe.id}>{nappe.titre}</option>)}
                                </Input> 
                              </FormGroup>
                            </Col>
                            <Col xs="3">
                              <FormGroup>
                                <Label htmlFor="intitule">Piézomètres *</Label>
                                <Input type="select" name="station" id="station" multiple required value={station} onChange={this.handleChangeStation}> 
                                {stations && stations.length && stations.map(station => <option key={station.id} value={station.id}>{station.code} : {station.titre}</option>)}
                                </Input> 
                              </FormGroup>
                            </Col>
                            <Col xs="3">
                              <FormGroup>
                                <Label htmlFor="debut">Date Début mesure *</Label>
                                <Input type="date" name="debut" id="debut" value={debut} onChange={this.handleChange} required/>  
                              </FormGroup>
                            </Col>
                            <Col xs="3">
                              <FormGroup>
                                <Label htmlFor="fin">Date fin mesure *</Label>
                                <Input type="date" name="fin" id="fin" value={fin} onChange={this.handleChange} required/> 
                              </FormGroup>
                            </Col>  
                          </Row>
                        </CardBody>
                      </Card>
                      </Col>
                  </Row>
                  <Row>
                    <Col xs="12">
                      <Card> 
                        <CardBody>
                          <div className="chart-wrapper"> 
                            <Line data={data} options={options} /> 
                          </div> 
                        </CardBody>
                      </Card>
                    </Col>
                  </Row>
                  
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
      )
    }
  }
 
}

export default Piezometrie;
