import React, { Component } from "react";
import { Link } from "react-router-dom";
import { userService, analysechimiquesService } from '../../../services';

import { Card, CardBody, CardHeader, Col, Row} from "reactstrap";
import { formatDate } from '../../../helpers';
// import { CSVLink } from 'react-csv';

import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

class AnalysesChimiques extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],  
      currentUser: null
    };  
  }
// initialiser
  async componentDidMount() {
    //On récupère l'utilisateur courant
    userService.currentUser.subscribe(x => this.setState({ currentUser: x }));
    //On récupère la liste de toutes les analyses chimiques
    const data = await analysechimiquesService.getAll();
    //on  récupère les données au format export en excel
    const exportData = await analysechimiquesService.exportData();
    const columns = [
      { title: 'D analyse', render: rowData => formatDate(rowData.date_analyse)},
      { title: 'H analyse', field: 'heure_analyse' },
      { title: '°C', field: 'temperature' },
      { title: 'Cond(µs/cm)', field: 'conductivite' },
      { title: 'pH', field: 'ph' }
    ]
    this.setState({ isLoaded: true, data: data, columns: columns });
  } 
/*
  jsPdfGenerator = () => {

      let data = [];
      let count = 1;
      var doc = new jsPDF('p', 'pt');
      doc.page=1;

      var width   = doc.internal.pageSize.getWidth();
      var height  = doc.internal.pageSize.getHeight(); 

      var header = function () {  
          doc.setFontStyle('normal'); 
          doc.setFontSize(14);
          doc.setFontStyle('bold'); 
          // move_from_left, move_from_height
          doc.text(200, 100, 'Something something')
      };

      var footer = function () { 
          doc.setFontSize(7);
          doc.text(width-40, height - 30, 'Page - ' + doc.page); 
          doc.page ++; 
      };

      var options = {
            beforePageContent: header,
            afterPageContent: footer,
            theme: 'grid',
            columnStyles: {
                count:          {columnWidth: 30, },
                c1:             {columnWidth: 50},
                c2:             {columnWidth: 80},
                c3:             {columnWidth: 30},
                c4:             {columnWidth: 50, halign: 'right'},
            },
            
            headStyles: {fillColor: 'white', textColor: 'black'},
            style: {cellWidth: 'auto'},
            margin: {top: 150, bottom: 100, horizontal: 10},
      }

      // Data Processing
      this.state.array1.map( (item, index) => { 
          let b = {
                  count: count,
                  c1: item.c1,
                  c2: item.c2,
                  c3: item.c3,
                  c4: item.c4,
              } 
          count++;
          data.push(b);
      }) 

      doc.setFontSize(12)
      doc.line(0, 145, width, 145)

      doc.autoTable(col, data, options)
      // doc.fromHTML("<div>table</div>", 1, 1)
      
      // Save the Data
      doc.save('Generated.pdf')
  }
 */
// affichage
  render() {
    const { isLoaded, data, columns, error, msg } = this.state;

    if (!isLoaded) {
      return <div>Loading ...</div>
    } else {
      return (
        <div className="animated fadeIn">
          <br />
          {error &&
            <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
            <div className={'alert alert-success'}>{msg}</div>
          }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Liste des Analyses physico-chimiques</strong>
                </CardHeader>
                <CardBody>
                  <MaterialTable
                    title="Liste" 
                    icons={tableIcons}
                    columns={columns}
                    data={data}
                    options= {{actionsColumnIndex: -1}}
                    actions={[
                      {
                        icon: () => <SaveAlt />,
                        tooltip: "Imprimer le rapport",
                        onClick: (event, rowData) => {
                          //this.jsPdfGenerator()
                            const link = `/rapports/analyseschimiquesprint/${rowData.id}`;
                            this.props.history.push(link);
                        }
                      } 
                    ]} 
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      );
    }
  }
 
}

export default AnalysesChimiques;
