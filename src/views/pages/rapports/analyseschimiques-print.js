import React, { Component } from "react"; 
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import _ from 'lodash';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { elementchimiqueService } from '../../../services'; 
import { formatDate, currentDate, callMaker, asyncLocalStorage, getParameterValueById } from '../../../helpers';  
import './rapport.scss';


class AnalysesChimiquesPrint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: {}, 
      elements_chimiques: [], 
    };  
  } 

// initialiser
  async componentDidMount() {
    if (this.props.match.params.id) {
      this.setState({ isLoaded: false });
      const analyse = await callMaker('get', `/collecte/analyse/chimiques/report/${this.props.match.params.id}`);
      const zones = await asyncLocalStorage.getItem('zonegeographique'); 
      const elementChimiques = await elementchimiqueService.getByAnalyse('analyse_chimique'); 

      if(zones !== ''){ 
        analyse["region"]= getParameterValueById(zones.regions, analyse.station_region_id);
        analyse['departement']= getParameterValueById(zones.departements, analyse.station_departement_id);
        analyse['commune']= getParameterValueById(zones.communes, analyse.station_commune_id);
      } 
      const _element_chimique = analyse.element_chimique[0]
      let elements_chimiques = []; 
      Object.keys(_element_chimique).forEach(e => {
         
        if(_element_chimique[e] !== ""){
          let objet = {};
          objet['key'] = e; 
          objet['value'] = Number(_element_chimique[e]);
          // recuperer les caracteristiques des parametres chimiques
          let norme = _.find(elementChimiques, {code: e}); 
          if(norme){
            objet['min_norme'] = Number(norme.min_norme);
            objet['max_norme'] = Number(norme.max_norme);
            objet['unite'] = norme.unite;
            objet['remarque'] = 'Non Conforme';
            if(objet['value'] >= objet['min_norme'] && objet['value'] <= objet['max_norme']){
              objet['remarque'] = 'Conforme';
            }
          }
          elements_chimiques.push(objet)
        } 
      });
       
      this.setState({ isLoaded: true, data: analyse, elements_chimiques: elements_chimiques}); 
       
    }
      
  } 
 
// affichage
  render() {
    const { isLoaded, error, data, elements_chimiques } = this.state;
    if (!isLoaded) {
      return <div>Loading ...</div>
    } else {
      return (
        <div className="animated fadeIn">
          <br/>
          {error &&
              <div className={'alert alert-danger'}>{error}</div>
          }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> <strong>Rapport d'analyse</strong>
                  <div className="card-header-actions">
                        <ReactHTMLTableToExcel
                              id="test-table-xls-button"
                              className="download-table-xls-button"
                              table="table-to-xls"
                              filename="rapport-analyse-physico-chimique"
                              sheet="rapport"
                              buttonText="Imprimer"/>
                  </div>
                </CardHeader>
                <CardBody>  
                        <div id="divToPrint"> 
                          <table className={"table_resultat"} id="table-to-xls"> 
                            <caption align="top"><h4>RAPPORT D'ANALYSE</h4><br/></caption>
                            <tbody>
                              <tr>
                                <td colSpan={2}><b>Identification de l'échantillon (No. Ech.)</b></td> 
                                <td ><b>Point d'observation:</b></td>  
                                <td colSpan={2}><b>IRH: </b>{ data.station_titre}</td> 
                              </tr>
                              <tr>
                                <td colSpan={2}><b>Laboratoire</b></td> 
                                <td colSpan={3} >{ data.laboratoire_titre}</td>   
                              </tr>
                              <tr>
                                <td colSpan={5}>&nbsp;</td>  
                              </tr>
                              <tr>
                                <td>Région:</td>
                                <td>{ data.region}</td> 
                                <td>&nbsp;</td>
                                <td>Lieu (Village):</td> 
                                <td>{ data.nom_localite}</td> 
                              </tr>
                              <tr>
                                <td>Département</td>
                                <td>{ data.departement}</td> 
                                <td></td>
                                <td>Indice:</td> 
                                <td></td> 
                              </tr>
                              <tr>
                                <td>Programme:</td>
                                <td></td> 
                                <td></td>
                                <td>Date de prélèvement:</td> 
                                <td>{ formatDate(data.date_prelevement) }</td> 
                              </tr>
                              <tr>
                                <td>Commune:</td>
                                <td>{ data.commune}</td> 
                                <td></td>
                                <td>Date de fin d'analyse:</td> 
                                <td>{ formatDate(data.date_analyse) }</td> 
                              </tr>
                              <tr>
                                <td>Coordonnées</td>
                                <td><b>X</b></td> 
                                <td>{ data.latitude || data.station_latitude }</td>
                                <td><b>Y</b></td> 
                                <td>{ data.longitude || data.station_longitude }</td> 
                              </tr>
                              <tr>
                                <td colSpan={5}>&nbsp;</td>  
                              </tr>
                              <tr>
                                <td colSpan={5}>&nbsp;</td>  
                              </tr>
                              <tr>
                                <td colSpan={3}><b>Résultats d'analyse</b></td> 
                                <td colSpan={2}><b>Nomenclature:</b></td>  
                              </tr>
                              <tr>
                                <td colSpan={5}>&nbsp;</td>  
                              </tr>
                              <tr>
                                <th>Paramètres</th>
                                <th>Résultat</th> 
                                <th>Unité</th>
                                <th>Norme OMS</th> 
                                <th>Remarque</th> 
                              </tr>
                              <tr>
                                <td>pH</td>
                                <td>{data.ph}</td> 
                                <td>-</td>
                                <td>-</td> 
                                <td></td> 
                              </tr>
                              <tr>
                                <td>Température(T)</td>
                                <td>{data.temperature}</td> 
                                <td>°C</td>
                                <td>-</td> 
                                <td></td> 
                              </tr>
                              <tr>
                                <td>Conductivité (CE)</td>
                                <td>{data.conductivite}</td> 
                                <td>(µs/cm)</td>
                                <td>-</td> 
                                <td></td> 
                              </tr>
                              {elements_chimiques && elements_chimiques.length > 0 && elements_chimiques.map((element_chimique, index) => 
                              <tr key={index}>
                                <td>{ element_chimique.key}</td>
                                <td>{ element_chimique.value}</td> 
                                <td>{ element_chimique.unite}</td>
                                <td>{ element_chimique.min_norme } - { element_chimique.max_norme }</td> 
                                <td>{ element_chimique.remarque }</td> 
                              </tr>
                              )}
                              <tr>
                                <td colSpan={5}><p>"-": Non mesuré</p></td> 
                              </tr>
                              <tr>
                                <td colSpan={2}><b>Observations</b></td> 
                                <td><b></b></td>
                                <td colSpan={2}>  </td>  
                              </tr>
                              <tr>
                                <td colSpan={5}><p>{data.note}</p></td> 
                              </tr>
                              <tr>
                                <td colSpan={5}>&nbsp;</td>  
                              </tr>
                              <tr>
                                <td colSpan={3}>Niamey le {currentDate()}<br/><br/></td>  
                                <td colSpan={2}>Date, nom et signature du Chimiste<br/><br/></td>  
                              </tr>
                            </tbody>
                          </table> 
                        </div> 
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }
}

export default AnalysesChimiquesPrint;
