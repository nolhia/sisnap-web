import React, { Component } from "react";
import {
    Card,
    CardBody,
    CardHeader,
    Col,
    Row
} from "reactstrap";

import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

import { userService, bacteriologieService } from '../../../services';
import { formatDate } from '../../../helpers';


const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

class AnalysesBacteriologiques extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: [],
            exportData: [],
            modalImport: false,
            selectedFile: null,
            filename: 'Analyses bactériologiques',
            currentUser: null
        };
    }
    // initialiser
    async componentDidMount() {
        //On récupère l'utilisateur courant
        userService.currentUser.subscribe(x => this.setState({ currentUser: x }));
        //On récupère la liste de toutes les analyses bactériologiques
        const data = await bacteriologieService.getAll();
        //on  récupère les données au format export en excel
        const exportData = await bacteriologieService.exportData();
        const columns = [
            { title: 'Date analyse', render: rowData => formatDate(rowData.date_analyse) },
            { title: 'Heure analyse', field: 'heure_debut_analyse' },
            { title: 'Coliforme totaux', field: 'coliforme_totaux' },
            { title: 'Coliforme fécaux', field: 'coliforme_fecaux' },
            { title: 'Laboratoire', field: 'laboratoire_titre' },
            { title: 'Commentaire', field: 'note' }
        ]
        this.setState({ isLoaded: true, data: data, exportData: exportData, columns: columns });

    }

    // affichage
    render() {
        const { isLoaded, data, columns, error, msg } = this.state;

        if (!isLoaded) {
            return <div>Loading ...</div>
        } else {
            return (
                <div className="animated fadeIn">
                    <br />
                    {error &&
                        <div className={'alert alert-danger'}>{error}</div>
                    }
                    {msg &&
                        <div className={'alert alert-success'}>{msg}</div>
                    }
                    <Row>
                        <Col xl={12}>
                            <Card>
                                <CardHeader>
                                    <i className="fa fa-align-justify" />
                                    <strong>Analyses bactériologiques</strong>
                                </CardHeader>
                                <CardBody>
                                    <MaterialTable
                                        title="Liste"
                                        icons={tableIcons}
                                        columns={columns}
                                        data={data}
                                        options={{ actionsColumnIndex: -1 }}
                                        actions={[
                                            {
                                                icon: () => <SaveAlt />,
                                                tooltip: "Imprimer le rapport",
                                                onClick: (event, rowData) => {
                                                    const link = `/rapports/bacteriologiquesprint/${rowData.id}`;
                                                    this.props.history.push(link);
                                                }
                                            }
                                        ]}
                                    />
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            );
        }
    }

}

export default AnalysesBacteriologiques;
