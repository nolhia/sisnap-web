import React, { Component } from "react";
import _ from 'lodash';
import { elementchimiqueService } from '../../../services';
import { Card, CardBody, CardHeader, Col, Row } from "reactstrap";
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import { formatDate, callMaker } from '../../../helpers';
import { parametersService } from '../../../services';

class AnalysesBacteriologique extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            data: [],
            currentUser: null
        };
    }
    async componentDidMount() {
        if (this.props.match.params.id) {
            this.setState({ isLoaded: false });
            const analyse = await callMaker('get', `/collecte/analyse/bacteriologie/report/${this.props.match.params.id}`);
            const elementChimiques = await elementchimiqueService.getByAnalyse('bacteriologie');
            const normes = await parametersService.getNormes();

            const _element_chimique = analyse.element_chimique[0]
            let elements_chimiques = [];
            Object.keys(_element_chimique).forEach(e => {

                if (_element_chimique[e] !== "") {
                    let objet = {};
                    objet['key'] = e;
                    objet['value'] = Number(_element_chimique[e]);
                    // recuperer les caracteristiques des parametres chimiques
                    let norme = _.find(elementChimiques, { code: e });
                    if (norme) {
                        objet['min_norme'] = Number(norme.min_norme);
                        objet['max_norme'] = Number(norme.max_norme);
                        objet['unite'] = norme.unite;
                        if ((objet['max_norme'] == 0 || objet['max_norme'] == null)
                            && objet['min_norme'] == 0) {
                            objet['remarque'] = 'absence'
                        }
                        else {
                            objet['remarque'] = objet['min_norme'] + ' - ' + objet['max_norme'];
                        }
                    }
                    elements_chimiques.push(objet)
                }
            });
            const norme_ct = normes.filter(n => { return n.id.match("coliforme_totaux") });
            const norme_cf = normes.filter(n => { return n.id.match("coliforme_fecaux") });

            const tableSize = elements_chimiques.length + 3;
            this.setState({ isLoaded: true, data: analyse, elements_chimiques: elements_chimiques, norme_ct: norme_ct, norme_cf: norme_cf, tableSize: tableSize });
        }
    }
    render() {
        const { isLoaded, error, data, elements_chimiques, norme_ct, norme_cf, tableSize } = this.state;
        if (!isLoaded) {
            return <div>Loading ...</div>
        } else {
            return (
                <div className="animated fadeIn">
                    <br />
                    {error &&
                        <div className={'alert alert-danger'}>{error}</div>
                    }
                    <Row>
                        <Col xl={12}>
                            <Card>
                                <CardHeader>
                                    <i className="fa fa-align-justify"></i> <strong>Analyse bactériologique</strong>
                                    <div className="card-header-actions">
                                        <ReactHTMLTableToExcel
                                            id="test-table-xls-button"
                                            className="download-table-xls-button"
                                            table="table-to-xls"
                                            filename="rapport-analyse-bacteriologique"
                                            sheet="rapport"
                                            buttonText="Imprimer" />
                                    </div>
                                </CardHeader>
                                <CardBody>
                                    <div id="divToPrint" >
                                        <table className="table_resultat" id="table-to-xls">
                                            <tbody>
                                                <tr>
                                                    <th className="report_title" colSpan={tableSize}>
                                                        <br />
                                                        <h4>RAPPORT D'ANALYSE BACTÉRIOLOGIQUE</h4>
                                                        <br />
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td colSpan={tableSize}>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <th colSpan={tableSize}>
                                                        <strong>Laboratoire:</strong> 
                                                        &nbsp;<span>{data.laboratoire}</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th colSpan={tableSize}>
                                                        <strong>Date analyse:</strong>
                                                        &nbsp;<span>{formatDate(data.date_analyse)}</span>&nbsp;&nbsp;
                                                        <strong>Heure debut:</strong>
                                                        &nbsp;<span>{data.heure_debut_analyse}</span>&nbsp;&nbsp;
                                                        <strong>Heure fin:</strong>
                                                        &nbsp;<span>{data.heure_fin_analyse}</span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <td colSpan={tableSize}>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <div className="determinations">
                                                            Déterminations
                                                        </div>
                                                        <div className="echantillons">
                                                            Échantillons
                                                        </div>
                                                    </th>
                                                    <th>Coliform totaux  / 100ml</th>
                                                    <th>Coliform fécaux / 100ml</th>
                                                    {elements_chimiques && elements_chimiques.length > 0 && elements_chimiques.map((element_chimique, index) =>
                                                        <th key={index}>{element_chimique.key} / {element_chimique.unite}</th>
                                                    )}
                                                </tr>
                                                <tr>
                                                    <td>Donées de l'analyse</td>
                                                    <td>{data.coliforme_totaux}</td>
                                                    <td>{data.coliforme_fecaux}</td>
                                                    {elements_chimiques && elements_chimiques.length > 0 && elements_chimiques.map((element_chimique, index) =>
                                                        <td key={index}>{element_chimique.value}</td>
                                                    )}
                                                </tr>
                                                <tr>
                                                    <td>Directives OMS</td>
                                                    <td>{norme_ct[0].title}</td>
                                                    <td>{norme_cf[0].title}</td>
                                                    {elements_chimiques && elements_chimiques.length > 0 && elements_chimiques.map((element_chimique, index) =>
                                                        <td key={index}>{element_chimique.remarque}</td>
                                                    )}
                                                </tr>
                                                <tr>
                                                    <td colSpan={tableSize}>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colSpan={tableSize}>
                                                        <br />
                                                        <div className="conclusion_box">
                                                            <b>Conclusion:</b>
                                                            <span>{data.note}</span>
                                                        </div>
                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colSpan={tableSize}>&nbsp;</td>
                                                </tr>
                                                <tr >
                                                    <td colSpan={tableSize}>
                                                        <br />
                                                        <div className="signature">
                                                            <strong>Chef Service / Laboratoire:</strong>
                                                        </div>
                                                        <br /><br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colSpan={tableSize}>&nbsp;</td>
                                                </tr>
                                                <tr >
                                                    <td colSpan={tableSize}>
                                                        <br />
                                                        <div className="signature">
                                                            <b>Le Directeur Général:</b>
                                                        </div>
                                                        <br /><br />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>

            )
        }
    }

}

export default AnalysesBacteriologique;
