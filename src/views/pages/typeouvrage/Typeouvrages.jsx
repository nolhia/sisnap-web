import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, Row, Table } from 'reactstrap';
import { typeOuvrageService } from '../../../services';

// les lignes du tableau
function TypeouvrageRow(props) {
  const objet = props.typeouvrage;
  const objetEditLink = `/parametre/typeouvrages/edit/${objet.id}`;

  return (
    <tr key={objet.id.toString()}>
      <td>{objet.code}</td>
      <td>{objet.titre}</td>
      <td>
        <Link to={objetEditLink}><i className="icon-note"></i></Link>
      </td>
    </tr>
  )
}

class Typeouvrages extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      typeouvrages: []
    };
  }
 // initialiser
  async componentDidMount() {
    const data = await typeOuvrageService.getAll();
    this.setState({ isLoaded: true, typeouvrages: data });
  }

  render() {

    const { isLoaded, typeouvrages } = this.state;

    if (this.state.error) {
      return <div>Error in loading</div>
    } else if (!isLoaded) {
      return <div>Loading ...</div>
    } else {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <span>Liste des types d'ouvrage</span>
                  <div className="card-header-actions">
                    <Link to='/parametre/typeouvrages/add'><Button color="primary" size="sm" className="btn-square"><i className="icon-plus"></i>&nbsp;Nouveau</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Table responsive hover>
                    <thead>
                      <tr>
                        <th scope="col">Code</th>
                        <th scope="col">Libelle</th>
                        <th scope="col">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
                      {typeouvrages && typeouvrages.length > 0 && typeouvrages.map((typeouvrage, index) =>
                        <TypeouvrageRow key={index} typeouvrage={typeouvrage} />
                      )}
                    </tbody>
                  </Table>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }
}

export default Typeouvrages;
