import Typeouvrages from './Typeouvrages';
import TypeouvrageCreate from './TypeouvrageCreate';

export {
    Typeouvrages,
    TypeouvrageCreate
};