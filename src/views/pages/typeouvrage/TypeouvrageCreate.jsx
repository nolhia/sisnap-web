import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { userService, typeOuvrageService } from '../../../services';

import {
  Button, Card, CardBody, CardHeader, Col, FormGroup,
  Input, Label, Row, Form
} from 'reactstrap';

class TypeouvrageCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      code: '',
      titre: '',
      mode_edition: 'no',
      msg: '',
      error: ''
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.baseState = this.state;
    this.handleChange = this.handleChange.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }
  // initialisation
  async componentDidMount() {
    if (this.props.match.params.id) {
      const typeOuvrage = await typeOuvrageService.find(this.props.match.params.id);
      this.setState({ isLoaded: true, mode_edition: 'yes', ...typeOuvrage });
      this.baseState = this.state;
    }
  }
  // soumission de la requete
  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      id: event.target.id.value,
      mode_edition: event.target.mode_edition.value,
      code: event.target.code.value,
      titre: event.target.titre.value,
    }
    if (!this.validateForm(data)) {
      console.log("Form Validation Failed => " + data)
      return;
    }

    if (data.id === '') {
      delete data['id'];
    }

    delete data['mode_edition'];

    this.setState({ loading: true });

    typeOuvrageService.save(data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => this.setState({ error, loading: false })
      );
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  // validation
  validateForm(data) {
    let message = "";
    this.setState({ error: message });

    return message === "" ? true : false;
  }
  // reinitialiser le formulaire
  resetForm = () => {
    this.setState(() => this.baseState)
  }


  render() {
    const { id, code, titre, mode_edition, error, msg } = this.state;
    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Type d'ouvrage</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button>
                    &nbsp;&nbsp;<Button type="reset" size="sm" color="danger" onClick={this.resetForm}><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/parametre/typeouvrages" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="2">
                      <FormGroup>
                        <Label htmlFor="code">Code *</Label>
                        <Input type="text" id="code" name="code" value={code} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                    <Col xs="10">
                      <FormGroup>
                        <Label htmlFor="titre">Libelle *</Label>
                        <Input type="text" id="titre" name="titre" value={titre} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
            <Input type="hidden" value={id} onChange={this.handleChange} name='id' />
            <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition' />
          </Row>
        </Form>
      </div>
    )
  }


}

export default TypeouvrageCreate;
