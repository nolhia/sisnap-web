import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { stationService, typeStationService, nappeService, typeOuvrageService, parametersService } from '../../../services';

import {
  Button, Card, CardBody, CardHeader, Col, FormGroup,
  Input, Label, Row, Form
} from 'reactstrap';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel'; 
import { validerLatitute, validerLongitude } from '../../../helpers';
import { ZoneGeographique } from '../../../components';

class StationCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      code: '',
      titre: '',
      latitude: '',
      longitude: '',
      altitude: '',
      profondeur_totale: '',
      repere_sol: '',
      type_station_id: '',
      type_enregistrement: '',
      type_ouvrage_id: '',
      nappe_id: '',
      observateur: '',
      note: '',
      status: 'actif',
      courbe_etalonnage: false,
      region_id: '',
      departement_id: '',
      commune_id: '',
      mode_edition: 'no',
      msg: '',
      error: ''
    }

    this.baseState = this.state;
    this.handleChange = this.handleChange.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }

  handleChange(e) {
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    this.setState({ [e.target.name]: value });
  }
// soumission de la requete
  handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      id: event.target.id.value,
      mode_edition: event.target.mode_edition.value,
      code: event.target.code.value,
      titre: event.target.titre.value,
      latitude: event.target.latitude.value,
      longitude: event.target.longitude.value,
      altitude: event.target.altitude.value,
      profondeur_totale: event.target.profondeur_totale.value,
      repere_sol: event.target.repere_sol.value,
      type_station_id: event.target.type_station_id.value,
      type_enregistrement: event.target.type_enregistrement.value,
      type_ouvrage_id: event.target.type_ouvrage_id.value,
      nappe_id: event.target.nappe_id.value,
      note: event.target.note.value,
      region_id: event.target.region_id.value,
      departement_id: event.target.departement_id.value,
      commune_id: event.target.commune_id.value,
      status: event.target.status.value,
      courbe_etalonnage: event.target.courbe_etalonnage.value,
      observateur: event.target.observateur.value,
    };

    if (!this.validateForm(data)) {
      console.log("Form Validation Failed => " + data)
      return;
    }

    if (data.id === '' || data.id === undefined) {
      delete data['id'];
    }
    if (data.profondeur_totale === '' || data.profondeur_totale === undefined) {
      delete data['profondeur_totale'];
    }
    if (data.repere_sol === '' || data.repere_sol === undefined) {
      delete data['repere_sol'];
    }
    if (data.region_id === '') {
      delete data['region_id'];
    }
    if (data.departement_id === '') {
      delete data['departement_id'];
    }
    if (data.commune_id === '') {
      delete data['commune_id'];
    }
    if (data.nappe_id === '' || data.nappe_id === undefined) {
      delete data['nappe_id'];
    }

    delete data['mode_edition'];

    this.setState({ loading: true });

    stationService.save(data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => this.setState({ error, loading: false })
      );

  }
// valider
  validateForm(data) {
    let message = "";
    if (!validerLatitute(data.latitude)) {
      message = message + ` La latitude n'est pas correcte. `;
    }

    if (!validerLongitude(data.longitude)) {
      message = message + ` La longitude n'est pas correcte. `;
    }

    this.setState({ error: message });

    return message === "" ? true : false;
  }
  // reinitialiser
  resetForm = () => {
    this.setState(() => this.baseState)
  }
// soumission de la requete
  async componentDidMount() {
    const typeStations = await typeStationService.getAll();
    const nappes = await nappeService.getAll();
    const typeEnregistrements = await parametersService.getTypeEnregistreur();
    const statusStations = await parametersService.getStatusStations();
    const typeOuvrages = await typeOuvrageService.getAll();

    this.setState({
      typeStations: typeStations,
      nappes: nappes,
      typeEnregistrements: typeEnregistrements,
      typeOuvrages: typeOuvrages,
      statusStations: statusStations
    });

    if (this.props.match.params.id) {
      const station = await stationService.find(this.props.match.params.id); 
      this.setState({ isLoaded: true, mode_edition: 'yes', ...station });
    } 
  }

  render() {
    const { id, code, titre, latitude, longitude, altitude, profondeur_totale, type_station_id, repere_sol, type_enregistrement,
      type_ouvrage_id, nappe_id, note, region_id, departement_id, commune_id, observateur, status, courbe_etalonnage, typeStations, nappes, typeEnregistrements,
      typeOuvrages, statusStations, mode_edition, error, msg } = this.state;
    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Station de suivi</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button>
                    &nbsp;&nbsp;<Button type="reset" size="sm" color="danger" onClick={this.resetForm}><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/parametre/stations" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Input type="hidden" value={id} onChange={this.handleChange} name='id' />
                  <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition' />
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="type_station_id">Type de station *</Label>
                        <Input type="select" id="type_station_id" name="type_station_id" value={type_station_id} onChange={this.handleChange} required >
                          <option value='' key=''></option>
                          {typeStations && typeStations.length > 0 && typeStations.map(typeStation => <option key={typeStation.id} value={typeStation.id}>{typeStation.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="code">Code *</Label>
                        <Input type="text" id="code" name="code" value={code} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="titre">Intitulé *</Label>
                        <Input type="text" id="titre" name="titre" value={titre} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="type_ouvrage_id">Type d'ouvrage *</Label>
                        <Input type="select" id="type_ouvrage_id" name="type_ouvrage_id" value={type_ouvrage_id} onChange={this.handleChange} required >
                          <option value='' key=''></option>
                          {typeOuvrages && typeOuvrages.length > 0 && typeOuvrages.map(typeOuvrage => <option key={typeOuvrage.id} value={typeOuvrage.id}>{typeOuvrage.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="longitude">Longitude *</Label>
                        <Input type="number" id="longitude" placeholder="5.2580" name="longitude" value={longitude} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="latitude">Latitude *</Label>
                        <Input type="number" id="latitude" placeholder="14.8905" name="latitude" value={latitude} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="altitude">Altitude *</Label>
                        <Input type="number" id="altitude" name="altitude" value={altitude} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row> 
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="nappe_id">Nappe </Label>
                        <Input type="select" id="nappe_id" name="nappe_id" value={nappe_id} onChange={this.handleChange} >
                          <option value='' key=''></option>
                          {nappes && nappes.length > 0 && nappes.map(nappe => <option key={nappe.id} value={nappe.id}>{nappe.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="type_enregistrement">Type enregistrement *</Label>
                        <Input type="select" id="type_enregistrement" name="type_enregistrement" value={type_enregistrement} onChange={this.handleChange} required >
                          {typeEnregistrements && typeEnregistrements.length > 0 && typeEnregistrements.map(typeEnregistrement => <option key={typeEnregistrement.id} value={typeEnregistrement.id}>{typeEnregistrement.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="profondeur_totale">Profondeur totale</Label>
                        <Input type="number" id="profondeur_totale" name="profondeur_totale" value={profondeur_totale} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="repere_sol">Repère sol</Label>
                        <Input type="number" id="repere_sol" name="repere_sol" value={repere_sol} onChange={this.handleChange} />
                      </FormGroup>
                    </Col> 
                  </Row>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <FormControlLabel
                          control={<Checkbox checked={courbe_etalonnage} onChange={this.handleChange} value={courbe_etalonnage} id="courbe_etalonnage" name="courbe_etalonnage" />}
                          label="Courbe d’étalonnage"
                        />
                      </FormGroup>
                    </Col> 
                    <Col xs="9">
                      <ZoneGeographique region_id={region_id}
                        departement_id={departement_id}
                        commune_id={commune_id}
                      />
                    </Col> 
                  </Row>
                  <Row> 
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="status">Statut de la station *</Label>
                        <Input type="select" id="status" name="status" value={status} onChange={this.handleChange} required >
                          {statusStations && statusStations.length > 0 && statusStations.map(statusStation => <option key={statusStation.id} value={statusStation.id}>{statusStation.title}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="observateur">Observateur</Label>
                        <Input type="textarea" id="observateur" name="observateur" value={observateur} onChange={this.handleChange} />
                      </FormGroup>
                    </Col> 
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="note">Commentaire</Label>
                        <Input type="textarea" id="note" value={note} onChange={this.handleChange} name="note" />
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }


}

export default StationCreate;
