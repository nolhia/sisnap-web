

import React from 'react';
import ReactDOM from 'react-dom'; 
import LaboratoireCreate from './LaboratoireCreate';
import { mount, shallow } from 'enzyme/build';
import { Form } from 'reactstrap'; 


describe("LaboratoireCreate component ", () => {
    test("renders", ()=> {
        const wrapper = shallow(<LaboratoireCreate />);
        expect(wrapper.exists()).toBe(true);
    });
    
    it('should be defined', () => { 
        expect(Form).toBeDefined()
    });

    test('return the default Laboratoire form', () => {
        const wrapper = shallow(<LaboratoireCreate />);

        expect(wrapper).toMatchSnapshot();
    });

    test('Count number of fields', ()=>{
        const wrapper = shallow(<LaboratoireCreate />);

        expect(wrapper.find('Input')).toHaveLength(7);
    })
/*
    test("user text", () => {
        const wrapper = shallow(<LaboratoireCreate />);

        wrapper.find('input#titre').simulate("change", {
            target: { value: "hello" }
        });
        expect(wrapper.find('input#titre').props().value).toEqual("hello")
    });

    
    test("When the form is submitted the event is cancelled", () => {
        const wrapper = shallow(<LaboratoireCreate />);
        let prevented = false;

        wrapper.find("form").simulate("submit", {
            preventDefault: () => {
                prevented = true;
            }
        });
        expect(prevented).toEqual(true)
    });
*/
    
}); 