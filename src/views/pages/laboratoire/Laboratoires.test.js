import React from 'react';
import ReactDOM from 'react-dom';
import Laboratoires from './Laboratoires';
import {mount} from 'enzyme/build';

describe("Laboratoire component ", () => {
    test("renders", ()=> {
        const wrapper = mount(<Laboratoires />);
        expect(wrapper.exists()).toBe(true);
    });
     
}); 