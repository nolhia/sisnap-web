import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, Row } from 'reactstrap';
import MaterialTable from 'material-table';
import { laboratoireService } from '../../../services';
import { userService } from '../../../services';

import { forwardRef } from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};


class Laboratoires extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],
      msg: ''
    };
    this.supprimer = this.supprimer.bind(this);
  }

  async componentDidMount() {
    const data = await laboratoireService.getAll();
    const columns = [
      { title: 'Code', field: 'code' },
      { title: 'Nom', field: 'titre' },
      { title: 'Téléphone', field: 'telephone' },
      { title: 'Courriel', field: 'email' },
      { title: 'Adresse', field: 'adresse' }
    ]
    this.setState({ isLoaded: true, data: data, columns: columns });
  }

  async supprimer(objet) {
    laboratoireService.remove(objet)
      .then(
        response => {
          if (response.status === 401) {
            this.setState({ error: response.message });
            const { from } = this.props.location.state || { from: { pathname: "/" } };
            this.props.history.push(from);
            userService.logout();
            window.location.reload(true);
          } else if (response.status >= 400) {
            this.setState({ error: response.message });
          }
          else {
            this.state.data.splice(this.state.data.indexOf(objet), 1);
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error })
          }
          else {
            this.setState({ error: 'Connexion serveur refusée.' })
          }
        }
      );
  }

  render() {
    const { isLoaded, data, columns, error, msg } = this.state;

    if (!isLoaded) {
      return <div>Loading ...</div>
    } else {
      return (
        <div className="animated fadeIn">
          <br />
          {error &&
            <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
            <div className={'alert alert-success'}>{msg}</div>
          }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <span>Liste des laboratoires</span>
                  <div className="card-header-actions">
                    <Link to='/parametre/laboratoires/add'><Button color="primary" size="sm" className="btn-square"><i className="icon-plus"></i>&nbsp;Nouveau</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <MaterialTable
                    title="Liste des laboratoires"
                    icons={tableIcons}
                    columns={columns}
                    data={data}
                    actions={[
                      {
                        icon: Edit,
                        tooltip: "Edit",
                        onClick: (event, rowData) => {
                          const link = `/parametre/laboratoires/edit/${rowData.id}`;
                          this.props.history.push(link);
                        }
                      }
                    ]}
                    editable={{
                      onRowDelete: oldData => {
                        return this.supprimer(oldData);

                      },
                    }
                    }
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }
}

export default Laboratoires;
