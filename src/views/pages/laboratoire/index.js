import Laboratoires from './Laboratoires';
import LaboratoireCreate from './LaboratoireCreate';

export {
    Laboratoires,
    LaboratoireCreate
};