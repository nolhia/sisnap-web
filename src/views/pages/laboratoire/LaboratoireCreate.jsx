import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Form } from 'reactstrap';
import { laboratoireService } from '../../../services';

class LaboratoireCreate extends Component {
  constructor(props) {
      super(props);
      this.state = {
          id: '',
          titre: '',
          code: '',
          email: '', 
          telephone: '', 
          adresse: '', 
          loading: false, 
          mode_edition: 'no',
          msg: '',
          error: ''
      }
      this.baseState = this.state;
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);  
      this.resetForm = this.resetForm.bind(this);  
  } 

  async componentDidMount() {  
      if(this.props.match.params.id){
          const laboratoire= await laboratoireService.find(this.props.match.params.id); 
          this.setState({ isLoaded: true, mode_edition:'yes', ...laboratoire });
          this.baseState = this.state;
      }
  }

  handleSubmit = async  (event) => {
      event.preventDefault()
      const data = {
          id: event.target.id.value,
          mode_edition: event.target.mode_edition.value,
          titre: event.target.titre.value,
          code: event.target.code.value,
          telephone: event.target.telephone.value,
          email: event.target.email.value, 
          adresse: event.target.adresse.value
      }

      if (!this.validateForm(data)) { 
        return;
      }
      if(data.id === ''){
        delete data['id'];
      } 
      delete data['mode_edition'];

      this.setState({ loading: true }); 
      laboratoireService.save(data)
      .then(
          response => { 
            if(response.status >= 400){
              this.setState({ error: response.message, loading: false });
            }
            else{
              
              this.setState({ loading: false, msg: "Traitement effectué avec succès" });
            } 
          },
          error => { 
            if(typeof error === 'string'){
                this.setState({ error , loading: false })
            }
            else{
                this.setState({ error: 'Connexion refusée.' , loading: false })
            } 
          } 
      );
    
  }
  handleChange(e){
      this.setState({[e.target.name]: e.target.value});  
  }

  validateForm(data) {
      let message = "";
      /*if (data.telephone.length < 11) {
        message = message + ` Le numero de téléphone n'est pas valide. `;
      }
      */
    
      this.setState({ error: message });

      return message === "" ? true : false;
  }

  resetForm = () => {
    this.setState(() => this.baseState);
  }

  render() {
    const { id, code, titre, email, adresse, telephone, error, msg, mode_edition } = this.state;
    return (
      <div className="animated fadeIn">
         <br/>
          {error &&
              <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
              <div className={'alert alert-success'}>{msg}</div>
          }
        <Form onSubmit={this.handleSubmit} method="POST">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Laboratoire</strong>
                <div className="card-header-actions">
                  <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button> 
                  &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm } color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                  &nbsp;&nbsp; <Link to="/parametre/laboratoires" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                </div>
              </CardHeader>
              <CardBody>
                <Row>
                  <Col xs="6">
                    <FormGroup>
                    <Label htmlFor="code">Code *</Label>
                    <Input type="hidden" value={id} onChange={this.handleChange} name='id'/>
                    <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition'/>
                    <Input type="text" value={code} onChange={this.handleChange} id="code" name="code" required />
                    </FormGroup>
                  </Col>
                  <Col xs="6">
                    <FormGroup>
                    <Label htmlFor="titre">Nom *</Label>
                    <Input type="text" value={titre} onChange={this.handleChange} id="titre" name="titre" required />
                    </FormGroup>
                  </Col> 
                </Row>  
                <Row>
                  <Col xs="6">
                      <FormGroup>
                      <Label htmlFor="telephone">Téléphone</Label>
                      <Input type="text" value={telephone} onChange={this.handleChange} id="telephone" name="telephone" />
                      </FormGroup>
                  </Col> 
                  <Col xs="6">
                    <FormGroup>
                    <Label htmlFor="email">Courriel</Label>
                    <Input type="email" value={email} onChange={this.handleChange} id="email" name="email" />
                    </FormGroup>
                  </Col> 
                </Row>  
                <Row>
                  <Col xs="12">
                    <FormGroup>
                    <Label htmlFor="adresse">Adresse</Label>
                    <Input type="textarea" value={adresse} onChange={this.handleChange} id="adresse" name="adresse" />
                    </FormGroup>
                  </Col>
                </Row> 
              </CardBody>
            </Card>
          </Col>
        </Row>
        </Form>
      </div>
    )
  }
 

}

export default LaboratoireCreate;
