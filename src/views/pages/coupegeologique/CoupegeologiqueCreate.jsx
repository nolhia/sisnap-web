import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, FormGroup,  Input, Label, Row, Form } from 'reactstrap';
import { userService } from '../../../services';
import { coupegeologiqueService } from '../../../services';
import { callMaker } from '../../../helpers'; 

class CoupegeologiqueCreate extends Component {
  constructor(props) {
    super(props);
      this.state = {
          id: '',
          code: '',
          cote_toit: '',
          cote_mur: '', 
          note: '', 
          station_id: '', 
          code_lithologique_id: '', 
          loading: false, 
          mode_edition: 'no',
          msg: '',
          error: ''
      }
      this.baseState = this.state;
      this.handleSubmit = this.handleSubmit.bind(this);
      this.handleChange = this.handleChange.bind(this);  
      this.resetForm = this.resetForm.bind(this);  
  }
// initialiser
  async componentDidMount() {  
      const stations = await callMaker('get', '/stations'); 
      const codeslithologiques = await coupegeologiqueService.getCodesLithologiques();  
      if(this.props.match.params.id){
          const coupegeologique= await coupegeologiqueService.find(this.props.match.params.id); 
          this.setState({ isLoaded: true, mode_edition:'yes', ...coupegeologique });
      } 
      this.setState({ stations: stations, codeslithologiques: codeslithologiques });
  }
// soumission de la requete
  handleSubmit = async  (event) => {
      event.preventDefault()
      const data = {
          id: event.target.id.value,
          mode_edition: event.target.mode_edition.value,
          cote_toit: event.target.cote_toit.value,
          cote_mur: event.target.cote_mur.value, 
          code_lithologique_id: event.target.code_lithologique_id.value,
          note: event.target.note.value, 
          station_id: event.target.station_id.value 
      }
 
      if(data.id === ''){
        delete data['id'];
      } 
      delete data['mode_edition'];
// controle des donnees
      if (data['cote_mur'] != null || data['cote_toit'] != null) {
        if (data['cote_mur'] === null) {
          this.setState({ error: "Côte mûr ne peut être vide si côte toit est fourni", loading: false });
          return;
        } else if (data['cote_toit'] === null) {
          this.setState({ error: "Côte toit ne peut être vide si côte mur est fourni", loading: false });
          return;
        }
        else if (data['cote_toit'] > data['cote_mur']) {
          this.setState({ error: "Côte mûr doit être supérieur à côte toit", loading: false });
          return;
        }
      }

      this.setState({ loading: true }); 
      coupegeologiqueService.save(data)
      .then(
          response => { 
            if(response.status === 401){
              this.setState({ error: response.message, loading: false });
              const { from } = this.props.location.state || { from: { pathname: "/" } };
              this.props.history.push(from);
              userService.logout();
              window.location.reload(true);
            } else if(response.status >= 400){
              this.setState({ error: response.message, loading: false });
            }
            else{
              
              this.setState({ loading: false, msg: "Traitement effectué avec succès" });
            } 
          },
          error => {  
            if(typeof error === 'string'){
                this.setState({ error , loading: false })
            }
            else{
                this.setState({ error: 'Connexion refusée.' , loading: false })
            } 
          } 
      );
    
  }
  // suivi changement
  handleChange(e){
      this.setState({[e.target.name]: e.target.value});  
  } 
// reinitialiser
  resetForm = () => {
    this.setState(() => this.baseState);
  } 
// afficher
  render() {
    const { id, cote_toit, cote_mur, note, station_id, code_lithologique_id, codeslithologiques, error, msg, mode_edition, stations } = this.state;

    return (
      <div className="animated fadeIn">
         <br/>
          {error &&
              <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
              <div className={'alert alert-success'}>{msg}</div>
          }
        <Form onSubmit={this.handleSubmit} method="POST">
        <Row>
          <Col>
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i><strong>Coupe géologique</strong>
                <div className="card-header-actions">
                  <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button> 
                  &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm } color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                  &nbsp;&nbsp; <Link to="/parametre/coupegeologiques" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                </div>
              </CardHeader>
              <CardBody>
                <Row> 
                  <Col xs="6">
                    <FormGroup>
                        <Label htmlFor="station_id">Piézomètre *</Label> 
                        <Input type="select" value={station_id} onChange={this.handleChange} name="station_id" id="station_id" required>
                          <option value='' key=''></option>
                          {stations && stations.length > 0 && stations.map(station => <option key={station.id} value={station.id}>{station.code} {station.titre}</option>)}
                        </Input> 
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id'/>
                    <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition'/> 
                    </FormGroup>
                  </Col>
                  <Col xs="6">
                    <FormGroup>
                      <Label htmlFor="code_lithologique_id">Code Lithologie *</Label> 
                      <Input type="select" value={code_lithologique_id} onChange={this.handleChange} name="code_lithologique_id" id="code_lithologique_id" required>
                          <option value='' key=''></option>
                          {codeslithologiques && codeslithologiques.length > 0 && codeslithologiques.map(codelithologique => <option key={codelithologique.id} value={codelithologique.id}>{codelithologique.code} : {codelithologique.titre}</option>)}
                      </Input> 
                    </FormGroup>
                  </Col>  
                </Row>  
                <Row>
                  <Col xs="6">
                    <FormGroup>
                    <Label htmlFor="cote_toit">Cote toit *</Label>
                    <Input type="number" value={cote_toit} onChange={this.handleChange} id="cote_toit" name="cote_toit" required />
                    </FormGroup>
                  </Col>
                  <Col xs="6">
                    <FormGroup>
                    <Label htmlFor="cote_mur">Cote mûr *</Label>
                    <Input type="text" value={cote_mur} onChange={this.handleChange} id="cote_mur" name="cote_mur" required />
                    </FormGroup>
                  </Col>
                  
                </Row>  
                <Row>
                  <Col xs="12">
                    <FormGroup>
                    <Label htmlFor="note">Commentaire</Label>
                    <Input type="textarea" value={note} onChange={this.handleChange} id="note" name="note" />
                    </FormGroup>
                  </Col>
                </Row> 
              </CardBody>
            </Card>
          </Col>
        </Row>
        </Form>
      </div>
    )
  }
 

}

export default CoupegeologiqueCreate;
