import Coupegeologiques from './Coupegeologiques';
import CoupegeologiqueCreate from './CoupegeologiqueCreate';

export {
    Coupegeologiques,
    CoupegeologiqueCreate
};