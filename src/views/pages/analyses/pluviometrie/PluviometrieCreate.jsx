import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Form } from 'reactstrap';
import { callMaker } from '../../../../helpers';
import { parametersService } from '../../../../services';

class PluviometrieCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      date_pluie: '',
      heure_pluie: '',
      quantite: '',
      station_suivi_id: '',
      note: '',
      stations: [],
      loading: false,
      msg: '',
      error: ''
    }
    this.baseState = this.state;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }
// initialiser
  async componentDidMount() {
    const stations = await callMaker('get', '/stations');
    if (this.props.match.params.id) {
      const objet = await callMaker('get', `/collecte/pluviometrie/${this.props.match.params.id}`);
      this.setState({ isLoaded: true, ...objet });
      this.baseState = this.state;
    }
    this.setState({ stations: stations });
  }
// soumettre la requete
  handleSubmit = async (event) => {
    event.preventDefault();
    let data = {
      id: event.target.id.value,
      date_pluie: event.target.date_pluie.value,
      heure_pluie: event.target.heure_pluie.value,
      quantite: event.target.quantite.value,
      station_suivi_id: event.target.station_suivi_id.value,
      note: event.target.note.value,
      status: event.target.status.value === '' ? 'pending' : event.target.status.value
    };

    if (data['date_pluie'] !== null || data['date_pluie'] !== undefined) {
      if ( moment(data['date_pluie']).isAfter(moment())) {
        this.setState({ error: "La date de pluie doit être antérieure à la date du jour.", loading: false });
        return;
      } 
    }

    const user = await parametersService.getUserInformation();  // utilisateur connecte

    if (!this.validateForm(data)) {
      return;
    }
    if (data['heure_pluie'] === '' || data['heure_pluie'] === null ) {
      delete data['heure_pluie'];
    }
    let method = 'put';
    if (data.id === '' || data.id === 0 || data.id === undefined) {
      delete data['id'];
      if(user){
          data['ajouter_par'] = user.id;
      }
      method = 'post';
    }
    else{ // modification
      if(user){
          data['modifier_par'] = user.id;
          data['modifier_le'] = new Date();
      }
    }

    this.setState({ loading: true });
    callMaker(method, `/collecte/pluviometrie`, data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error, loading: false })
          }
          else {
            this.setState({ error: 'Connexion refusée.', loading: false })
          }
        }
      );

  }
  // suivre changement
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
// valider
  validateForm(data) {
    let message = "";
    
    this.setState({ error: message });

    return message === "" ? true : false;
  }
// reinitialiser les formulaires
  resetForm = () => {
    this.setState(() => this.baseState)
  }
// affichage
  render() {
    const { id, date_pluie, heure_pluie, quantite, stations, station_suivi_id, note, error, msg, status } = this.state;
    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <Input type="hidden" value={status} onChange={this.handleChange} name='status' />
                  <i className="fa fa-align-justify" />
                  <strong>Pluviométrie</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button>
                    &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm} color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/collecte/pluviometries" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="date_pluie">Date pluie *</Label>
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id' id='id' />
                        <Input type="date" value={date_pluie} onChange={this.handleChange} id="date_pluie" name="date_pluie" required />
                      </FormGroup>
                    </Col>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="heure_pluie">Heure pluie </Label>
                        <Input type="time" value={heure_pluie} onChange={this.handleChange} id="heure_pluie" name="heure_pluie" />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="station_suivi_id">Station de suivi *</Label>
                        <Input type="select" id="station_suivi_id" name="station_suivi_id" value={station_suivi_id} onChange={this.handleChange} required > 
                          {stations && stations.length > 0 && stations.map(station => <option key={station.id} value={station.id}>{station.code} {station.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="quantite">Quantité (mm) *</Label>
                        <Input type="number" value={quantite} onChange={this.handleChange} id="quantite" name="quantite" required />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="note">Commentaire</Label>
                        <Input type="textarea" id="note" value={note} onChange={this.handleChange} name="note" />
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }

}

export default PluviometrieCreate;
