import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Form } from 'reactstrap';
import { callMaker } from '../../../../helpers';
import { parametersService } from '../../../../services';

class PiezometrieCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      date_releve: '',
      heure_releve: '',
      hauteur_soir: '',
      hauteur_matin: '',
      hauteur_moyenne: '',
      station_suivi_id: '',
      nom_ouvrage: '',
      note: '',
      debit: '',
      charge: '',
      voltage: '',
      stations: [],
      loading: false,
      msg: '',
      error: ''
    }
    this.baseState = this.state;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }
// initialiser
  async componentDidMount() {
    const stations = await callMaker('get', '/stations');
    if (this.props.match.params.id) {
      const objet = await callMaker('get', `/collecte/hydrometrie/${this.props.match.params.id}`);
      this.setState({ isLoaded: true, ...objet });
      this.baseState = this.state;
    }
    this.setState({ stations: stations });
  }
// soumettre
  handleSubmit = async (event) => {
    event.preventDefault();
    let data = {
      id: event.target.id.value,
      date_releve: event.target.date_releve.value,
      heure_releve: event.target.heure_releve.value != '' ? event.target.heure_releve.value : null,
      hauteur_soir: event.target.hauteur_soir.value != '' ? event.target.hauteur_soir.value : null,
      hauteur_matin: event.target.hauteur_matin.value != '' ? event.target.hauteur_matin.value : null,
      hauteur_moyenne: event.target.hauteur_moyenne.value != '' ? event.target.hauteur_moyenne.value : null,
      station_suivi_id: event.target.station_suivi_id.value,
      nom_ouvrage: event.target.nom_ouvrage.value,
      debit: event.target.debit.value != '' ? event.target.debit.value : null,
      charge: event.target.charge.value != '' ? event.target.charge.value : null,
      voltage: event.target.voltage.value != '' ? event.target.voltage.value : null,
      note: event.target.note.value,
      status: event.target.status.value === '' ? 'pending' : event.target.status.value
    }; 
    
    if (data['date_releve'] != null || data['date_releve'] != undefined) {
      if ( moment(data['date_releve']).isAfter(moment())) {
        this.setState({ error: "La date de relevé doit être antérieure à la date du jour.", loading: false });
        return;
      } 
    }

    const user = await parametersService.getUserInformation();  // utilisateur connecte
 
    if (data['heure_releve'] === '' || data['heure_releve'] === null ) {
      delete data['heure_releve'];
    }
    let method = 'put';
    if (data.id === '' || data.id === 0 || data.id === undefined) {
      delete data['id'];
      if (user) {
        data['ajouter_par'] = user.id;
      }
      method = 'post';
    }
    else { // modification
      if (user) {
        data['modifier_par'] = user.id;
        data['modifier_le'] = new Date();
      }
    }

    this.setState({ loading: true });
    callMaker(method, `/collecte/hydrometrie`, data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error, loading: false })
          }
          else {
            this.setState({ error: 'Connexion refusée.', loading: false })
          }
        }
      );

  }
  // suivre le changement
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
 // reinitialiser le formulaire
  resetForm = () => {
    this.setState(() => this.baseState)
  }
// affichage
  render() {
    const { id, date_releve, heure_releve, hauteur_soir, hauteur_matin, hauteur_moyenne, stations, station_suivi_id, nom_ouvrage, debit, charge, voltage, note, error, msg, status } = this.state;
    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader style={{color: '#246195'}}>
                  <Input type="hidden" value={status} onChange={this.handleChange} name='status' />
                  <i className="fa fa-align-justify" />
                  <strong>Hydrométrie</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button>
                    &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm} color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/collecte/hydrometries" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="date_releve">Date relevé *</Label>
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id' id='id' />
                        <Input type="date" value={date_releve} onChange={this.handleChange} id="date_releve" name="date_releve" required />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="heure_releve">Heure relevé</Label>
                        <Input type="time" value={heure_releve} onChange={this.handleChange} id="heure_releve" name="heure_releve" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="station_suivi_id">Station de suivi *</Label>
                        <Input type="select" id="station_suivi_id" name="station_suivi_id" value={station_suivi_id} onChange={this.handleChange} required >
                          {stations && stations.length > 0 && stations.map(station => <option key={station.id} value={station.id}>{station.code} {station.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="nom_ouvrage">Nom ouvrage</Label>
                        <Input type="text" id="nom_ouvrage" name="nom_ouvrage" value={nom_ouvrage} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="hauteur_matin">Hauteur matin (mm)</Label>
                        <Input type="number" id="hauteur_matin" value={hauteur_matin} onChange={this.handleChange} name="hauteur_matin" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="hauteur_soir">Hauteur soir (mm)</Label>
                        <Input type="number" id="hauteur_soir" value={hauteur_soir} onChange={this.handleChange} name="hauteur_soir" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="hauteur_moyenne">Hauteur moyenne (mm)</Label>
                        <Input type="number" id="hauteur_moyenne" value={hauteur_moyenne} onChange={this.handleChange} name="hauteur_moyenne" />
                      </FormGroup>
                    </Col>

                  </Row>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="debit">Débit</Label>
                        <Input type="number" id="debit" value={debit} onChange={this.handleChange} name="debit" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="charge">Charge</Label>
                        <Input type="number" id="charge" value={charge} onChange={this.handleChange} name="charge" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="voltage">Voltage</Label>
                        <Input type="number" id="voltage" value={voltage} onChange={this.handleChange} name="voltage" />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="note">Commentaire</Label>
                        <Input type="textarea" id="note" value={note} onChange={this.handleChange} name="note" />
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }

}

export default PiezometrieCreate;
