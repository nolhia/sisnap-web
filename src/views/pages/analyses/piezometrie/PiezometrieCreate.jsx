import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import { Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Form } from 'reactstrap';
import { callMaker } from '../../../../helpers';
import { parametersService } from '../../../../services';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel'; 

class PiezometrieCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      date_mesure: '',
      heure_mesure: '',
      temperature: '',
      conductivite: '',
      ph: '',
      repere_eau: '',
      repere_sol: '',
      niveau_statique_sol: '',
      niveau_dynamique_sol: false,
      station_suivi_id: '',
      note: '',
      stations: [],
      loading: false,
      status: '', 
      msg: '',
      error: ''
    }
    this.baseState = this.state;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }
// initialiser
  async componentDidMount() {
    const stations = await callMaker('get', '/stations');
    if (this.props.match.params.id) {
      const objet = await callMaker('get', `/collecte/piezometrie/${this.props.match.params.id}`);
      this.setState({ isLoaded: true, ...objet });
      this.baseState = this.state;
    }

    this.setState({ stations: stations });
  }
// soumettre la requete
  handleSubmit = async (event) => {
    event.preventDefault();
    let data = {
      id: event.target.id.value,
      date_mesure: event.target.date_mesure.value,
      heure_mesure: event.target.heure_mesure.value != '' ? event.target.heure_mesure.value : null,
      temperature: event.target.temperature.value != '' ? event.target.temperature.value : null,
      conductivite: event.target.conductivite.value != '' ? event.target.conductivite.value : null,
      ph: event.target.ph.value != '' ? event.target.ph.value : null,
      repere_eau: event.target.repere_eau.value != '' ? event.target.repere_eau.value : null,
      repere_sol: event.target.repere_sol.value != '' ? event.target.repere_sol.value : null,
      niveau_statique_sol: event.target.niveau_statique_sol.value != '' ? event.target.niveau_statique_sol.value : null,
      niveau_dynamique_sol: event.target.niveau_dynamique_sol.value,
      station_suivi_id: event.target.station_suivi_id.value, 
      note: event.target.note.value,
      status: event.target.status.value === '' ? 'pending' : event.target.status.value
    };

    if (data['date_mesure'] != null || data['date_mesure'] != undefined) {
      if ( moment(data['date_mesure']).isAfter(moment())) {
        this.setState({ error: "La date de mesure doit être antérieure à la date du jour.", loading: false });
        return;
      } 
    }

    const user = await parametersService.getUserInformation();  // utilisateur connecte
 
    if (data['heure_mesure'] === '' || data['heure_mesure'] === null ) {
      delete data['heure_mesure'];
    }

    let method = 'put';
    if (data.id === '' || data.id === 0 || data.id === undefined) {
      delete data['id'];
      if (user) {
        data['ajouter_par'] = user.id;
      }
      method = 'post';
    }
    else { // modification
      if (user) {
        data['modifier_par'] = user.id;
        data['modifier_le'] = new Date();
      }
    }

    this.setState({ loading: true });
    callMaker(method, `/collecte/piezometrie`, data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error, loading: false })
          }
          else {
            this.setState({ error: 'Connexion refusée.', loading: false })
          }
        }
      );

  }
  //suivre changement
  handleChange(e) {
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    this.setState({ [e.target.name]: value });
  }
  // reinitialiser
  resetForm = () => {
    this.setState(() => this.baseState)
  }

// affichage
  render() {
    const { id, date_mesure, heure_mesure, temperature, conductivite, ph, repere_sol,
      repere_eau, niveau_statique_sol, niveau_dynamique_sol, stations, station_suivi_id, note, error, msg, status } = this.state;
    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader style={{color: '#246195'}}>
                  <Input type="hidden" value={status} onChange={this.handleChange} name='status' />
                  <i className="fa fa-align-justify" />
                  <strong>Piézométrie</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button>
                    &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm} color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/collecte/piezometries" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="date_mesure">Date mesure *</Label>
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id' id='id' />
                        <Input type="date" value={date_mesure} onChange={this.handleChange} id="date_mesure" name="date_mesure" required />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="heure_mesure">Heure mesure</Label>
                        <Input type="time" value={heure_mesure} onChange={this.handleChange} id="heure_mesure" name="heure_mesure" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="station_suivi_id">Station de suivi *</Label>
                        <Input type="select" id="station_suivi_id" name="station_suivi_id" value={station_suivi_id} onChange={this.handleChange} required >
                          {stations && stations.length > 0 && stations.map(station => <option key={station.id} value={station.id}>{station.code} {station.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <FormControlLabel
                          control={<Checkbox checked={niveau_dynamique_sol} onChange={this.handleChange} value={niveau_dynamique_sol} id="niveau_dynamique_sol" name="niveau_dynamique_sol" />}
                          label="Niveau dynamique sol"
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="repere_sol">Repère sol</Label>
                        <Input type="number" id="repere_sol" value={repere_sol} onChange={this.handleChange} name="repere_sol" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="repere_eau"> Repère eau</Label>
                        <Input type="number" id="repere_eau" value={repere_eau} onChange={this.handleChange} name="repere_eau" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="niveau_statique_sol">Niveau statique sol</Label>
                        <Input type="number" value={niveau_statique_sol} onChange={this.handleChange} id="niveau_statique_sol" name="niveau_statique_sol" />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="temperature">Température (°C)</Label>
                        <Input type="number" id="temperature" value={temperature} onChange={this.handleChange} name="temperature" />
                      </FormGroup>
                    </Col>
                  </Row> 
                  <Row>
                  <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="conductivite">Conductivité (µs/cm)</Label>
                        <Input type="number" id="conductivite" value={conductivite} onChange={this.handleChange} name="conductivite" />
                      </FormGroup>
                    </Col> 
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="ph">pH</Label>
                        <Input type="number" id="ph" value={ph} onChange={this.handleChange} name="ph" />
                      </FormGroup>
                    </Col>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="note">Commentaire</Label>
                        <Input type="textarea" id="note" value={note} onChange={this.handleChange} name="note" />
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }

}

export default PiezometrieCreate;
