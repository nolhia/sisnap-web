import React, { Component } from 'react';
import moment from 'moment';
import { userService, bacteriologieService, laboratoireService, elementchimiqueService, stationService } from '../../../../services';

import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  Form
} from 'reactstrap';
 
class AnalyseBacteriologiqueCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      date_prelevement: '',
      heure_prelevement: '',
      date_analyse: '',
      coliforme_totaux: '',
      coliforme_fecaux: '',
      element_chimique: '',
      note: '',
      laboratoire: '',
      chimiste: '',
      station: '',
      longitude: '',
      latitude: '',
      altitude: '',
      nom_localite: '',
      mode_edition: 'no',
      status: '',
      msg: '',
      error: '',
      absent_coliforme_totaux: false,
      disabled_coliforme_totaux: false, 
      disabled_coliforme_fecaux: false
    }

    this.baseState = this.state;
    this.handleChange = this.handleChange.bind(this);
    this.handleChangeAbsent = this.handleChangeAbsent.bind(this);
    this.resetForm = this.resetForm.bind(this);
    this.elementChimiques = '';
  }
// suivre les changements
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
// suivre le changement du champ coliforme totaux
  handleChangeAbsent(e) {
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    if (e.target.name === 'absent_coliforme_totaux') {
      this.setState({ disabled_coliforme_totaux: !this.state.disabled_coliforme_totaux })
    } else if (e.target.name === 'absent_coliforme_fecaux') {
      this.setState({ disabled_coliforme_fecaux: !this.state.disabled_coliforme_fecaux })
    }
    this.setState({ [e.target.name]: value });
  }
// valider les donnees
  validateForm(data) {
    let message = "";
    if (data.station === '' || data.station === null || data.station === undefined) {
      if (data.longitude === null || data.latitude === null || data.altitude === null)
        message = message + ` Si la station de suivi n'est pas sélectionnée, vous devez remplir les champs longitude, latitude et altitude. `;
    }

    if (data['date_prelevement'] != null || data['date_prelevement'] != undefined) {
      if ( moment(data['date_prelevement']).isAfter(moment())) {
        this.setState({ error: "La date de prélevement doit être antérieure à la date du jour.", loading: false });
        return;
      } 
    }

    if (data['date_analyse'] != null || data['date_analyse'] != undefined) {
      if ( moment(data['date_analyse']).isAfter(moment())) {
        this.setState({ error: "La date d'analyse doit être antérieure à la date du jour.", loading: false });
        return;
      } 
    }

    if (data['date_prelevement'] != null && data['date_analyse'] != undefined) {
      if ( moment(data['date_prelevement']).isAfter(data['date_analyse'])) {
        this.setState({ error: "La date de prélevement doit être antérieure à la date d'analyse.", loading: false });
        return;
      } 
    }

    this.setState({ error: message });

    return message === "" ? true : false;
  }
// reinitialiser le formulaire
  resetForm = () => {
    this.setState(() => this.baseState)
  }
// soumettre la requerte
  handleSubmit = async (event) => {
    event.preventDefault();
    var element_chimique = '{';
    this.elementChimiques.map(

      elementChimique => {
        element_chimique = element_chimique +
          '"' + elementChimique.code +
          '":' + '"' +
          event.target[elementChimique.code].value + '",';
      }

    )
    element_chimique = element_chimique.substring(0, element_chimique.length - 1) + '}';

    var data = {
      id: event.target.id.value,
      mode_edition: event.target.mode_edition.value,
      date_prelevement: event.target.date_prelevement.value,
      heure_prelevement: event.target.heure_prelevement.value !== '' ? event.target.heure_prelevement.value : null,
      date_analyse: event.target.date_analyse.value,
      heure_debut_analyse: event.target.heure_debut_analyse.value !== '' ? event.target.heure_debut_analyse.value : null,
      heure_fin_analyse: event.target.heure_fin_analyse.value !== '' ? event.target.heure_fin_analyse.value : null,
      coliforme_fecaux: event.target.coliforme_fecaux.value,
      coliforme_totaux: event.target.coliforme_totaux.value,
      element_chimique: [JSON.parse(element_chimique)],
      note: event.target.note.value,
      laboratoire: event.target.laboratoire.value,
      chimiste: event.target.chimiste.value,
      station: event.target.station.value,
      longitude: event.target.longitude.value !== '' ? event.target.longitude.value : null,
      latitude: event.target.latitude.value !== '' ? event.target.latitude.value : null,
      altitude: event.target.altitude.value !== '' ? event.target.altitude.value : null,
      nom_localite: event.target.nom_localite.value,
      status: event.target.status.value === '' ? 'pending' : event.target.status.value
    };

    if (!this.validateForm(data)) {
      console.log("Form Validation Failed => " + data)
      return;
    }

    if (data.id === '') {
      delete data['id'];
    }
    if (data.station === '') {
      delete data['station'];
    }
    delete data['mode_edition'];

    if (this.state.absent_coliforme_totaux) {
      data['coliforme_totaux'] = 0;
    }
    if (this.state.absent_coliforme_fecaux) {
      data['coliforme_fecaux'] = 0;
    }

    this.setState({ loading: true });

    bacteriologieService.save(data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => this.setState({ error, loading: false })
      );

  }
// initialiser 
  async componentDidMount() {
    this.setState({ isLoaded: false });
    this.elementChimiques = await elementchimiqueService.getByAnalyse('bacteriologie');
    const laboratoires = await laboratoireService.getAll();
    const stations = await stationService.getAll();

    if (this.props.match.params.id) {
      const analyse = await bacteriologieService.find(this.props.match.params.id);
      this.setState({ isLoaded: true, mode_edition: 'yes', ...analyse });

      if (analyse.coliforme_totaux == 0) {
        this.setState({ absent_coliforme_totaux: true, disabled_coliforme_totaux: true });
      }
      if (analyse.coliforme_fecaux == 0) {
        this.setState({ absent_coliforme_fecaux: true, disabled_coliforme_fecaux: true });
      }

      this.baseState = this.state;

      this.elementChimiques.map(
        elementChimique => {
          this.setState({ [elementChimique.code]: analyse.element_chimique[0][elementChimique.code] })
        }
      )
    }

    this.setState({
      elementChimiques: this.elementChimiques,
      laboratoires: laboratoires,
      stations: stations
    });

  }
// affichage
  render() {
    const { id, date_prelevement, heure_prelevement, date_analyse, heure_debut_analyse, heure_fin_analyse, coliforme_totaux, coliforme_fecaux, elementChimiques, laboratoires, stations, longitude, latitude, altitude, nom_localite, note, laboratoire, chimiste, station, status, error, msg, mode_edition } = this.state;
    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Analyse Bactériologique</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary">
                      <i className="fa fa-dot-circle-o" /> Enregistrer
                  </Button>
                    &nbsp;&nbsp;
                  <Button type="reset" size="sm" color="danger" onClick={this.resetForm}>
                      <i className="fa fa-ban" /> Réinitialiser
                  </Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Input type="hidden" value={id} onChange={this.handleChange} name='id' id='id' />
                    <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition' />
                    <Input type="hidden" value={status} onChange={this.handleChange} name='status' />
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="date_prelevement">Date prélèvement*</Label>
                        <Input type="date" id="date_prelevement" name="date_prelevement" value={date_prelevement} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                    <Col xs="2">
                      <FormGroup>
                        <Label htmlFor="heure_prelevement">Heure prélèvement</Label>
                        <Input type="time" id="heure_prelevement" name="heure_prelevement" value={heure_prelevement} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="date_analyse">Date analyse*</Label>
                        <Input type="date" id="date_analyse" name="date_analyse" value={date_analyse} onChange={this.handleChange} required />
                      </FormGroup>
                    </Col>
                    <Col xs="2">
                      <FormGroup>
                        <Label htmlFor="heure_debut_analyse">Heure debut analyse</Label>
                        <Input type="time" id="heure_debut_analyse" name="heure_debut_analyse" value={heure_debut_analyse} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col xs="2">
                      <FormGroup>
                        <Label htmlFor="heure_fin_analyse">Heure fin analyse</Label>
                        <Input type="time" id="heure_fin_analyse" name="heure_fin_analyse" value={heure_fin_analyse} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="station">Station de suivi</Label>
                        <Input type="select" id="station" name="station" value={station} onChange={this.handleChange} >
                          <option value='' key=''></option>
                          {stations && stations.length > 0 && stations.map(station => <option key={station.id} value={station.id}>{station.code} {station.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="2">
                      <FormGroup>
                        <Label htmlFor="longitude">Longitude</Label>
                        <Input type="number" id="longitude" placeholder="5.2580" name="longitude" value={longitude} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col xs="2">
                      <FormGroup>
                        <Label htmlFor="latitude">Latitude</Label>
                        <Input type="number" id="latitude" placeholder="14.8905" name="latitude" value={latitude} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col xs="2">
                      <FormGroup>
                        <Label htmlFor="altitude">Altitude</Label>
                        <Input type="number" id="altitude" name="altitude" value={altitude} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="nom_localite">Nom localité</Label>
                        <Input type="text" id="nom_localite" name="nom_localite" value={nom_localite} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="laboratoire">Laboratoire*</Label>
                        <Input type="select" id="laboratoire" name="laboratoire" value={laboratoire} onChange={this.handleChange} required >
                          {laboratoires && laboratoires.length > 0 && laboratoires.map(laboratoire => <option key={laboratoire.id} value={laboratoire.id}>{laboratoire.code} {laboratoire.titre}</option>)}
                        </Input>
                      </FormGroup>
                    </Col>
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="chimiste">Biologiste</Label>
                        <Input type="text" id="chimiste" placeholder="Nom" name="chimiste" value={chimiste} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                  </Row> 
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="coliforme_totaux">Coliforme totaux*</Label>
                        <Input type="number" id="coliforme_totaux" name="coliforme_totaux" value={coliforme_totaux} onChange={this.handleChange} disabled={(this.state.disabled_coliforme_totaux) ? "disabled" : ""} required />
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="coliforme_fecaux">Coliforme fécaux*</Label>
                        <Input type="number" id="coliforme_fecaux" name="coliforme_fecaux" value={coliforme_fecaux} onChange={this.handleChange} disabled={(this.state.disabled_coliforme_fecaux) ? "disabled" : ""} required />
                      </FormGroup>
                    </Col> 
                    <Col xs="6">
                      <FormGroup>
                        <Label htmlFor="note">Commentaire</Label>
                        <Input type="textarea" name="note" id="note" value={note} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr color="info" />

                  <Row>
                    {elementChimiques && elementChimiques.length > 0 && elementChimiques.map(
                      elementChimique =>
                        <Col key={elementChimique.id} xs="3">
                          <FormGroup className="elementChimiques">
                            <Label htmlFor={elementChimique.code}>{elementChimique.titre}</Label>
                            <Input type="number" id={elementChimique.code} name={elementChimique.code} value={this.state[elementChimique.code]} onChange={this.handleChange} />
                          </FormGroup>
                        </Col>
                    )}
                  </Row>

                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }

}

export default AnalyseBacteriologiqueCreate;
