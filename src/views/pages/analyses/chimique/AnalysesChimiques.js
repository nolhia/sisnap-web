import React, { Component } from "react";
import { Link } from "react-router-dom";
import { userService, analysechimiquesService } from '../../../../services';

import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Input, Modal, ModalBody, ModalFooter, ModalHeader
} from "reactstrap";
import { callFileTransfer, formatDate } from '../../../../helpers';
import { CSVLink } from 'react-csv';

import MaterialTable from 'material-table';
import { forwardRef } from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';
import Status from '@material-ui/icons/PowerSettingsNewOutlined';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

class AnalysesChimiques extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],
      exportData: [],
      modalImport: false,
      selectedFile: null,
      filename: 'Analyses chimiques',
      currentUser: null
    };
    this.supprimer = this.supprimer.bind(this);
    this.toggle = this.toggle.bind(this);
  }
// initialiser
  async componentDidMount() {
    //On récupère l'utilisateur courant
    userService.currentUser.subscribe(x => this.setState({ currentUser: x }));
    //On récupère la liste de toutes les analyses chimiques
    const data = await analysechimiquesService.getAll();
    //on  récupère les données au format export en excel
    const exportData = await analysechimiquesService.exportData();
    const columns = [
      { title: 'D analyse', render: rowData => formatDate(rowData.date_analyse)},
      { title: 'H analyse', field: 'heure_analyse' },
      { title: '°C', field: 'temperature' },
      { title: 'Cond(µs/cm)', field: 'conductivite' },
      { title: 'pH', field: 'ph' },
      { title: 'Commentaire', field: 'note' }
    ]
    this.setState({ isLoaded: true, data: data, exportData: exportData, columns: columns });
  }
// supprimer
  async supprimer(objet) {
    try {
      analysechimiquesService.remove(objet)
      this.state.data.splice(this.state.data.indexOf(objet), 1);
      this.setState({ isLoaded: true, msg: "Traitement effectué avec succès" });
    } catch (error) {
      this.setState({ isLoaded: true, error: 'Erreur inatendue' });
    }
  }
// valider les donnees
  async valider(objet) {
    try {
      this.setState({ loading: true });
      objet.status = "validate";
      delete objet['tableData'];
      analysechimiquesService.save(objet);
      this.setState({ isLoaded: true, msg: "Traitement effectué avec succès" });
    } catch (error) {
      this.setState({ isLoaded: true, error: 'Erreur inatendue' });
    }
  }
// chargement de fichier
  onFileChangeHandler = event => {
    this.setState({
      selectedFile: event.target.files[0]
    })
  }

  toggle() {
    this.setState({
      modalImport: !this.state.modalImport
    });
  }
// importer
  onClickImportHandler = async () => {
    let data = new FormData()
    data.append('file', this.state.selectedFile);
    this.setState({ isLoaded: false, msg: "" });
    callFileTransfer('/collecte/analyse/chimiques/upload', data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: "Une erreure inatendue s'est produite durant l'opération. Veuillez vérifier le contenu de votre fichier SVP.", isLoaded: true, modalImport: false });
          }
          else {
            this.setState({ isLoaded: true, msg: "Traitement effectué avec succès", modalImport: false });
            window.location.reload(true);
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error, loading: false })
          }
          else {
            this.setState({ error: 'Connexion refusée.', loading: false })
          }
        })
  }
// affichage
  render() {
    const { isLoaded, data, exportData, columns, error, msg, filename, currentUser } = this.state;

    if (!isLoaded) {
      return <div>Loading ...</div>
    } else {
      return (
        <div className="animated fadeIn">
          <br />
          {error &&
            <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
            <div className={'alert alert-success'}>{msg}</div>
          }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify" />
                  <strong>Liste des Analyses physico-chimiques</strong>
                  <div className="card-header-actions">
                    {this.import_view()}
                    <Link to="/collecte/analyseschimiques/add">
                      <Button color="primary" size="sm" className="btn-square">
                        <i className="icon-plus" />
                        &nbsp;Nouveau
                      </Button>
                    </Link>
                    &nbsp;
                    <Button color="secondary" onClick={this.toggle} size="sm" className="btn-square"><i className="icon-arrow-down-circle"></i>&nbsp;Importer un fichier</Button>
                    &nbsp;
                    <Button color="secondary" size="sm" className="btn-square"><i className="icon-printer"></i>
                      &nbsp;<CSVLink data={exportData} filename={filename}>Exporter en Excel</CSVLink>
                    </Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <MaterialTable
                    title="Liste"
                    icons={tableIcons}
                    columns={columns}
                    data={data}
                    actions={[
                      {
                        icon: () => <Edit />,
                        tooltip: "Edit",
                        onClick: (event, rowData) => {
                          const link = `/collecte/analysesChimiques/edit/${rowData.id}`;
                          this.props.history.push(link);
                        }
                      },
                      {
                        icon: () => <Status />,
                        tooltip: "Valider les données",
                        hidden: currentUser && currentUser.user_info.role !== 2,
                        onClick: (event, rowData) => {
                          if (rowData.status === "pending") {
                            return this.valider(rowData);
                          } else {
                            this.setState({ isLoaded: true, error: 'Cette analyse chimique est deja validée' });
                          }
                        }
                      }
                    ]}
                    editable={{
                      onRowDelete: oldData => {
                        return this.supprimer(oldData);
                      },
                    }}
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      );
    }
  }

  import_view() {
    return (
      <Modal isOpen={this.state.modalImport} toggle={this.toggle} >
        <ModalHeader toggle={this.toggle}>Importer un fichier</ModalHeader>
        <ModalBody>
          <Input type="file" name="file" onChange={this.onFileChangeHandler} />
        </ModalBody>
        <ModalFooter>
          <Button color="primary" type="button" onClick={this.onClickImportHandler}>Executer</Button>
          <Button color="secondary" onClick={this.toggle}>Annuler</Button>
        </ModalFooter>
      </Modal>
    )

  }
}

export default AnalysesChimiques;
