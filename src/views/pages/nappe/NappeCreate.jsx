import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Form } from 'reactstrap';
import { nappeService, parametersService } from '../../../services';

class NappeCreate extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      code: '',
      type_nappe: '',
      cote_toit: '',
      cote_mur: '',
      note: '',
      titre: '',
      loading: false,
      mode_edition: 'no',
      msg: '',
      error: ''
    }
    this.baseState = this.state;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }
// initialiser
  async componentDidMount() {
    const typeNappes = await parametersService.getTypeNappes();
    this.setState({ typeNappes: typeNappes });
    if (this.props.match.params.id) {
      const nappe = await nappeService.find(this.props.match.params.id);
      this.setState({ isLoaded: true, mode_edition: 'yes', ...nappe });
      this.baseState = this.state;
    }
  }
// soumission de la requete
  handleSubmit = async (event) => {
    event.preventDefault()
    const data = {
      id: event.target.id.value,
      mode_edition: event.target.mode_edition.value,
      cote_toit: event.target.cote_toit.value !== '' ? event.target.cote_toit.value : null,
      cote_mur: event.target.cote_mur.value !== '' ? event.target.cote_mur.value : null,
      note: event.target.note.value,
      code: event.target.code.value,
      titre: event.target.titre.value,
      type_nappe: event.target.type_nappe.value,
    }
 
    if (data.id === '') {
      delete data['id']; 
    }
    delete data['mode_edition'];
    // validation 
    if (data['cote_mur'] != null || data['cote_toit'] != null) {
      if (data['cote_mur'] === null) {
        this.setState({ error: "Côte mûr ne peut être vide si côte toit est fourni", loading: false });
        return;
      } else if (data['cote_toit'] === null) {
        this.setState({ error: "Côte toit ne peut être vide si côte mur est fourni", loading: false });
        return;
      }
      else if (data['cote_toit'] > data['cote_mur']) {
        this.setState({ error: "Côte mûr doit être supérieur à côte toit", loading: false });
        return;
      }
    }

    this.setState({ loading: true });
    nappeService.save(data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error, loading: false })
          }
          else {
            this.setState({ error: 'Connexion refusée.', loading: false })
          }
        }
      );

  }
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
 // reinitialiser
  resetForm = () => {
    this.setState(() => this.baseState)
  }

  render() {
    const { id, cote_toit, cote_mur, note, code, titre, type_nappe, typeNappes, error, msg, mode_edition } = this.state;

    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Nappes</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button>
                    &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm} color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/parametre/nappes" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                  <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="code">Code *</Label>
                        <Input type="text" value={code} onChange={this.handleChange} id="code" name="code" required />
                      </FormGroup>
                    </Col>
                    <Col xs="8">
                      <FormGroup>
                        <Label htmlFor="titre">Nom de la nappe *</Label>
                        <Input type="text" value={titre} onChange={this.handleChange} id="titre" name="titre" required />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="type_nappe">Type nappe *</Label>
                        <Input type="select" id="type_nappe" name="type_nappe" value={type_nappe} onChange={this.handleChange} required >
                          {typeNappes && typeNappes.length > 0 && typeNappes.map(typeNappe => <option key={typeNappe.id} value={typeNappe.id}>{typeNappe.title}</option>)}
                        </Input>
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id' />
                        <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition' />
                      </FormGroup>
                    </Col>
                    <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="cote_toit">Côte toit</Label>
                        <Input type="number" value={cote_toit} onChange={this.handleChange} id="cote_toit" name="cote_toit" min={0}/>
                      </FormGroup>
                    </Col>
                    <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="cote_mur">Côte mûr</Label>
                        <Input type="number" value={cote_mur} onChange={this.handleChange} id="cote_mur" name="cote_mur" min={0}/>
                      </FormGroup>
                    </Col> 
                  </Row>

                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="note">Commentaire</Label>
                        <Input type="textarea" value={note} onChange={this.handleChange} id="note" name="note" />
                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }


}

export default NappeCreate;
