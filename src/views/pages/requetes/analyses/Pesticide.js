import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { Table } from 'reactstrap';
function DataRow(props) {
  const data = props.data

  return (
    <tr key={data.id.toString()}>
      <td>{data.date_prelevement} / {data.heure_prelevement}</td>
      <td>{data.date_analyse} / {data.heure_analyse}</td>
      <td>{data.station_titre}</td>
      <td>{data.laboratoire_titre}</td>
      <td>{data.note}</td>
    </tr>
  )
}

class Pesticide extends Component {

  constructor(props){
      super(props);
      this.state = {
          error : null,
          isLoaded : false,
          data : []
      };
  }

  async componentDidMount() {
    const data = this.props.data;
    this.setState({
      isLoaded: true,
      data: data
    });
  }


  render() {
      const { isLoaded, data } = this.state;

      if (this.state.error){
          return <div>Erreur pendant le chargement</div>
      } else if (!isLoaded) {
          return <div>Chargement ...</div>
      } else{
        return (
              <Table responsive hover>
                <thead>
                  <tr>
                    <th scope="col">D/H prélèvement </th>
                    <th scope="col">D/H analyse </th>
                    <th scope="col">Station </th>
                    <th scope="col">Laboratoire</th>
                    <th scope="col">Note</th>
                  </tr>
                </thead>
                <tbody>
                  {data && data.length > 0 && data.map((objet, index) =>
                    <DataRow key={index} data={objet}/>
                  )}
                </tbody>
              </Table>
        )
      }
  }

}

export default Pesticide;
