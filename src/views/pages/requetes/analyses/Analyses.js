import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Input, Label } from 'reactstrap';
import { parametersService  } from '../../../../services';
import { callMaker } from '../../../../helpers'; 
import Chimique  from './Chimique'; 
import Bacteriologique  from './Bacteriologique'; 
import Isotope  from './Isotope'; 
import Pesticide  from './Pesticide'; 
import { ZoneGeographique }  from '../../../../components'; 
import { CSVLink } from 'react-csv';

class Analyses extends Component {
  
  constructor(props){
      super(props);
      this.state = {
          error : null,
          isLoaded : true,
          data : [],        
          type_requete : [],
          type_requete_id: 1,
          station_id: 0,
          filename: 'Résultats'
      };   
      this.handleTypeAnalyse = this.handleTypeAnalyse.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() { 
    const type_requete = await parametersService.getTypeAnalyse(); 
    const stations = await callMaker('get', '/stations'); 
    this.setState({ 
      isLoaded: true,  
      type_requete: type_requete, 
      stations: stations
    });
  }  

  handleSubmit = async  (event) => {
      event.preventDefault()
      const type_requete_id = event.target.type_requete_id.value;
      let params = {
        type_requete_id: event.target.type_requete_id.value, 
        station_id: event.target.station_id.value, 
        date_debut: event.target.date_debut.value, 
        date_fin: event.target.date_fin.value, 
        region_id: event.target.region_id.value, 
        departement_id: event.target.departement_id.value, 
        commune_id: event.target.commune_id.value, 
      }
      if(type_requete_id == 1){
        params.conductivite = event.target.conductivite.value;
        params.temperature = event.target.temperature.value;
        params.ph = event.target.ph.value;
      }
      else if (type_requete_id == 2){
        params.coliforme_totaux = event.target.coliforme_totaux.value;
        params.coliforme_fecaux = event.target.coliforme_fecaux.value;
      } 

      this.setState({ isLoaded: false }); 
      callMaker('post', '/requete/analyses', params) 
      .then(
          response => {  
            if(response.status >= 400){
              this.setState({ error: response.message, lisLoaded: true });
            }
            else{  
              this.setState({ isLoaded: true, data: response, msg: "Requête traitée avec succès" });
            } 
          },
          error => { 
            if(typeof error === 'string'){
                this.setState({ error, data: [], isLoaded: true })
            }
            else{
                this.setState({ error: 'Connexion refusée.', data: [], isLoaded: true })
            } 
          } 
      ); 
  }

  handleTypeAnalyse(e) { 
    this.setState({ type_requete_id: e.target.value }); 
  }
  handleChange(e){
      this.setState({[e.target.name]: e.target.value});  
  }
  
  render() {
    const { isLoaded, data, type_requete_id, filename } = this.state;  
    
    if (this.state.error){
        return <div>Error in loading</div>
    } else if (!isLoaded) {
        return <div>Loading ...</div>
    } else{
      return (
        <div className="animated fadeIn">
          { this.filtre() }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> <strong>Résultats</strong> 
                  <div className="card-header-actions"> 
                    <Button color="secondary" size="sm" className="btn-square"><i className="icon-printer"></i>
                    &nbsp;<CSVLink data={data} filename={filename}>Exporter en Excel</CSVLink>
                    </Button> 
                  </div>
                </CardHeader>
                <CardBody>
                  { data && data.length == 0 && <strong>Aucune donnée à afficher</strong> }
                  { type_requete_id == 1 && data && data.length > 0 && <Chimique data = {data} {...this.props} /> }
                  { type_requete_id == 2 && data && data.length > 0 && <Bacteriologique data = {data} {...this.props} /> }
                  { type_requete_id == 3 && data && data.length > 0 && <Isotope data = {data} {...this.props} /> }
                  { type_requete_id == 4 && data && data.length > 0 && <Pesticide data = {data} {...this.props} /> }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }

  filtre() { 
    const {  type_requete, stations, type_requete_id, station_id } = this.state; 
    return (
      <div className="animated fadeIn">
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Requête</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="danger"><i className="fa fa-dot-circle-o"></i> Executer</Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="type_requete_id">Type de requête</Label>
                        <Input type="select" id="type_requete_id" name="type_requete_id" value={type_requete_id} onChange={this.handleTypeAnalyse} >
                          {type_requete && type_requete.length > 0 && type_requete.map(type => <option key={type.id} value={type.id}>{type.titre}</option>)}
                        </Input> 
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="station_id">Station de suivi</Label>
                        <Input type="select" id="station_id" name="station_id" value={station_id} onChange={this.handleChange} >
                          <option value='Tout' key='0'></option>
                          {stations && stations.length > 0 && stations.map(station => <option key={station.id} value={station.id}>{station.titre}</option>)}
                        </Input> 
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                      <Label htmlFor="date_debut">Date début</Label>
                      <Input type="date" id="date_debut" name="cout" />
                      </FormGroup>
                    </Col> 
                    <Col xs="3">
                      <FormGroup>
                      <Label htmlFor="date_fin">Date fin</Label>
                      <Input type="date" id="date_fin" name="date_fin" />
                      </FormGroup>
                    </Col> 
                  </Row> 
                  <ZoneGeographique {...this.props}/> 
                  <hr/> 
                  { type_requete_id == 1 && this.chimique() }
                  { type_requete_id == 2 && this.bacteriologique() }
                  { type_requete_id == 3 && this.isotope() }
                  { type_requete_id == 4 && this.pesticide() }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }

  chimique() {  
    return ( 
      <Row> 
        <Col xs="4">
          <FormGroup>
          <Label htmlFor="conductivite">Conductivité</Label>
          <Input type="text" id="conductivite" name="conductivite" />
          </FormGroup>
        </Col> 
        <Col xs="4">
          <FormGroup>
          <Label htmlFor="temperature">Température</Label>
          <Input type="text" id="temperature" name="temperature" />
          </FormGroup>
        </Col> 
        <Col xs="4">
          <FormGroup>
          <Label htmlFor="ph">pH</Label>
          <Input type="text" id="ph" name="ph" />
          </FormGroup>
        </Col> 
      </Row> 
    )
  }

  bacteriologique() {  
    return ( 
      <Row> 
        <Col xs="6">
          <FormGroup>
          <Label htmlFor="coliforme_fecaux">Coliformes fecaux</Label>
          <Input type="text" id="coliforme_fecaux" name="coliforme_fecaux" />
          </FormGroup>
        </Col> 
        <Col xs="6">
          <FormGroup>
          <Label htmlFor="coliforme_totaux">Coliformes totaux</Label>
          <Input type="text" id="coliforme_totaux" name="coliforme_totaux" />
          </FormGroup>
        </Col>  
      </Row> 
    )
  }

  isotope() {  
    return ( 
      <>  
      </> 
    )
  }

  pesticide() {  
    return ( 
      <>  
    </> 
    )
  }
}

export default Analyses;
