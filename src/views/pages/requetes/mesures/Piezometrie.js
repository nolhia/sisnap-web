import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { Table } from 'reactstrap';
import { formatDate } from '../../../../helpers';
function DataRow(props) {
  const data = props.data

  return (
    <tr key={data.id.toString()}>
      <td>{formatDate(data.date_mesure)} / {data.heure_mesure}</td>
      <td>{data.station_titre}</td>
      <td>{data.niveau_statique_sol}</td>
      <td>{data.repere_sol}</td>
      <td>{data.repere_eau}</td>
      <td>{data.ph}</td>
      <td>{data.temperature}</td>
      <td>{data.conductivite}</td>
    </tr>
  )
}

class Piezometrie extends Component {

  constructor(props){
      super(props);
      this.state = {
          error : null,
          isLoaded : false,
          data : []
      };
  }
 // initialiser
  async componentDidMount() {
    const data = this.props.data;
    this.setState({
      isLoaded: true,
      data: data
    });
  }
// affichage

  render() {
      const { isLoaded, data } = this.state;

      if (this.state.error){
          return <div>Error in loading</div>
      } else if (!isLoaded) {
          return <div>Loading ...</div>
      } else{
        return (
              <Table responsive hover>
                <thead>
                  <tr>
                    <th scope="col">D/H Mesure </th>
                    <th scope="col">Piézometre </th>
                    <th scope="col">NS. sol</th>
                    <th scope="col">Rep. sol</th>
                    <th scope="col">Rep. eau</th>
                    <th scope="col">pH</th>
                    <th scope="col">&deg;C</th>
                    <th scope="col">Conductivité</th>
                  </tr>
                </thead>
                <tbody>
                  {data && data.length > 0 && data.map((objet, index) =>
                    <DataRow key={index} data={objet}/>
                  )}
                </tbody>
              </Table>
        )
      }
  }

}

export default Piezometrie;
