import React, { Component } from 'react'; 
import { Table } from 'reactstrap';
import { formatDate } from '../../../../helpers';

function DataRow(props) {
  const data = props.data
   
  return (
    <tr key={data.id.toString()}>
      <td>{formatDate(data.date_releve)} / {data.heure_suivi}</td>  
      <td>{data.hauteur_moyenne}</td>
      <td>{data.hauteur_matin}</td>
      <td>{data.hauteur_soir}</td>  
    </tr>
  )
}

class Hydrometrie extends Component {
  
  constructor(props){
      super(props);
      this.state = {
          error : null,
          isLoaded : false,
          data : []
      };    
  }
// initialiser
  async componentDidMount() { 
    const data = this.props.data; 
    this.setState({ 
      isLoaded: true, 
      data: data
    });
  }  

  // affichage
  render() {
      const { isLoaded, data } = this.state;  
      
      if (this.state.error){
          return <div>Error in loading</div>
      } else if (!isLoaded) {
          return <div>Loading ...</div>
      } else{
        return (
              <Table responsive hover>
                <thead>
                  <tr>
                  <th scope="col">D/H Mesure </th>  
                    <th scope="col">Hauteur moyenne</th>
                    <th scope="col">Hauteur matin</th>
                    <th scope="col">Hauteur soir</th> 
                  </tr>
                </thead>
                <tbody>
                  {data && data.length > 0 && data.map((objet, index) =>
                    <DataRow key={index} data={objet}/>
                  )}
                </tbody>
              </Table> 
        )
      }
  }
 
}

export default Hydrometrie;
