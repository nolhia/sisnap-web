import React, { Component } from 'react';
import { Button, Card, CardBody, CardHeader, Col, Row, Form, FormGroup, Input, Label } from 'reactstrap';
import { parametersService  } from '../../../../services';
import { callMaker } from '../../../../helpers'; 
import Piezometrie  from './Piezometrie'; 
import Pluviometrie  from './Pluviometrie'; 
import Hydrometrie  from './Hydrometrie'; 
import { ZoneGeographique }  from '../../../../components'; 
import { CSVLink } from 'react-csv';

class Mesures extends Component {
  
  constructor(props){
      super(props);
      this.state = {
          error : null,
          isLoaded : true,
          data : [],        
          type_requete : [],
          type_requete_id: 1,
          station_id: 0,
          filename: 'Résultats'
      };   
      this.handleTypeMesure = this.handleTypeMesure.bind(this);
      this.handleChange = this.handleChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() { 
    const type_requete = await parametersService.getTypeMesure(); 
    const stations = await callMaker('get', '/stations'); 
    this.setState({ 
      isLoaded: true,  
      type_requete: type_requete, 
      stations: stations
    });
  }  

  handleSubmit = async  (event) => {
      event.preventDefault()
      const type_requete_id = event.target.type_requete_id.value;
      let params = {
        type_requete_id: event.target.type_requete_id.value, 
        station_id: event.target.station_id.value, 
        date_debut: event.target.date_debut.value || '1970-01-01', 
        date_fin: event.target.date_fin.value || new Date(), 
        region_id: event.target.region_id.value, 
        departement_id: event.target.departement_id.value, 
        commune_id: event.target.commune_id.value, 
      }
      if(type_requete_id == 1){
        params.conductivite = event.target.conductivite.value;
        params.temperature = event.target.temperature.value;
        params.ph = event.target.ph.value;
      }
      else if (type_requete_id == 2){
        params.quantite = event.target.quantite.value;
      }
      else if (type_requete_id == 3){
        params.hauteur_matin = event.target.hauteur_matin.value;
        params.hauteur_soir = event.target.hauteur_soir.value;
      }

      this.setState({ isLoaded: false }); 
      callMaker('post', '/requete/mesures', params) 
      .then(
          response => { 
            if(response.status >= 400){
              this.setState({ error: response.message, lisLoaded: true });
            }
            else{ 
              console.log(response)
              this.setState({ isLoaded: true, data: response, msg: "Requête traitée avec succès" });
            } 
          },
          error => { 
            if(typeof error === 'string'){
                this.setState({ error, data: [], isLoaded: true })
            }
            else{
                this.setState({ error: 'Connexion refusée.', data: [], isLoaded: true })
            } 
          } 
      ); 
  }

  handleTypeMesure(e) { 
    this.setState({ type_requete_id: e.target.value }); 
  }
  handleChange(e){
      this.setState({[e.target.name]: e.target.value});  
  }
  
  render() {
    const { isLoaded, data, type_requete_id, filename } = this.state;  
    
    if (this.state.error){
        return <div>Error in loading</div>
    } else if (!isLoaded) {
        return <div>Loading ...</div>
    } else{
      return (
        <div className="animated fadeIn">
          { this.filtre() }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i> <strong>Résultats</strong> 
                  <div className="card-header-actions">
                    <Button color="secondary" size="sm" className="btn-square"><i className="icon-printer"></i>
                      &nbsp;<CSVLink data={data} filename={filename}>Exporter en Excel</CSVLink>
                    </Button>  
                  </div>
                </CardHeader>
                <CardBody>
                  { data && data.length == 0 && <strong>Aucune donnée à afficher</strong> }
                  { type_requete_id == 1 && data && data.length > 0 && <Piezometrie data = {data} {...this.props} /> }
                  { type_requete_id == 2 && data && data.length > 0 && <Pluviometrie data = {data} {...this.props} /> }
                  { type_requete_id == 3 && data && data.length > 0 && <Hydrometrie data = {data} {...this.props} /> }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }

  filtre() { 
    const {  type_requete, stations, type_requete_id, station_id } = this.state; 
    return (
      <div className="animated fadeIn">
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Requête</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="danger"><i className="fa fa-dot-circle-o"></i> Executer</Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="type_requete_id">Type de requête</Label>
                        <Input type="select" id="type_requete_id" name="type_requete_id" value={type_requete_id} onChange={this.handleTypeMesure} required >
                          {type_requete && type_requete.length > 0 && type_requete.map(type => <option key={type.id} value={type.id}>{type.titre}</option>)}
                        </Input> 
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="station_id">Station de suivi</Label>
                        <Input type="select" id="station_id" name="station_id" value={station_id} onChange={this.handleChange} >
                          <option value='Tout' key='0'></option>
                          {stations && stations.length > 0 && stations.map(station => <option key={station.id} value={station.id}>{station.titre}</option>)}
                        </Input> 
                      </FormGroup>
                    </Col>
                    <Col xs="3">
                      <FormGroup>
                      <Label htmlFor="date_debut">Date début</Label>
                      <Input type="date" id="date_debut" name="cout" />
                      </FormGroup>
                    </Col> 
                    <Col xs="3">
                      <FormGroup>
                      <Label htmlFor="date_fin">Date fin</Label>
                      <Input type="date" id="date_fin" name="date_fin" />
                      </FormGroup>
                    </Col> 
                  </Row> 
                  <ZoneGeographique {...this.props}/> 
                  <hr/> 
                  { type_requete_id == 1 && this.piezometrie() }
                  { type_requete_id == 2 && this.pluviometrie() }
                  { type_requete_id == 3 && this.hydrometrie() }
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }

  piezometrie() {  
    return ( 
      <Row> 
        <Col xs="4">
          <FormGroup>
          <Label htmlFor="conductivite">Conductivité</Label>
          <Input type="text" id="conductivite" name="conductivite" />
          </FormGroup>
        </Col> 
        <Col xs="4">
          <FormGroup>
          <Label htmlFor="temperature">Température</Label>
          <Input type="text" id="temperature" name="temperature" />
          </FormGroup>
        </Col> 
        <Col xs="4">
          <FormGroup>
          <Label htmlFor="ph">pH</Label>
          <Input type="text" id="ph" name="ph" />
          </FormGroup>
        </Col> 
      </Row> 
    )
  }

  pluviometrie() {  
    return ( 
        <Row> 
          <Col xs="12">
            <FormGroup>
            <Label htmlFor="quantite">Quantité</Label>
            <Input type="text" id="quantite" name="quantite" />
            </FormGroup>
          </Col>  
        </Row> 
    )
  }

  hydrometrie() {  
    return ( 
        <Row> 
          <Col xs="6">
            <FormGroup>
            <Label htmlFor="hauteur_matin">Hauteur matin</Label>
            <Input type="text" id="hauteur_matin" name="hauteur_matin" />
            </FormGroup>
          </Col> 
          <Col xs="6">
            <FormGroup>
            <Label htmlFor="hauteur_soir">Hauteur soir</Label>
            <Input type="text" id="hauteur_soir" name="hauteur_soir" />
            </FormGroup>
          </Col> 
        </Row> 
    )
  }

}

export default Mesures;
