import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, Input, Row, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import MaterialTable from 'material-table'; 
import { callMaker, callFileTransfer } from '../../../helpers';
import { CSVLink } from 'react-csv';

import { forwardRef } from 'react';
import AddBox from '@material-ui/icons/AddBox';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const tableIcons = {
  Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
  ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

class Codelithologique extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      data: [],
      msg: '',
      modalImport: false,
      selectedFile: null,
      filename: 'Code lithologique'
    };
    this.supprimer = this.supprimer.bind(this);
    this.toggle = this.toggle.bind(this);
  }
// initilisation
  async componentDidMount() {
    const data = await callMaker('get', `/parametre/codelithologique`);

    const columns = [
      { title: 'Code', field: 'code' },
      { title: 'Titre', field: 'titre' }  
    ]
    this.setState({ isLoaded: true, data: data, columns: columns });
  }
// supprimer
  async supprimer(objet) {
      try {
        await callMaker('delete', `/parametre/codelithologique/${objet.id}`); 
        this.setState({ loading: false, msg: "Traitement effectué avec succès" });
      } catch (error) {
        this.setState({ error })
      } 
  }
// charger un fichier
  onFileChangeHandler = event => {
    this.setState({
      selectedFile: event.target.files[0]
    })
  }
// afficher le popup de selection du fichier
  toggle() {
    this.setState({
      modalImport: !this.state.modalImport
    });
  }
  // gerer le chargement de fichier
  onClickImportHandler = async () => {
    let data = new FormData()
    data.append('file', this.state.selectedFile);
    this.setState({ isLoaded: false, msg: "" });
    callFileTransfer('/parametre/codelithologique/upload', data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: "Une erreure inatendue s'est produite durant l'opération. Veuillez vérifier le contenu de votre fichier SVP.", isLoaded: true, modalImport: false });
          }
          else {
            this.setState({ isLoaded: true, msg: "Traitement effectué avec succès", modalImport: false });
            window.location.reload(true);
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error, loading: false })
          }
          else {
            this.setState({ error: 'Connexion refusée.', loading: false })
          }
        })
  }
// affichage
  render() {

    const { isLoaded, data, columns, error, msg, filename } = this.state;

    if (!isLoaded) {
      return <div>Loading ...</div>
    } else {
      return (
        <div className="animated fadeIn">
          <br />
          {error &&
            <div className={'alert alert-danger'}>{error}</div>
          }
          {msg &&
            <div className={'alert alert-success'}>{msg}</div>
          }
          <Row>
            <Col xl={12}>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify" /><strong>Liste des codes lithologiques</strong>
                  <div className="card-header-actions">
                    {this.import_view()}
                    <Link to='/parametre/codelithologique/add'><Button color="primary" size="sm" className="btn-square"><i className="icon-plus"></i>&nbsp;Nouveau</Button></Link>&nbsp;&nbsp;
                    <Button color="secondary" onClick={this.toggle} size="sm" className="btn-square"><i className="icon-arrow-down-circle"></i>&nbsp;Importer un fichier</Button>
                    &nbsp;
                    <Button color="secondary" size="sm" className="btn-square"><i className="icon-printer"></i>
                      &nbsp;<CSVLink data={data} filename={filename}>Exporter en Excel</CSVLink>
                    </Button>
                  </div>
                </CardHeader>
                <CardBody>
                  <MaterialTable
                    title="Liste" 
                    rowsPerPageOptions={[5, 10, 25, 50,100]}
                    rowsPerPage={ 10}
                    icons={tableIcons}
                    columns={columns}
                    data={data}
                    actions={[
                      {
                        icon: () => <Edit />,
                        tooltip: "Edit",
                        onClick: (event, rowData) => {
                          this.props.history.push(`/parametre/codelithologique/edit/${rowData.id}`);
                        }
                      }
                    ]}
                    editable={{
                      onRowDelete: oldData => {
                        return this.supprimer(oldData);
                      },
                    }}
                  />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      )
    }
  }

  import_view() {
    return (
      <Modal isOpen={this.state.modalImport} toggle={this.toggle} >
        <ModalHeader toggle={this.toggle}>Importer un fichier</ModalHeader>
        <ModalBody>
          <Input type="file" name="file" onChange={this.onFileChangeHandler} />
        </ModalBody>
        <ModalFooter>
          <Button color="primary" type="button" onClick={this.onClickImportHandler}>Executer</Button>
          <Button color="secondary" onClick={this.toggle}>Annuler</Button>
        </ModalFooter>
      </Modal>
    )

  }
}

export default Codelithologique;
