import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Form } from 'reactstrap'; 
import { callMaker } from '../../../helpers';
class CodelithologiqueCreate extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      code: '', 
      titre: '',
      loading: false, 
      msg: '',
      error: ''
    }
    this.baseState = this.state;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }
// initialiser
  async componentDidMount() {  
    if (this.props.match.params.id) {
      const codelithologique = await await callMaker('get', `/parametre/codelithologique/${this.props.match.params.id}`);
      this.setState({ isLoaded: true, ...codelithologique });
      this.baseState = this.state;
    }
  }
// soumettre le formulaire
  handleSubmit = async (event) => {
    event.preventDefault()
    const data = {
      id: event.target.id.value, 
      code: event.target.code.value, 
      titre: event.target.titre.value
    }

    let method = 'put';
    if (data.id === '' || data.id === 0 || data.id === undefined) {
      delete data['id']; 
      method = 'post';
    }  
  
    this.setState({ loading: true });
    callMaker(method, `/parametre/codelithologique`, data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error, loading: false })
          }
          else {
            this.setState({ error: 'Connexion refusée.', loading: false })
          }
        }
      );

  }
// suivre les changements
  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
// reinitialiser le formulaire
  resetForm = () => {
    this.setState(() => this.baseState)
  }
// Afficher
  render() {
    const { id, code, titre, error, msg } = this.state;

    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Code lithologique</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button>
                    &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm} color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/parametre/codelithologique" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="3">
                      <FormGroup>
                        <Label htmlFor="titre">Code *</Label>
                        <Input type="text" value={code} onChange={this.handleChange} id="code" name="code" required />
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id' />
                      </FormGroup>
                    </Col>
                    <Col xs="9">
                      <FormGroup>
                        <Label htmlFor="titre">Intitulé *</Label>
                        <Input type="text" value={titre} onChange={this.handleChange} id="titre" name="titre" required />
                      </FormGroup>
                    </Col>
                  </Row>  
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }


}

export default CodelithologiqueCreate;
