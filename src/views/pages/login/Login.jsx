import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { userService } from '../../../services';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from '../../../../node_modules/reactstrap/lib';
import { callMaker } from '../../../helpers'; 

class Login extends Component {

  constructor(props) {
    super(props);  
    userService.logout();

    this.state = {
        email: '',
        password: '',
        submitted: false,
        loading: false,
        error: '',
        action: 'login',
        submitButtonTitle: 'Connecter',
        resetButtonTitle: 'Mot de passe oublié?',
        msg: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);  
    this.forgotPassword = this.forgotPassword.bind(this);
  }

  handleChange(e) {
      const { name, value } = e.target;
      this.setState({ [name]: value });
  }
  
  handleSubmit = async  (e) => {
      e.preventDefault(); 
      this.setState({ submitted: true });
      const { email, password } = this.state;

      // stop here if form is invalid
      if (this.state.action === 'login' && !(email && password)) { //login action
          return;
      }
      else if (this.state.action === 'reset' && !email) {  //reset password action
          return;
      }

      this.setState({ loading: true });
      if (this.state.action === 'login'){
          userService.login(email, password) 
              .then(
                  user => { 
                      //stocker les donnees statiques dans le local storage
                      callMaker('get', '/parametre/regions/all').then(
                        zonegeographique => { 
                          localStorage.setItem('zonegeographique', JSON.stringify(zonegeographique));
                          
                      });
                      // sauvegarder les parametres par defaut
                      localStorage.setItem('parametre', JSON.stringify({
                          niveau_entree: user.user_info.niveau_entree,
                          region_id: (user.user_info.region_id !== null) ? user.user_info.region_id : '',
                          departement_id: (user.user_info.departement_id !== null) ? user.user_info.departement_id : '',
                          enquete_id: ''
                      }));
                      const { from } = this.props.location.state || { from: { pathname: "/" } };
                      this.props.history.push(from);
                  },
                  error => {
                    if(typeof error === 'string'){
                      this.setState({ error , loading: false })
                    }
                    else{
                      this.setState({ error: 'Connexion au serveur refusée.' , loading: false })
                    }
                    
                  }
              );
      }
      else if (this.state.action === 'reset'){ 
          userService.forgotPassword(email)
              .then(
                  user => {
                      let message = `Nous vous avons envoyé un lien vous permettant de réinitialiser votre mot de passe. Ce lien sera valable pour une heure.`
                      this.setState({ msg: message , loading: false })
                      // const { from } = this.props.location.state || { from: { pathname: "/dashboard" } };
                      // this.props.history.push(from);
                  },
                  error => { 
                      if(typeof error === 'string'){
                          this.setState({ error , loading: false })
                      }
                      else{
                          this.setState({ error: 'Connexion refusée.' , loading: false })
                      } 
              }
          );
      }
  }

  validateForm() {
    return this.state.email.length > 5 && this.state.password.length > 3;
  } 

  forgotPassword(e) { 
      if(this.state.action === 'login'){
          this.setState({ action: 'reset', submitButtonTitle: 'Réinitialiser', resetButtonTitle: "Se connecter" }); 
      }
      else{
          this.setState({ action: 'login', submitButtonTitle: 'Connecter', resetButtonTitle: "Mot de passe oublié?" }); 
      } 
  } 

  render() {
    const { email, password, loading, error, action, submitButtonTitle, resetButtonTitle, msg } = this.state;

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form name="form" onSubmit={this.handleSubmit}>
                      <h1>S'identifier</h1>
                      <p className="text-muted">Accéder à votre compte</p>
                      {action === 'login' && 
                          <>
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="icon-user"></i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input type="text" placeholder="Courriel" autoComplete="email" name="email" value={email} onChange={this.handleChange}  />
                          </InputGroup>
                          <InputGroup className="mb-4">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="icon-lock"></i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input type="password" placeholder="Mot de passe" autoComplete="password" name="password" value={password} onChange={this.handleChange} />
                          </InputGroup>
                          </>
                      }
                      {action === 'reset' && 
                          <>
                          <InputGroup className="mb-3">
                            <InputGroupAddon addonType="prepend">
                              <InputGroupText>
                                <i className="icon-user"></i>
                              </InputGroupText>
                            </InputGroupAddon>
                            <Input type="text" placeholder="Courriel" autoComplete="email" name="email" value={email} onChange={this.handleChange}  />
                          </InputGroup>
                          </>
                      }
                      <Row>
                        <Col xs="5">
                          <Button color="primary" className="px-4" disabled={loading}>{ submitButtonTitle }</Button>
                          {loading &&
                              <img alt='Loading...' src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" />
                          }
                        </Col>
                        <Col xs="7" className="text-right">
                          <Button color="link" className="px-0" onClick={this.forgotPassword} >{ resetButtonTitle }</Button>
                        </Col>
                      </Row>
                      <br/>
                      {error &&
                          <div className={'alert alert-danger'}>{error}</div>
                      }
                      {msg &&
                          <div className={'alert alert-success'}>{msg}</div>
                      }
                    </Form>
                  </CardBody>
                </Card>
                <Card className="text-white bg-primary py-5 d-md-down-none" style={{ width: '44%' }}>
                  <CardBody className="text-center">
                    <div>
                      <Row>
                        <Col xs="6">
                            <img src={'../../assets/img/flag.png'}  alt="NIGER" width="60" height="40"/> 
                        </Col>
                        <Col xs="6" > 
                          <img src={'../../assets/img/ue-logo.png'}  alt="UE" width="60" height="40"/>
                        </Col>
                      </Row>
                      <br/>
                      <h2>SISNA</h2>
                      <p>
                        Système d'Information pour le suivi des nappes alluviales 
                      </p>
                      <p>
                        Une composante du SISEAN
                      </p> 
                    </div>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
