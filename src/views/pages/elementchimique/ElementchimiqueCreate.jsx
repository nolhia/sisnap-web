import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button, Card, CardBody, CardHeader, Col, FormGroup, Input, Label, Row, Form } from 'reactstrap';
import { userService } from '../../../services';
import { elementchimiqueService } from '../../../services';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';

class ElementchimiqueCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      code: '',
      titre: '',
      unite: '',
      analyse_chimique: false,
      bacteriologie: false,
      isotopie: false,
      pesticide: false,
      loading: false,
      mode_edition: 'no',
      min_norme: 0,
      max_norme: '',
      msg: '',
      error: ''
    }
    this.baseState = this.state;
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.resetForm = this.resetForm.bind(this);
  }

  async componentDidMount() {
    if (this.props.match.params.id) {
      const elementchimique = await elementchimiqueService.find(this.props.match.params.id);
      this.setState({ isLoaded: true, mode_edition: 'yes', ...elementchimique });
      this.baseState = this.state;
    }
  }

  handleSubmit = async (event) => {
    event.preventDefault()
    const data = {
      id: event.target.id.value,
      mode_edition: event.target.mode_edition.value,
      titre: event.target.titre.value,
      code: event.target.code.value,
      unite: event.target.unite.value,
      analyse_chimique: event.target.analyse_chimique.value,
      bacteriologie: event.target.bacteriologie.value,
      isotopie: event.target.isotopie.value,
      pesticide: event.target.pesticide.value,
      min_norme: event.target.min_norme.value !== '' ? Number(event.target.min_norme.value) : 0,
      max_norme: event.target.max_norme.value !== '' ? Number(event.target.max_norme.value) : null
    }
 
    if (data['min_norme'] != null || data['max_norme'] != null) {
      if (data['min_norme'] > data['max_norme']) {
        this.setState({ error: "La valeur maximale de la norme doit être supérieure à la valeur minimale", loading: false });
        return;
      }
    }
 
    if (data.id === '') {
      delete data['id'];
    }
    delete data['mode_edition'];

    this.setState({ loading: true });
    elementchimiqueService.save(data)
      .then(
        response => {
          if (response.status >= 400) {
            this.setState({ error: response.message, loading: false });
          }
          else {
            this.setState({ loading: false, msg: "Traitement effectué avec succès" });
          }
        },
        error => {
          if (typeof error === 'string') {
            this.setState({ error, loading: false })
          }
          else {
            this.setState({ error: 'Connexion refusée.', loading: false })
          }
        }
      );
  }

  handleChange(e) {
    const value = e.target.type === 'checkbox' ? e.target.checked : e.target.value;
    this.setState({
      [e.target.name]: value
    });
  }
  
  resetForm = () => {
    this.setState(() => this.baseState);
  }

  render() {
    const { id, code, titre, unite, analyse_chimique, bacteriologie, isotopie, pesticide, min_norme, max_norme, error, msg, mode_edition } = this.state;
    return (
      <div className="animated fadeIn">
        <br />
        {error &&
          <div className={'alert alert-danger'}>{error}</div>
        }
        {msg &&
          <div className={'alert alert-success'}>{msg}</div>
        }
        <Form onSubmit={this.handleSubmit} method="POST">
          <Row>
            <Col>
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i><strong>Paramètres d'analyse</strong>
                  <div className="card-header-actions">
                    <Button type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enregistrer</Button>
                    &nbsp;&nbsp;<Button type="reset" size="sm" onClick={this.resetForm} color="danger"><i className="fa fa-ban"></i> Réinitialiser</Button>
                    &nbsp;&nbsp; <Link to="/parametre/elementchimiques" ><Button color="secondary" size="sm" className="btn-square"><i className="icon-list"></i>&nbsp;Afficher la liste</Button></Link>
                  </div>
                </CardHeader>
                <CardBody>
                  <Row>
                    <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="code">Code *</Label>
                        <Input type="hidden" value={id} onChange={this.handleChange} name='id' />
                        <Input type="hidden" value={mode_edition} onChange={this.handleChange} name='mode_edition' />
                        <Input type="text" value={code} onChange={this.handleChange} id="code" name="code" required />
                      </FormGroup>
                    </Col>
                    <Col xs="8">
                      <FormGroup>
                        <Label htmlFor="titre">Libelle *</Label>
                        <Input type="text" value={titre} onChange={this.handleChange} id="titre" name="titre" required />
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr color="info" />
                  <Row>
                    <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="unite">Unité</Label>
                        <Input type="text" id="unite" name="unite" value={unite} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="min_norme">Norme: valeur minimale</Label>
                        <Input type="number" id="min_norme" name="min_norme" value={min_norme} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                    <Col xs="4">
                      <FormGroup>
                        <Label htmlFor="max_norme">Norme: valeur maximale</Label>
                        <Input type="number" id="max_norme" name="max_norme" value={max_norme} onChange={this.handleChange} />
                      </FormGroup>
                    </Col>
                  </Row>
                  <hr color="info" />
                  <Row>
                    <Col xs="12">
                      <FormGroup>
                        <Label htmlFor="type_analyse">Types d'analyse associés </Label>
                        <br />
                        <FormControlLabel
                          control={<Checkbox checked={analyse_chimique} onChange={this.handleChange} value={analyse_chimique} id="analyse_chimique" name="analyse_chimique" />}
                          label="Analyse chimique"
                        />
                        <FormControlLabel
                          control={<Checkbox checked={bacteriologie} onChange={this.handleChange} value={bacteriologie} id="bacteriologie" name="bacteriologie" />}
                          label="Bactériologie"
                        />
                        <FormControlLabel
                          control={<Checkbox checked={isotopie} onChange={this.handleChange} value={isotopie} id="isotopie" name="isotopie" />}
                          label="Isotopie"
                        />
                        <FormControlLabel
                          control={<Checkbox checked={pesticide} onChange={this.handleChange} value={pesticide} id="pesticide" name="pesticide" />}
                          label="Pesticide"
                        />

                      </FormGroup>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Form>
      </div>
    )
  }

}

export default ElementchimiqueCreate;
