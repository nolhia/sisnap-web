import * as config  from '../config'; 
import { userService  } from '../services';
// retourner l'url de l'api backend
export function baseUrl() {
    const backendUrl = config.default.backendUrl;
    return backendUrl;
}
// gestion de l'entete des requetes
export function authHeader() {
    // return authorization header with basic auth credentials
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return { 
            'Content-type': 'application/json',
            'Authorization': 'Bearer ' + user.token 
        };
    } else {
        return {};
    }
}
// entete des requetes avec des fichiers
function authHeaderUpload() { 
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return {  
            'Authorization': 'Bearer ' + user.token 
        };
    } else {
        return {};
    }
}
 
// fonction faisant les appels
export async function callMaker(method, endpoint, body){ 
    const response = await fetch(`${baseUrl()}${endpoint}`, {
      method: method,
      body: body && JSON.stringify(body),
      headers: authHeader(),
    });
     
    if(response && response.status === 401){  
        userService.logout();
        window.location.reload(true);
        return;
    } 
    return await response.json();
}

// execution des requetes avec des fichiers
export async function callFileTransfer(endpoint, body){ 
    const response = await fetch(`${baseUrl()}${endpoint}`, {
        method: 'POST',
        body: body,
        //mode: 'no-cors',
        headers: authHeaderUpload(),
    });
     
    if(response && response.status === 401){  
        userService.logout();
        return;
    } 
    return await response; //response.json();
}
// appel aux url non connus
export async function unsecureCallMaker(method, endpoint, body){ 
    const response = await fetch(`${baseUrl()}${endpoint}`, {
      method: method,
      body: body && JSON.stringify(body),
      headers: { 'Content-type': 'application/json' },
    });

    return await response.json();
}
