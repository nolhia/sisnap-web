import moment from 'moment';
// nettoyage des donnees, null
export function nullReplace(jsonObject) {
    let object = JSON.stringify(jsonObject).replace(/null/g, '""');
    return JSON.parse(object);
}
// nettoyage des donnees, vide
export function removeEmptyAttributes(jsonObject) {
    for (var propName in jsonObject) {
        if (jsonObject[propName] === null || jsonObject[propName] === undefined || jsonObject[propName] === '') {
            delete jsonObject[propName];
        }
    }

    return jsonObject;
}
// retrouver un parametre
export function getParameterValue(jsonObject, code) { 
    const res = jsonObject && jsonObject.map(function(objet) {
        if (objet && objet.code == code){
            return objet.value;
        }
    });

    return res || '';
}
// retrouver un parametre par id
export function getParameterValueById(jsonObject, id) { 
    const res = jsonObject && jsonObject instanceof Array && jsonObject.map(function(objet) {
        if (objet && objet.id == id){
            return objet.title || objet.titre;
        }
    });

    return res || '';
}
//formater les dates
export function formatDate(_date) {
    if(_date){
        return moment(_date).format("DD/MM/Y");
    }
    return ''
}

//date du jour
export function currentDate() { 
    return moment().format("DD/MM/Y"); 
}

// valider latitude
export function validerLatitute(value) {
    if(value >= 11 && value <= 24){
        return true;
    }
    return false;
}
// valider longitude
export function validerLongitude(value) {
    if(value >= 0 && value <= 16){
        return true;
    }
    return false;
}
// gestion base de donnees locale
export const asyncLocalStorage = {
    setItem: async function (key, value) {
        await null;
        return localStorage.setItem(key, value);
    },
    getItem: async function (key) {
        await null;
        return JSON.parse(localStorage.getItem(key));
    }
}; 
