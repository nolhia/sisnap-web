// Menu visible pour les agents de collectes
export default {
  items: [ 
    {
      divider: true
    },
    {
      title: true,
      name: 'Collectes',
      wrapper: {
        element: '',
        attributes: {}
      }
    },
    {
      name: 'Analyses',
      url: '/collecte-analyses',
      icon: 'icon-grid',
      children: [
        {
          name: 'Physico-chimiques',
          url: '/collecte/analyseschimiques',
          icon: 'icon-chemistry'
        },
        {
          name: 'Bactériologiques',
          url: '/collecte/analysesbacteriologiques',
          icon: 'icon-social-reddit'
        },
        {
          name: 'Isotopiques',
          url: '/collecte/analysesisotopiques',
          icon: 'icon-layers'
        },
        {
          name: 'Pesticides',
          url: '/collecte/analysespesticides',
          icon: 'icon-layers'
        } 
      ]
    },
    {
      name: "Mesures",
      url: "/collecte-mesures",
      icon: "icon-equalizer",
      children: [
        {
          name: 'Piézométrie',
          url: '/collecte/piezometries',
          icon: 'icon-control-pause'
        },
        {
          name: 'Pluviométrie',
          url: '/collecte/pluviometries',
          icon: 'icon-magnet'
        },
        {
          name: 'Hydrométrie',
          url: '/collecte/hydrometries',
          icon: 'icon-social-soundcloud'
        } 
      ]
    } 
  ]
};
