// Menu visible pour les gestionnaires et les administrateurs
export default {
  items: [
    {
      divider: true
    },
    {
      title: true,
      name: 'Paramétrage',
      wrapper: {
        // optional wrapper object
        element: '', // required valid HTML5 element tag
        attributes: {} // optional valid JS object with JS API naming ex: { className: "my-class", style: { fontFamily: "Verdana" }, id: "my-id"}
      },
      class: '' // optional class names space delimited list for title item ex: "text-center"
    },
    {
      name: 'Tournées/Campagnes',
      url: '/parametre/enquetes',
      icon: 'icon-notebook'
    },
    {
      name: 'Données de base',
      url: '/sdfdsfdgd',
      icon: 'icon-settings',
      children: [
        {
          name: 'Nappes',
          url: '/parametre/nappes',
          icon: 'icon-social-soundcloud'
        },
        {
          name: "Station de suivi",
          url: "/parametre/stations",
          icon: "icon-compass"
        },
        {
          name: 'Coupe géologique',
          url: '/parametre/coupegeologiques',
          icon: 'icon-picture'
        },
        {
          name: 'Zone géographique',
          url: '/parametre/regions',
          icon: 'icon-globe' 
        },
        {
          name: 'Type ouvrage',
          url: '/parametre/typeouvrages',
          icon: 'icon-flag'
        },
        {
          name: 'Code lithologique',
          url: '/parametre/codelithologique',
          icon: 'icon-globe-alt'
        },
        {
          name: "Paramètres d'analyse",
          url: '/parametre/elementchimiques',
          icon: 'icon-chemistry'
        },
        {
          name: 'Laboratoires',
          url: '/parametre/laboratoires',
          icon: 'icon-eyeglass'
        }
      ]
    },
    {
      name: 'Utilisateurs',
      url: '/users',
      icon: 'icon-people'
    }
  ]
};
